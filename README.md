# Pig: API specification editor

## Overview

This is an editor that generates projects that may be generated into APIs and working servers.

## Templating

We support two different template types with substitution that will happen at different times depending on the type:
1. {{variable}} - our own templating that will be replaced before an OpenAPI spec is rendered. The variables that are
	used for substitution include, but are not limited to a project's name a projects environment, a model's name, a view's
	transport type, etc.
2. http://path/{param} - variables in single brackets are reserved for use by swagger. They are substituted at runtime.   

## Actions/Reducers

### Immutability

We have adopted an object oriented model with well defined classes.  So we discourage working outside of well defined objects.  So how does one apply updates to an object?  The process is as follows:


	1. Clone the object so that the id is the same. All of our model objects have a clone method.
 	2. Execute the correct action to replace the object.
 
