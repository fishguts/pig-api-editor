/**
 * User: curtis
 * Date: 3/24/18
 * Time: 4:46 AM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");

const types=module.exports.types={
	SET_STATE: "HISTORY_SET_STATE",
	NAVIGATE_BACKWARD: "HISTORY_NAVIGATE_BACKWARD",
	NAVIGATE_FORWARD: "HISTORY_NAVIGATE_FORWARD",
	NAVIGATE_TO: "HISTORY_NAVIGATE_TO"
};


/**
 * Goes backwards in history
 * @return {Object}
 */
module.exports.goBack=function() {
	return {
		type: types.NAVIGATE_BACKWARD
	};
};

/**
 * Goes forwards in history
 * @return {Object}
 */
module.exports.goForward=function() {
	return {
		type: types.NAVIGATE_FORWARD
	};
};

/**
 * Navigates to editor for specified project
 * @param {Project|String} project - project object or projectId
 * @param {string} subView
 * @return {Object}
 */
module.exports.goToProject=function(project, subView="core") {
	const projectId=_.isObject(project) ? project.id : project;
	return {
		type: types.NAVIGATE_TO,
		path: `/project/${projectId}/${subView}`,
		replace: false
	};
};

/**
 * Navigates to the main projects view
 * @return {Object}
 */
module.exports.goToProjects=function() {
	return {
		type: types.NAVIGATE_TO,
		path: "/projects",
		replace: false
	};
};

/**
 * Sets up history in our store. We are violating a few rules here.
 * @param {Object} history
 * @return {Object}
 */
module.exports.set=function(history) {
	return {
		type: types.SET_STATE,
		history: history
	};
};

