/**
 * User: curtis
 * Date: 5/13/2018
 * Time: 5:11 PM
 * Copyright @2018 by Xraymen Inc.
 */

const constant=require("../common/constant");
const model_factory=require("../model/factory");

const types=exports.types={
	LOAD: "GAMUTS_LOAD"
};

/* eslint-disable valid-jsdoc */
/**
 * Loads the gamuts. Right now our gamuts are bundled in our package. This guy is a placeholder should
 * we ever decide to load them server side.
 */
module.exports.load=function() {
	return {
		type: types.LOAD,
		status: model_factory.createStatus({
			value: constant.status.SUCCESS
		})
	};
};
