/**
 * User: "APP_User",
 * Date: "APP_Date",
 * Time: "APP_Time",
 * Copyright @2018 by Xraymen Inc.
 */

const constant=require("../common/constant");
const model_factory=require("../model/factory");


const types=exports.types={
	LOAD: "APP_APPLICATION_LOAD",
	CONFIGURE: "APP_CONFIGURATION_LOAD",
	CONSOLE_VISIBLE: "APP_CONSOLE_VISIBLE",
	SET_SESSION_SETTING: "APP_SET_SESSION_SETTING"
};

/* eslint-disable valid-jsdoc */
/**
 * Gets the application to configure itself
 * @param {string} nodenv
 */
module.exports.configure=function(nodenv=undefined) {
	return {
		type: types.CONFIGURE,
		nodenv: nodenv,
		status: model_factory.createStatus({value: constant.status.action.EXECUTE})
	};
};

/**
 * Gets the application to configure itself
 * @param {Status} status
 */
module.exports.configured=function(status=model_factory.createStatus({value: constant.status.action.SUCCESS})) {
	return {
		type: types.CONFIGURE,
		status
	};
};

/**
 * Whether activity console is visible or not
 * @param {Boolean} visible
 * @return {{type: string, visible: *}}
 */
module.exports.console=function(visible) {
	return {
		type: types.CONSOLE_VISIBLE,
		visible: visible
	};
};


/**
 * Tells the app that it's time to load the UI
 */
module.exports.load=function() {
	return {
		type: types.LOAD,
		status: model_factory.createStatus({value: constant.status.action.EXECUTE})
	};
};

/**
 * Tells the app that the application has been loaded
 */
module.exports.loaded=function(status=model_factory.createStatus({value: constant.status.action.SUCCESS})) {
	return {
		type: types.LOAD,
		status
	};
};

/**
 * Gets the value of a session variable. Useful for keeping info about app, page, component states
 * @param {string} id
 * @param {*} value
 * @param {string} component
 * @param {string} page
 */
/**
 *
 * @param {string} id
 * @param {*} value
 * @param {string|undefined} context
 * @return {{type: string, id: string, context: string|undefined, value: *}}
 */
module.exports.setSessionSetting=function({id,
	value,
	context=undefined
}) {
	return {
		type: types.SET_SESSION_SETTING,
		context,
		id,
		value
	};
};
