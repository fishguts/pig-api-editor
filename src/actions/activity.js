/**
 * User: curtis
 * Date: 3/9/18
 * Time: 1:08 AM
 * Copyright @2018 by Xraymen Inc.
 */

const application=require("./application");
const mutators=require("../store/mutators");

const types=exports.types={
	ADD: "ACTIVITY_ADD",
	CLEAR: "ACTIVITY_CLEAR",
	FILTER: "ACTIVITY_FILTER"
};

/* eslint-disable valid-jsdoc */

/**
 * Adds an entry to our console and log
 * @param {Activity} activity
 * @param {Boolean} autoOpen - whether to open the console if it is not currently opened
 */
module.exports.add=function(activity, {autoOpen=true}={}) {
	return (dispatch)=>{
		dispatch({
			type: types.ADD,
			autoOpen,
			activity
		});
		if(autoOpen) {
			dispatch(application.console(true));
		}
		return Promise.resolve();
	};
};

/**
 * Clears all entries from the console, etc.
 */
module.exports.clear=function() {
	return {
		type: types.CLEAR
	};
};

/**
 * Filters activity by severity
 */
module.exports.filter=function({severity=undefined}={}) {
	return {
		type: types.FILTER,
		level: severity,
		filter: (severity!==undefined)
			? mutators.filter.createSeverity(severity, "severity")
			: mutators.filter.createPassThrough()
	};
};
