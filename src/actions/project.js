/**
 * User: curtis
 * Date: 3/9/18
 * Time: 12:37 AM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const assert=require("pig-core").assert;
const log=require("pig-core").log;
const history_actions=require("./history");
const activity_actions=require("./activity");
const constant=require("../common/constant");
const project_io=require("../io/project");
const model_factory=require("../model/factory");
const model_util=require("../model/util");
const mutators=require("../store/mutators");

const types=exports.types={
	// types that act on projects as a group
	LOAD: "PROJECTS_LOAD",
	SORT: "PROJECTS_SORT",
	// project management types
	BUILD: "PROJECT_BUILD",
	CREATE: "PROJECT_CREATE",
	DELETE: "PROJECT_DELETE",
	EDIT: "PROJECT_EDIT",
	RUN: "PROJECT_RUN",
	SAVE: "PROJECT_SAVE",
	SELECT: "PROJECT_SELECT",
	STATUS: "STATUS",
	STOP: "PROJECT_STOP",
	// project edit types
	ADD_DATABASE: "PROJECT_ADD_DATABASE",
	ADD_ENVIRONMENT: "PROJECT_ADD_ENVIRONMENT",
	ADD_MODEL: "PROJECT_ADD_MODEL",
	ADD_MODEL_FIELD: "PROJECT_ADD_MODEL_FIELD",
	ADD_MODEL_VIEW: "PROJECT_ADD_MODEL_VIEW",
	ADD_MODEL_VIEW_REQUEST_FIELD: "PROJECT_ADD_MODEL_VIEW_REQUEST_FIELD",
	ADD_MODEL_VIEW_RESPONSE_FIELD: "PROJECT_ADD_MODEL_VIEW_RESPONSE_FIELD",
	ADD_PROXY: "PROJECT_ADD_PROXY",
	DELETE_DATABASE: "PROJECT_DELETE_DATABASE",
	DELETE_ENVIRONMENT: "PROJECT_DELETE_ENVIRONMENT",
	DELETE_MODEL: "PROJECT_DELETE_MODEL",
	DELETE_MODEL_FIELD: "PROJECT_DELETE_MODEL_FIELD",
	DELETE_MODEL_VIEW: "PROJECT_DELETE_MODEL_VIEW",
	DELETE_MODEL_VIEW_REQUEST_FIELD: "PROJECT_DELETE_MODEL_VIEW_REQUEST_FIELD",
	DELETE_MODEL_VIEW_RESPONSE_FIELD: "PROJECT_DELETE_MODEL_VIEW_RESPONSE_FIELD",
	DELETE_PROXY: "PROJECT_DELETE_PROXY",
	DELETE_VARIABLE: "PROJECT_DELETE_VARIABLE",
	SET_VARIABLE: "PROJECT_SET_VARIABLE",
	SORT_ENVIRONMENTS: "PROJECT_SORT_ENVIRONMENTS",
	SORT_MODEL_FIELDS: "PROJECT_SORT_MODEL_FIELDS",
	SORT_MODEL_VIEW_REQUEST_FIELDS: "PROJECT_MODEL_VIEW_REQUEST_FIELDS",
	SORT_MODEL_VIEW_RESPONSE_FIELDS: "PROJECT_MODEL_VIEW_RESPONSE_FIELDS",
	UPDATE_MODEL: "PROJECT_UPDATE_MODEL",
	UPDATE_DATABASE: "PROJECT_UPDATE_DATABASE",
	UPDATE_ENVIRONMENT: "PROJECT_UPDATE_ENVIRONMENT",
	UPDATE_INFO: "PROJECT_UPDATE_INFO",
	UPDATE_PROJECT: "PROJECT_UPDATE_PROJECT",
	UPDATE_MODEL_FIELD: "PROJECT_UPDATE_MODEL_FIELD",
	UPDATE_MODEL_VIEW: "PROJECT_UPDATE_MODEL_VIEW",
	UPDATE_MODEL_VIEW_REQUEST_FIELD: "PROJECT_UPDATE_MODEL_VIEW_REQUEST_FIELD",
	UPDATE_MODEL_VIEW_RESPONSE_FIELD: "PROJECT_UPDATE_MODEL_VIEW_RESPONSE_FIELD",
	UPDATE_PROXY: "PROJECT_UPDATE_PROXY",
	UPDATE_SERVER: "PROJECT_UPDATE_SERVER"
};

const state={
	/**
	 * Gets gamuts state
	 * @returns {GamutsState}
	 */
	get gamuts() {
		// this guy is in here to avoid circular references.
		const store=require("../store/index");
		return store.getState().gamuts;
	},
	/**
	 * Gets currently selected project
	 * @returns {Project}
	 */
	get project() {
		return state.projects.selected;
	},
	/**
	 * Gets current state of projects
	 * @returns {[Project]}
	 */
	get projects() {
		// this guy is in here to avoid circular references.
		const store=require("../store/index");
		return store.getState().projects;
	},
	/**
	 * Gets the currently logged in user
	 * @returns {User}
	 */
	get user() {
		// this guy is in here to avoid circular references.
		const store=require("../store/index");
		return store.getState().user.loggedIn;
	}
};

const storage={
	/**
	 * We use this guy as a queing mechanism so that we may synchronize operations when deep linking.
	 */
	loadingPromise: Promise.resolve()
};

/* eslint-disable valid-jsdoc */

/****************** PROJECT CRUD, sort and selection actions *******************/
/**
 * Gets all projects belonging to the current user
 */
module.exports.load=function(user=undefined) {
	user=user || state.user;
	return (dispatch)=>{
		// let 'em know we are making the request
		dispatch({
			type: types.LOAD,
			status: model_factory.createStatus({value: constant.status.action.EXECUTE})
		});
		storage.loadingPromise=project_io.load(user)
			.then((projects)=>{
				dispatch({
					type: types.LOAD,
					result: projects,
					status: model_factory.createStatus({value: constant.status.action.SUCCESS})
				});
				// and before we exit let's send off a request to get the status of each of our ravens
				projects.forEach((project)=>dispatch(exports.status(project)));
			})
			.catch((error)=>{
				dispatch({
					type: types.LOAD,
					result: error,
					status: model_factory.createStatus({
						value: constant.status.action.FAILURE,
						error
					})
				});
				dispatch(activity_actions.add(
					model_factory.createActivity({
						message: "Attempt to load projects failed",
						error: error
					})
				));
			});
	};
};

/**
 * Creates a new project for the current user
 */
module.exports.create=function(project=undefined, {
	edit=true,
	save=true
}={}) {
	project=project || model_factory.createProject({user: state.user});
	return (dispatch)=>{
		dispatch({
			type: types.CREATE,
			project: project
		});
		if(edit) {
			dispatch(exports.edit(project));
		}
		if(save) {
			dispatch(exports.save({project, mode: "create"}));
		}
		return Promise.resolve();
	};
};

/**
 * Deletes this specified project
 * @param {Project} project
 * @param {User} user - he will be assigned to the current store state if left undefined
 * @returns {Promise}
 */
module.exports.delete=function(project, user=undefined) {
	user=user || state.user;
	return (dispatch)=>{
		dispatch({
			type: types.DELETE,
			project: project,
			status: model_factory.createStatus({value: constant.status.action.EXECUTE})
		});
		return project_io.delete(project, user)
			.then(()=>{
				dispatch({
					type: types.DELETE,
					project: project,
					status: model_factory.createStatus({value: constant.status.action.SUCCESS})
				});
			})
			.catch((error)=>{
				dispatch({
					type: types.DELETE,
					project: project,
					result: error,
					status: model_factory.createStatus({
						value: constant.status.action.SUCCESS,
						error
					})
				});
				dispatch(activity_actions.add(
					model_factory.createActivity({
						message: `Attempt to delete "${project.name}" failed`,
						error: error
					})
				));
			});
	};
};

/**
 * Sets this guy up for editing.  We anticipate two different sources for this request:
 *  1. this is an internal triggered request to an actual project
 *  2. this is a deep link and the project is a string id
 * @param {Project|string} project
 * @return {function(*)}
 */
module.exports.edit=function(project) {
	if(_.isObject(project)) {
		return history_actions.goToProject(project);
	} else {
		// we are going to make sure that our projects are loaded before we navigate to the project
		return (dispatch)=>{
			return storage.loadingPromise
				.then(()=>{
					if((project=state.projects.find(project))) {
						dispatch({
							type: types.EDIT,
							project: project
						});
					} else {
						log.warn(`actions.project.edit(): project ${project} not found. Redirecting to projects`);
						dispatch(history_actions.goToProjects());
					}
				});
		};
	}
};

/**
 * Saves project to server.  If you don't specify a project then it will save the currently selected one. The reason
 * for this default (and encouraged) behavior behavior is that you will get the absolute latest state...which has been a problem.
 * @param {"create"|"update"} mode
 * @param {Project} project - let it default unless you know better.
 * @param {User} user - he will be assigned to the current store state if left undefined
 * @returns {Promise}
 */
module.exports.save=function({mode, project=undefined, user=undefined}) {
	user=user || state.user;
	project=project || state.project;
	return (dispatch)=>{
		if(!project.dirty) {
			return Promise.resolve();
		} else {
			// let 'em know
			dispatch({
				type: types.SAVE,
				project: project,
				status: model_factory.createStatus({value: constant.status.action.EXECUTE})
			});
			return project_io.save(project, mode, user)
				.then(()=>{
					dispatch({
						type: types.SAVE,
						project: project,
						status: model_factory.createStatus({value: constant.status.action.SUCCESS})
					});
				})
				.catch((error)=>{
					dispatch({
						type: types.SAVE,
						project: project,
						result: error,
						status: model_factory.createStatus({
							value: constant.status.action.FAILURE,
							error
						})
					});
					dispatch(activity_actions.add(
						model_factory.createActivity({
							message: `Attempt to save "${project.name}" failed`,
							error: error
						})
					));
				});
		}
	};
};

/**
 * Selects this project as the current project of interest
 * @param project
 * @returns {{type: string, project: *}}
 */
module.exports.select=function(project) {
	return {
		type: types.SELECT,
		project: project
	};
};

/**
 * @param {string} propertyPath - any property path for a project.
 */
module.exports.sort=function(propertyPath="timestampUpdated:reverse") {
	return {
		type: types.SORT,
		sort: mutators.sort.createByProperty(propertyPath)
	};
};

/********************** PROJECT execution actions **********************/
/**
 * Builds the specified project server-side
 */
module.exports.build=function(project) {
	return (dispatch)=>{
		dispatch({
			type: types.BUILD,
			project: project,
			status: model_factory.createStatus({value: constant.status.build.EXECUTE})
		});
		return project_io.build(project, state.user)
			.then((result)=>{
				const status=model_factory.createStatus({settings: result.build.status});
				dispatch({
					type: types.BUILD,
					// let's make sure the project is up to date
					project: state.projects.find(project.id),
					status: status
				});
				dispatch(exports._statusToActivity(status, `Attempt to build "${project.name}"`));
			})
			.catch((error)=>{
				dispatch({
					type: types.BUILD,
					project: state.projects.find(project.id),
					status: model_factory.createStatus({error})
				});
				dispatch(activity_actions.add(
					model_factory.createActivity({
						message: `Attempt to build "${project.name}" failed`,
						error: error
					})
				));
			});
	};
};

/**
 * Runs this project server-side. Implies a build
 */
module.exports.run=function(project) {
	return (dispatch)=>{
		dispatch({
			type: types.RUN,
			project: project,
			status: model_factory.createStatus({value: constant.status.build.EXECUTE})
		});
		return project_io.run(project, state.user)
			.then((result)=>{
				const status=model_factory.createStatus({settings: result.run.status});
				dispatch({
					type: types.RUN,
					// let's make sure the project is up to date
					project: state.projects.find(project.id),
					status: status
				});
				dispatch(exports._statusToActivity(status, `Attempt to run "${project.name}"`));
			})
			.catch((error)=>{
				dispatch({
					type: types.RUN,
					project: state.projects.find(project.id),
					status: model_factory.createStatus({error})

				});
				dispatch(activity_actions.add(
					model_factory.createActivity({
						message: `Attempt to run "${project.name}" failed`,
						error: error
					})
				));
			});
	};
};

/**
 * Gets the current server-side status of this project
 */
module.exports.status=function(project) {
	return (dispatch)=>{
		dispatch({
			type: types.STATUS,
			project: project,
			status: model_factory.createStatus({value: constant.status.build.EXECUTE})
		});
		return project_io.status(project, state.user)
			.then((result)=>{
				// let's make sure the project is up to date
				project=state.projects.find(project.id);
				dispatch({
					type: types.STATUS,
					project: project,
					status: model_factory.createStatus({value: constant.status.SUCCESS}),
					result: Object.assign({},
						project.status,
						{
							build: model_factory.createStatus({settings: result.build.status}),
							run: model_factory.createStatus({settings: result.run.status}),
							stop: model_factory.createStatus({settings: result.stop.status})
						}
					)
				});
			})
			.catch((error)=>{
				dispatch(activity_actions.add(
					model_factory.createActivity({
						message: `Attempt to get project "${project.name}" status failed`,
						error: error
					})
				));
			});
	};
};

/**
 * Stops the project from running on the server
 */
module.exports.stop=function(project) {
	return (dispatch)=>{
		dispatch({
			type: types.STOP,
			project: project,
			status: model_factory.createStatus({value: constant.status.build.EXECUTE})
		});
		return project_io.stop(project, state.user)
			.then((result)=>{
				const status=model_factory.createStatus({settings: result.stop.status});
				dispatch({
					type: types.STOP,
					// let's make sure the project is up to date
					project: state.projects.find(project.id),
					status: status
				});
				if(_.hasIn(result, "run.status")) {
					dispatch({
						type: types.RUN,
						project: state.projects.find(project.id),
						status: model_factory.createStatus({settings: result.run.status})
					});
				}
				dispatch(exports._statusToActivity(status, `Attempt to stop "${project.name}"`));
			})
			.catch((error)=>{
				dispatch({
					type: types.STOP,
					project: state.projects.find(project.id),
					status: model_factory.createStatus({error})

				});
				dispatch(activity_actions.add(
					model_factory.createActivity({
						message: `Attempt to stop "${project.name}" failed`,
						error: error
					})
				));
			});
	};
};

/****** Project Editing Support: All are applied to the currently selected project ******/
/**
 /**
 * Adds a database to the currently selected project
 */
module.exports.addDatabase=function(database=model_factory.createDatabase()) {
	return {
		type: types.ADD_DATABASE,
		database
	};
};

/**
 * Deletes database from this project
 */
module.exports.deleteDatabase=function(database) {
	return {
		type: types.DELETE_DATABASE,
		database
	};
};

/**
 * Replaces the database with the same Id.  If you have any questions about how immutability works in our
 * model see the notes in the <link>README.md</link>
 */
module.exports.updateDatabase=function(database) {
	return {
		type: types.UPDATE_DATABASE,
		database
	};
};

/**
 * Adds an environment to the currently selected project
 */
module.exports.addEnvironment=function(environment=model_factory.createEnvironment()) {
	return {
		type: types.ADD_ENVIRONMENT,
		environment
	};
};

/**
 * Deletes environment from the currently selected project
 */
module.exports.deleteEnvironment=function(environment) {
	return {
		type: types.DELETE_ENVIRONMENT,
		environment
	};
};

/**
 * Updates the specified project
 */
module.exports.updateEnvironment=function(environment) {
	return {
		type: types.UPDATE_ENVIRONMENT,
		environment
	};
};

/**
 * Sorts environments
 */
module.exports.sortEnvironments=function(environments, property="name", reverse=false) {
	return {
		type: types.SORT_ENVIRONMENTS,
		environments,
		property,
		reverse
	};
};

/**
 * Update project info
 */
module.exports.updateInfo=function(info) {
	return {
		type: types.UPDATE_INFO,
		info: info
	};
};

/**
 * Update project info
 */
module.exports.updateServer=function(server) {
	return {
		type: types.UPDATE_SERVER,
		server: server
	};
};

/**
 * Adds a data-model
 */
module.exports.addDataModel=function(model=model_factory.createDataModel()) {
	return {
		type: types.ADD_MODEL,
		model: model
	};
};

/**
 * Deletes data-model and all of it's associated views
 */
module.exports.deleteDataModel=function(model) {
	return {
		type: types.DELETE_MODEL,
		model: model
	};
};

/**
 * Updates the specified data-model
 */
module.exports.updateDataModel=function(model) {
	return {
		type: types.UPDATE_MODEL,
		model: model
	};
};

/**
 * Adds a field to the specified model.
 */
module.exports.addDataField=function(model, field=model_factory.createDataModelField()) {
	return {
		type: types.ADD_MODEL_FIELD,
		model: model,
		field: field
	};
};

/**
 * Removes field from the model
 */
module.exports.deleteDataField=function(model, field) {
	return {
		type: types.DELETE_MODEL_FIELD,
		model: model,
		field: field
	};
};

/**
 * Updates a data-field over a model
 * @param {DataModel} model
 * @param {DataField} fieldOld
 * @param {DataField} fieldNew
 * @return {*}
 */
module.exports.updateDataField=function(model, fieldOld, fieldNew) {
	if(fieldNew.type===fieldOld.type) {
		return {
			type: types.UPDATE_MODEL_FIELD,
			model: model,
			field: fieldNew
		};
	} else {
		// let's do a little conditioning should his primary type have been converted to an array
		if(fieldNew.type===constant.project.field.type.ARRAY) {
			fieldNew.subtype=fieldNew.subtype || constant.project.field.type.OBJECT;
		}
		// the reason we are making this an async update isn't because we want async behavior but rather because
		// we want to check on request fields default values and make sure they match the new primary type
		return (dispatch)=>{
			dispatch({
				type: types.UPDATE_MODEL_FIELD,
				model: model,
				field: fieldNew
			});
			// we really only have two encodings for default values: array and string
			const typeNew=(fieldNew.type===constant.project.field.type.ARRAY)
				? constant.project.field.type.ARRAY
				: constant.project.field.type.STRING;
			model.views.forEach(view=>{
				/**
				 * Process list of request/response fields and update those who need conversion
				 * @param {Array<DataFieldReference>} fields
				 * @private
				 */
				const _convertDefaults=(fields, factory)=>{
					fields.forEach(field=>{
						if(field.fieldId===fieldNew.id && field.defaultValue!==undefined) {
							if(typeNew===constant.project.field.type.ARRAY) {
								if(_.isArray(field.defaultValue)===false) {
									field=field.clone();
									field.defaultValue=_.split(field.defaultValue, /\s*,\s*/);
									dispatch(factory(field));
								}
							} else {
								// target type is STRING
								if(typeof(field.defaultValue)!=="string") {
									field=field.clone();
									field.defaultValue=_.join(field.defaultValue, ",");
									dispatch(factory(field));
								}
							}
						}
					});
				};
				_convertDefaults(view.requestFields, (field)=>exports.updateDataViewRequestField(model, view, field, false));
				_convertDefaults(view.responseFields, (field)=>exports.updateDataViewResponseField(model, view, field, false));
			});
			return Promise.resolve();
		};
	}
};

/**
 * Sorts model fields as specified
 * @param {DataModel} model
 * @param {string} property
 * @param {Boolean} reverse
 */
module.exports.sortDataFields=function(model, property, reverse=false) {
	return {
		type: types.SORT_MODEL_FIELDS,
		model,
		property,
		reverse
	};
};

/**
 * Adds a data-view to the specified model.
 */
module.exports.addDataView=function(model, view=undefined) {
	view=view || model_factory.createDataView({model});
	return {
		type: types.ADD_MODEL_VIEW,
		model: model,
		view: view
	};
};

/**
 * Removes data-view from the model
 */
module.exports.deleteDataView=function(model, view) {
	return {
		type: types.DELETE_MODEL_VIEW,
		model: model,
		view: view
	};
};

/**
 * Updates a data-view over a model
 */
module.exports.updateDataView=function(model, view) {
	return {
		type: types.UPDATE_MODEL_VIEW,
		model: model,
		view: view
	};
};

/**
 * Adds filter field to view
 * @param {DataModel} model
 * @param {DataView} view
 * @param {DataFieldOperation|undefined} field - if left undefined then this guy will attempt to find an unreferenced field in the model to add
 * @param {Boolean} updateViewPath - whether to automatically apply changes to params that affect
 *   the view path to the view's path
 * @returns {function(*): Promise<void>}
 */
module.exports.addDataViewRequestField=function(model, view,
	field=undefined,
	updateViewPath=true
) {
	if(!field) {
		field=model_factory.createDataModelFieldOperation();
		// Now look for a more intelligent field to reference than the default
		const excludeIds=_.map(view.requestFields, "fieldId").concat(["urn:mdl:fld:*"]),
			excludeTypes=[constant.project.field.type.ARRAY, constant.project.field.type.OBJECT],
			candidates=model_util.arrangeFields({
				excludeIds, model
			}).filter(field=>!_.includes(excludeTypes, field.type));
		field.fieldId=_.get(candidates, "0.id", field.fieldId);
	}
	return (dispatch)=>{
		dispatch({
			type: types.ADD_MODEL_VIEW_REQUEST_FIELD,
			model,
			view,
			field
		});
		if(updateViewPath) {
			const action=exports._updateDataViewPath(model.id, view.id);
			if(action) {
				dispatch(action);
			}
		}
		return Promise.resolve();
	};
};

/**
 * Remove filter field from view
 * @param {DataModel} model
 * @param {DataView} view
 * @param {DataFieldOperation} field
 * @param {Boolean} updateViewPath - whether to automatically apply changes to params that affect
 *   the view path to the view's path
 * @returns {function(*): Promise<void>}
 */
module.exports.deleteDataViewRequestField=function(model, view, field, updateViewPath=true) {
	return (dispatch)=>{
		dispatch({
			type: types.DELETE_MODEL_VIEW_REQUEST_FIELD,
			model,
			view,
			field
		});
		if(updateViewPath) {
			const action=exports._updateDataViewPath(model.id, view.id);
			if(action) {
				dispatch(action);
			}
		}
		return Promise.resolve();
	};
};

/**
 * Updates filter field in view
 * @param {DataModel} model
 * @param {DataView} view
 * @param {DataFieldOperation} field
 * @param {Boolean} updateViewPath - whether to automatically apply changes to params that affect
 *   the view path to the view's path
 * @returns {function(*): Promise<void>}
 */
module.exports.updateDataViewRequestField=function(model, view, field, updateViewPath=true) {
	return (dispatch)=>{
		dispatch({
			type: types.UPDATE_MODEL_VIEW_REQUEST_FIELD,
			model,
			view,
			field
		});
		if(updateViewPath) {
			const action=exports._updateDataViewPath(model.id, view.id);
			if(action) {
				dispatch(action);
			}
		}
		return Promise.resolve();
	};
};

/**
 * Sorts fields as specified
 * @param {DataModel} model
 * @param {DataView} view
 * @param {string} property
 * @param {Boolean} reverse
 * @param {Array} sorted - already sorted fields in which case property and reverse are ignored
 */
module.exports.sortDataViewRequestFields=function({
	model, view,
	property=undefined,
	reverse=false,
	sorted=undefined
}) {
	assert.toLog(property || sorted);
	return {
		type: types.SORT_MODEL_VIEW_REQUEST_FIELDS,
		model,
		view,
		property,
		reverse,
		sorted
	};
};

/**
 * Adds field to view's response list
 * @param {DataModel} model
 * @param {DataView} view
 * @param {DataFieldReference|undefined} field - if left undefined then this guy will attempt to find an unreferenced field in the model to add
 * @return {Object}
 */
module.exports.addDataViewResponseField=function(model, view,
	field=undefined
) {
	if(!field) {
		field=model_factory.createDataModelFieldReference();
		// Now look for a more intelligent field to reference than the default
		const candidates=model_util.arrangeFields({
			excludeIds: _.map(view.responseFields, "fieldId"),
			model: model
		});
		field.fieldId=_.get(candidates, "0.id", field.fieldId);
	}
	return {
		type: types.ADD_MODEL_VIEW_RESPONSE_FIELD,
		model,
		view,
		field
	};
};

/**
 * Remove field from view's response list
 */
module.exports.deleteDataViewResponseField=function(model, view, field) {
	return {
		type: types.DELETE_MODEL_VIEW_RESPONSE_FIELD,
		model,
		view,
		field
	};
};

/**
 * Updates field in view's response list
 */
module.exports.updateDataViewResponseField=function(model, view, field) {
	return {
		type: types.UPDATE_MODEL_VIEW_RESPONSE_FIELD,
		model,
		view,
		field
	};
};

/**
 * Sorts fields as specified
 * @param {DataModel} model
 * @param {DataView} view
 * @param {string} property
 * @param {Boolean} reverse
 */
module.exports.sortDataViewResponseFields=function(model, view, property, reverse=false) {
	return {
		type: types.SORT_MODEL_VIEW_RESPONSE_FIELDS,
		model,
		view,
		property,
		reverse
	};
};

/**
 * Updates the whole project. Only really meant for changes to the tippy top properties such as "targets"...
 * @param {Project} project - should be a clone (and original should still be immutable)
 * @returns {{type: string, project: Object}}
 */
module.exports.updateProject=function(project) {
	return {
		type: types.UPDATE_PROJECT,
		project
	};
};

/**
 * Adds a service proxy to the model
 */
module.exports.addProxy=function(proxy=model_factory.createServiceProxy()) {
	return {
		type: types.ADD_PROXY,
		proxy: proxy
	};
};

/**
 * Deletes service proxy from the model
 */
module.exports.deleteProxy=function(proxy) {
	return {
		type: types.DELETE_PROXY,
		proxy: proxy
	};
};

/**
 * Updates a service proxy on the model
 */
module.exports.updateProxy=function(proxy) {
	return {
		type: types.UPDATE_PROXY,
		proxy: proxy
	};
};

/**
 * Creates a new variable and sets it up in the default environment
 * @param {Variable} variable
 */
module.exports.addVariable=function(variable=model_factory.createVariable()) {
	return {
		type: types.SET_VARIABLE,
		variable: variable
	};
};

/**
 * Sets a global variable or env variable.
 * @param {Variable} variable
 */
module.exports.setVariable=function(variable) {
	return {
		type: types.SET_VARIABLE,
		variable: variable
	};
};

/**
 * Removes variable
 * @param {Variable} variable
 */
module.exports.deleteVariable=function(variable) {
	return {
		type: types.DELETE_VARIABLE,
		variable: variable
	};
};

/********************** Private Interface **********************/
/**
 * Convert a Status's goodies to an Activity instance
 * @param {Status} status
 * @param {string} message
 * @returns {Activity}
 * @private
 */
module.exports._statusToActivity=function(status, message) {
	switch(status.value) {
		case constant.status.NONE:
		case constant.status.SUCCESS: {
			return activity_actions.add(model_factory.createActivity({
				message: `${message} succeeded`,
				severity: constant.severity.INFO,
				stdio: status.stdio
			}));
		}
		case constant.status.EXECUTE: {
			return activity_actions.add(model_factory.createActivity({
				message: `${message} executing`,
				severity: constant.severity.INFO,
				stdio: status.stdio
			}));
		}
		default: {
			return activity_actions.add(model_factory.createActivity({
				message: `${message} failed`,
				severity: constant.severity.ERROR,
				stdio: status.stdio
			}));
		}
	}
};

/**
 * Build a suitable default path for these guys and update if different from existing
 * Note: the reason he takes ids and not objects is so that we are always looking at the current state of the model.
 * @param {string} modelId
 * @param {string} viewId
 * @returns {Object|null} - returns non-null if path has been updated
 * @private
 */
module.exports._updateDataViewPath=function(modelId, viewId) {
	const model=state.project.findModel(modelId),
		view=model.findView(viewId),
		path=model_util.getDataViewPath({
			gamuts: state.gamuts,
			model: state.project.findModel(modelId),
			view: model.findView(viewId)
		});
	if(path && path!==view.path) {
		const clone=view.clone();
		clone.path=path;
		return exports.updateDataView(model, clone);
	}
	return null;
};
