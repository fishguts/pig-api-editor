/**
 * User: curtis
 * Date: 3/18/18
 * Time: 6:23 PM
 * Copyright @2018 by Xraymen Inc.
 */

const activity=require("./activity");
const application=require("./application");
const history=require("./history");
const gamuts=require("./gamuts");
const project=require("./project");
const user=require("./user");

module.exports={
	activity,
	application,
	history,
	gamuts,
	project,
	user
};
