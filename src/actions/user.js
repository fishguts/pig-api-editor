/**
 * User: curtis
 * Date: 3/10/18
 * Time: 8:06 PM
 * Copyright @2018 by Xraymen Inc.
 */

const constant=require("../common/constant");
const activity=require("./activity");
const model_factory=require("../model/factory");

const types=exports.types={
	SHOW: "USER_SHOW",
	LOGIN: "USER_LOGIN",
	LOGOUT: "USER_LOGOUT"
};

/* eslint-disable valid-jsdoc */

/**
 * Attempts to login the specified user
 * @param {User} user
 */
module.exports.login=function(user) {
	return (dispatch)=> {
		dispatch({
			status: constant.status.action.EXECUTE,
			type: types.LOGIN,
			user: user
		});
		// todo: need to do login
		Promise.resolve(model_factory.createUser())
			.then((user)=> {
				dispatch({
					status: model_factory.createStatus({value: constant.status.action.SUCCESS}),
					type: types.LOGIN,
					user: user
				});
			})
			.catch((error)=> {
				dispatch({
					error: error,
					status: model_factory.createStatus({
						value: constant.status.action.FAILURE,
						error
					}),
					type: types.LOGIN,
					user: user
				});
				dispatch(activity.add("Login failed", {error: error}));
			});
	};
};

/**
 * Logs specified user out
 * @param {User} user
 */
module.exports.logout=function(user) {
	return (dispatch)=> {
		dispatch({
			status: model_factory.createStatus({value: constant.status.action.EXECUTE}),
			type: types.LOGOUT,
			user: user
		});
		// todo: need to do logout
		Promise.resolve()
			.then(()=> {
				dispatch({
					status: model_factory.createStatus({value: constant.status.action.SUCCESS}),
					type: types.LOGOUT,
					user: null
				});
			})
			.catch((error)=> {
				dispatch({
					status: model_factory.createStatus({
						value: constant.status.action.FAILURE,
						error
					}),
					type: types.LOGOUT,
					user: user
				});
				dispatch(activity.add("Logout failed", {error: error}));
			});
	};
};

/**
 * event to trigger beginning of login sequence
 */
module.exports.show=function() {
	return {
		type: types.SHOW
	};
};
