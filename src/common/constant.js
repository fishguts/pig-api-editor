/**
 * User: curt
 * Date: 3/5/2018
 * Time: 9:10 PM
 */

const constant=require("pig-core").constant;

/**
 * @type {module:pig-core/constant}
 */
module.exports=Object.assign(module.exports, constant);

/**
 * Known environments.
 */
module.exports.nodenv={
	DEVELOPMENT: "development",
	PRODUCTION: "production"
};
module.exports.isValidNodenv=(value)=>constant.isValidValue(exports.nodenv, value);

/**
 * Our set of constants having to do with a project. We are largely driven by config files.
 * See <link>./res/configuration/model-gamuts.yaml</link> for more options
 */
module.exports.project={
	field: {
		attribute: {
			AUTO_ID: "auto-id",
			MIXED: "mixed",
			REQUIRED: "required"
		},
		propertyOf: {
			// a field found in a model.
			MODEL: "model",
			// A field that applies to queries.
			QUERY: "query",
			// Describes a response property that is not a field in the model such as "count" or "deleted"
			RESULT: "result"
		},
		type: {
			ARRAY: "array",
			BOOLEAN: "boolean",
			DATE: "date",
			ENUM: "enum",
			ID: "id",
			INTEGER: "integer",
			OBJECT: "object",
			NUMBER: "number",
			STRING: "string"
		}
	},
	request: {
		param: {
			location: {
				BODY: "body",
				HEADER: "header",
				LITERAL: "literal",
				PATH: "path",
				QUERY: "query"
			}
		}
	}
};


/**
 * status constants
 */
module.exports.status={
	NONE: null,
	EXECUTE: "execute",
	SUCCESS: "success",
	FAILURE: "failure",

	/**
	 * Various statuses that an async action may have
	 */
	action: {
		EXECUTE: "execute",
		SUCCESS: "success",
		FAILURE: "failure"
	},
	build: {
		NONE: null,
		EXECUTE: "execute",
		SUCCESS: "success",
		FAILURE: "failure"
	},
	run: {
		NONE: null,
		EXECUTE: "execute",
		SUCCESS: "success",
		FAILURE: "failure"
	}
};

/**
 * URN prefix and friends
 */
module.exports.urn={
	type: {
		ACTIVITY: "act",
		DB_INSTANCE: "db",
		DATA_MODEL: "mdl",
		DATA_MODEL_FIELD: "mdl:fld",
		DATA_MODEL_FIELD_REFERENCE: "mdl:fld:ref",
		DATA_MODEL_FIELD_OPERATION: "mdl:fld:op",
		DATA_MODEL_VIEW: "mdl:vwe",
		ENVIRONMENT: "env",
		PROJECT: "prj",
		SPECIFICATION: "spc",
		SVC_INSTANCE: "svc",
		SVC_HTTP: "svc:http",
		SVC_TRANSFORM: "svc:tfm",
		USER: "usr",
		VARIABLE: "var"
	}
};
