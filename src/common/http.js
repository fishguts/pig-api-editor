/**
 * User: curtis
 * Date: 3/12/18
 * Time: 9:50 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const {PigError}=require("pig-core").error;
const http=require("pig-core").http;
const util=require("pig-core").util;

/**
 * @type {module:pig-core/http}
 */
module.exports=Object.assign(module.exports, http);

/**
 * He's a thin abstraction layer from the underlying details of whatever method we use.
 * Note: if the content-type is json then he will parse the response. Otherwise you get everything.
 * @param {string} url
 * @param {Object|string|undefined} body
 * @param {"cache"|"no-cache"} cache
 * @param {string} contentType
 * @param {"DELETE"|"GET"|"PUT"|"POST"|"HEADER"} method
 * @param {Object} options
 * @returns {Promise<Response>}
 * @resolve {{headers:Object, json:Function, ok:boolean, status:Number, statusText:string, text:Function, url:string}}
 */
module.exports.fetch=function(url, {
	body=undefined,
	cache="no-cache",
	contentType="application/json",
	method="GET",
	...options
}={}) {
	options=_.merge(
		util.scrubObject({
			body,
			cache,
			headers: {
				"content-type": contentType
			},
			method
		}, util.isNullOrUndefined),
		options
	);
	if(contentType==="application/json") {
		if(_.isPlainObject(options.body)) {
			options.body=JSON.stringify(options.body);
		}
	}
	// note: I am not clear on how to know the return type here. It doesn't look like the response
	// content-type is in the headers.  The caller knows best what he wants to do with the results.
	// See the <code>@resolve</code> block up above.
	return fetch(url, options)
		.then((result)=>{
			if(result.ok) {
				return result;
			} else {
				throw new PigError({
					details: url,
					statusCode: result.status
				});
			}
		});
};

/**
 * DELETE implementation
 * @param {string} url
 * @param {Object|string|undefined} body
 * @param {Object} options
 * @returns {Promise<Response>}
 */
module.exports.delete=function(url, {body=undefined, ...options}={}) {
	return exports.fetch(url, Object.assign({
		body,
		method: "DELETE"
	}, options));
};

/**
 * GET implementation
 * @param {string} url
 * @param {Object|string|undefined} body
 * @param {"cache"|"no-cache"} cache
 * @param {string} contentType
 * @param {"DELETE"|"GET"|"PUT"|"POST"|"HEADER"} method
 * @param {Object} options
 * @returns {Promise<Response>}
 */
module.exports.get=exports.fetch;


/**
 * PUT implementation
 * @param {string} url
 * @param {Object|string|undefined} body
 * @param {Object} options
 * @returns {Promise<Response>}
 */
module.exports.put=function(url, {body, ...options}) {
	return exports.fetch(url, Object.assign({
		body,
		method: "PUT"
	}, options));
};

/**
 * POST implementation
 * @param {string} url
 * @param {Object|string|undefined} body
 * @param {Object} options
 * @returns {Promise<Response>}
 */
module.exports.post=function(url, {body, ...options}) {
	return exports.fetch(url, Object.assign({
		body,
		method: "POST"
	}, options));
};

