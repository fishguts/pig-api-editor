/**
 * User: curtis
 * Date: 3/13/18
 * Time: 11:35 PM
 * Copyright @2018 by Xraymen Inc.
 */

const log=require("pig-core").log;
const actions=require("../actions/application");
const model_factory=require("../model/factory");

/**
 * @param {ApplicationState} state
 * @param {Object} action
 * @returns {ApplicationState}
 */
module.exports.application=function(state=model_factory.createApplicationState(), action) {
	switch(action.type) {
		case actions.types.CONFIGURE: {
			state=state.clone();
			state.status=action.status;
			log.configure({
				applicationName: state.name,
				configuration: log,
				environment: state.nodenv
			});
			return state;
		}
		case actions.types.CONSOLE_VISIBLE: {
			if(state.consoleVisible!==action.visible) {
				state=state.clone();
				state.consoleVisible=action.visible;
			}
			return state;
		}
		case actions.types.LOAD: {
			// nothing to do here?
			return state;
		}
		case actions.types.SET_SESSION_SETTING: {
			state=state.clone();
			state.setSessionSetting(action);
			return state;
		}
	}
	return state;
};
