/**
 * User: curtis
 * Date: 3/9/18
 * Time: 8:10 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const mutable=require("pig-core").mutation.mutable;
const assert=require("./_assert");
const actions=require("../actions/project");
const constant=require("../common/constant");
const model_factory=require("../model/factory");
const model_util=require("../model/util");


/**
 * Will clone the project and the ProjectsState to retain immutability of both
 * @param {function(project)} dataOperation - function with which you can make your changes to the selected project's clone
 * @param {string} dataPath - clone path within project
 * @param {*} dataValue - optional value that you want to set <code>dataPath</code> to.
 * @param {Boolean} dirty - whether to update the state of <code>project.dirty</code> to true. Also updates the last-updated-timestamp
 * @param {Project} project - will default to currently selected project
 * @param {ProjectsState} projects
 * @returns {ProjectsState} - a clone with the updated project
 * @private
 */
function _updateProject({
	dataPath=null,
	dataValue=undefined,
	dataOperation=undefined,
	dirty=true,
	project=undefined,
	projects
}={}) {
	project=project||projects.selected;
	assert.ok(project);
	// the reason for the following ternary is that we don't need to clone the trailing property in our dataPath if the operation
	// is a clone and set dataValue job. We only need to clone up to the last property and then that property will be replaced.
	project=(_.isEmpty(dataPath) || dataValue===undefined)
		? project.clone({path: dataPath})
		: project.clone({
			path: _(dataPath)
				.split(".")
				.initial()
				.join(".")
		});
	if(dataValue!==undefined) {
		_.set(project, dataPath, dataValue);
	}
	if(dataOperation!==undefined) {
		dataOperation(project);
	}
	return projects.update(project, {dirty});
}


/**
 * Handles actions having to do with project lives
 * @param {ProjectsState} projects
 * @param {Action} action
 * @returns {ProjectsState}
 */
module.exports.projects=function(projects=model_factory.createProjectsState(), action) {
	const verifyImmutable=assert.actionImmutable(action, projects.selected);
	projects=exports._processManage(projects, action);
	projects=exports._processExecute(projects, action);
	projects=exports._processEdit(projects, action);
	verifyImmutable();
	return projects;
};

/**
 * Handles actions having to do with project lives and cohabitation
 * @param {ProjectsState} projects
 * @param {Action} action
 * @returns {ProjectsState}
 * @private
 */
module.exports._processManage=function(projects, action) {
	switch(action.type) {
		case actions.types.LOAD: {
			projects=projects.clone();
			projects.status.load=action.status;
			if(action.status.value===constant.status.action.SUCCESS) {
				projects.set(action.result, {clone: false});
			} else if(action.status.value===constant.status.action.FAILURE) {
				// We updated our project's state.  There is nothing more for us to do here.
			}
			return projects;
		}
		case actions.types.CREATE: {
			return projects.add(action.project);
		}
		case actions.types.DELETE: {
			if(action.status.value===constant.status.action.SUCCESS) {
				projects=projects.remove(action.project);
			}
			return projects;
		}
		case actions.types.EDIT:
		case actions.types.SELECT: {
			projects=projects.clone();
			projects.selected=action.project;
			return projects;
		}
		case actions.types.SAVE: {
			return _updateProject({
				dataPath: "status.save",
				dataOperation: (project)=>{
					project.dirty=false;
					project.status.save=action.status;
				},
				dirty: false,
				projects
			});
		}
		case actions.types.SORT: {
			projects=projects.clone();
			projects.sort=action.sort;
			return projects;
		}
		default: {
			return projects;
		}
	}
};

/**
 * Handles actions having to do with project lives
 * @param {ProjectsState} projects
 * @param {Action} action
 * @returns {ProjectsState}
 * @private
 */
module.exports._processExecute=function(projects, action) {
	switch(action.type) {
		case actions.types.BUILD: {
			return _updateProject({
				dataPath: "status.build",
				dataValue: action.status,
				dirty: false,
				project: action.project,
				projects
			});
		}
		case actions.types.RUN: {
			return _updateProject({
				dataPath: "status.run",
				dataValue: action.status,
				dirty: false,
				project: action.project,
				projects
			});
		}
		case actions.types.STATUS: {
			// this guy is an exception in that we are getting the status of the project.
			// We only want to persist it if the request to get it was successful.
			if(action.status.value===constant.status.SUCCESS) {
				return _updateProject({
					dataPath: "status",
					dataValue: action.result,
					dirty: false,
					project: action.project,
					projects
				});
			}
			return projects;
		}
		case actions.types.STOP: {
			return _updateProject({
				dataPath: "status",
				dataOperation: (project)=>{
					project.status.stop=action.status;
					if(action.status.value===constant.status.action.SUCCESS) {
						project.status.run=model_factory.createStatus();
					}
				},
				dirty: false,
				projects
			});
		}
		default: {
			return projects;
		}
	}
};

/**
 * Handles all actions that act upon a single project.
 * todo: we can replace <code>cloneSelectedProject</code> with <code>_updateSelectedProject</code>
 * @param {ProjectsState} projects
 * @param {Action} action
 * @returns {Project}
 * @private
 */
module.exports._processEdit=function(projects, action) {
	/**
	 * Clones the path of the selected project
	 * @param {string} path
	 * @param {boolean} deep
	 * @return {Project}
	 */
	function cloneSelectedProject({path=null, deep=false}={}) {
		assert.ok(projects.selected);
		return projects.selected.clone({deep, path});
	}

	/**
	 * Gets the index of the object in the container.
	 * @param {Array<{id:string}>} container
	 * @param {{id:string}} object
	 * @return {number}
	 */
	function getObjectIndex(container, object) {
		const index=_.findIndex(container, {id: object.id});
		assert.ok(index>-1);
		return index;
	}

	switch(action.type) {
		case actions.types.UPDATE_PROJECT: {
			return projects.update(action.project);
		}
		case actions.types.UPDATE_INFO: {
			const project=cloneSelectedProject();
			project.info=action.info;
			return projects.update(project);
		}
		case actions.types.UPDATE_SERVER: {
			const project=cloneSelectedProject();
			project.server=action.server;
			return projects.update(project);
		}
		case actions.types.ADD_DATABASE: {
			const project=cloneSelectedProject({path: "databases"});
			project.databases.push(action.database);
			return projects.update(project);
		}
		case actions.types.DELETE_DATABASE: {
			const project=cloneSelectedProject({path: "databases"});
			mutable.array.remove(project.databases, {
				predicate: {id: action.database.id}
			});
			return projects.update(project);
		}
		case actions.types.UPDATE_DATABASE: {
			const project=cloneSelectedProject({path: "databases"});
			mutable.array.replace(project.databases, action.database, {
				predicate: {id: action.database.id}
			});
			return projects.update(project);
		}
		case actions.types.ADD_ENVIRONMENT: {
			const project=cloneSelectedProject({path: "envs"});
			project.envs.push(action.environment);
			return projects.update(project);
		}
		case actions.types.DELETE_ENVIRONMENT: {
			const project=cloneSelectedProject({path: "envs"});
			mutable.array.remove(project.envs, {
				predicate: {id: action.environment.id}
			});
			return projects.update(project);
		}
		case actions.types.UPDATE_ENVIRONMENT: {
			const project=cloneSelectedProject({path: "envs"});
			mutable.array.replace(project.envs, action.environment, {
				predicate: {id: action.environment.id}
			});
			return projects.update(project);
		}
		case actions.types.SORT_ENVIRONMENTS: {
			const project=cloneSelectedProject({path: "envs"});
			mutable.array.sort(project.envs, action.property, {reverse: action.reverse});
			return projects.update(project);
		}
		case actions.types.ADD_MODEL: {
			const project=cloneSelectedProject({path: "models"});
			project.models.push(action.model);
			return projects.update(project);
		}
		case actions.types.DELETE_MODEL: {
			const project=cloneSelectedProject({path: "models"});
			mutable.array.remove(project.models, {
				predicate: {id: action.model.id}
			});
			return projects.update(project);
		}
		case actions.types.UPDATE_MODEL: {
			const project=cloneSelectedProject({path: "models"});
			mutable.array.replace(project.models, action.model, {
				predicate: {id: action.model.id}
			});
			return projects.update(project);
		}
		case actions.types.ADD_MODEL_FIELD: {
			let project=projects.selected;
			const modelIndex=getObjectIndex(project.models, action.model);
			project=project.clone({path: `models.${modelIndex}.fields`});
			project.models[modelIndex].fields.push(action.field);
			return projects.update(project);
		}
		case actions.types.DELETE_MODEL_FIELD: {
			let project=projects.selected;
			const modelIndex=getObjectIndex(project.models, action.model);
			project=project.clone({path: `models.${modelIndex}.fields`});
			mutable.array.remove(project.models[modelIndex].fields, {
				predicate: {id: action.field.id}
			});
			return projects.update(project);
		}
		case actions.types.UPDATE_MODEL_FIELD: {
			let project=projects.selected;
			const modelIndex=getObjectIndex(project.models, action.model);
			project=project.clone({path: `models.${modelIndex}.fields`});
			mutable.array.replace(project.models[modelIndex].fields, action.field, {
				predicate: {id: action.field.id}
			});
			return projects.update(project);
		}
		case actions.types.SORT_MODEL_FIELDS: {
			let project=projects.selected;
			const modelIndex=getObjectIndex(project.models, action.model),
				fieldsPath=`models.${modelIndex}.fields`;
			project=project.clone({path: fieldsPath});
			mutable.array.sort(_.get(project, fieldsPath), action.property, {
				comparer: model_util.fieldNameCompare,
				reverse: action.reverse
			});
			return projects.update(project);
		}
		case actions.types.ADD_MODEL_VIEW: {
			let project=projects.selected;
			const modelIndex=getObjectIndex(project.models, action.model);
			project=project.clone({path: `models.${modelIndex}.views`});
			project.models[modelIndex].views.push(action.view);
			return projects.update(project);
		}
		case actions.types.DELETE_MODEL_VIEW: {
			let project=projects.selected;
			const modelIndex=getObjectIndex(project.models, action.model);
			project=project.clone({path: `models.${modelIndex}.views`});
			mutable.array.remove(project.models[modelIndex].views, {
				predicate: {id: action.view.id}
			});
			return projects.update(project);
		}
		case actions.types.UPDATE_MODEL_VIEW: {
			let project=projects.selected;
			const modelIndex=getObjectIndex(project.models, action.model);
			project=project.clone({path: `models.${modelIndex}.views`});
			mutable.array.replace(project.models[modelIndex].views, action.view, {
				predicate: {id: action.view.id}
			});
			return projects.update(project);
		}
		case actions.types.ADD_MODEL_VIEW_REQUEST_FIELD: {
			let project=projects.selected;
			const modelIndex=getObjectIndex(project.models, action.model),
				viewIndex=getObjectIndex(action.model.views, action.view);
			project=project.clone({path: `models.${modelIndex}.views.${viewIndex}.requestFields`});
			project.models[modelIndex].views[viewIndex].requestFields.push(action.field);
			return projects.update(project);
		}
		case actions.types.DELETE_MODEL_VIEW_REQUEST_FIELD: {
			let project=projects.selected;
			const modelIndex=getObjectIndex(project.models, action.model),
				viewIndex=getObjectIndex(action.model.views, action.view);
			project=project.clone({path: `models.${modelIndex}.views.${viewIndex}.requestFields`});
			mutable.array.remove(project.models[modelIndex].views[viewIndex].requestFields, {
				predicate: {id: action.field.id}
			});
			return projects.update(project);
		}
		case actions.types.UPDATE_MODEL_VIEW_REQUEST_FIELD: {
			let project=projects.selected;
			const modelIndex=getObjectIndex(project.models, action.model),
				viewIndex=getObjectIndex(action.model.views, action.view);
			project=project.clone({path: `models.${modelIndex}.views.${viewIndex}.requestFields`});
			mutable.array.replace(project.models[modelIndex].views[viewIndex].requestFields, action.field, {
				predicate: {id: action.field.id}
			});
			return projects.update(project);
		}
		case actions.types.SORT_MODEL_VIEW_REQUEST_FIELDS: {
			let project=projects.selected;
			const modelIndex=getObjectIndex(project.models, action.model),
				viewIndex=getObjectIndex(action.model.views, action.view),
				fieldsPath=`models.${modelIndex}.views.${viewIndex}.requestFields`;
			project=project.clone({path: fieldsPath});
			if(action.sorted) {
				_.set(project, fieldsPath, action.sorted);
			} else {
				mutable.array.sort(_.get(project, fieldsPath), action.property, {
					comparer: model_util.fieldNameCompare,
					reverse: action.reverse
				});
			}
			return projects.update(project);
		}
		case actions.types.ADD_MODEL_VIEW_RESPONSE_FIELD: {
			let project=projects.selected;
			const modelIndex=getObjectIndex(project.models, action.model),
				viewIndex=getObjectIndex(action.model.views, action.view);
			project=project.clone({path: `models.${modelIndex}.views.${viewIndex}.responseFields`});
			project.models[modelIndex].views[viewIndex].responseFields.push(action.field);
			return projects.update(project);
		}
		case actions.types.DELETE_MODEL_VIEW_RESPONSE_FIELD: {
			let project=projects.selected;
			const modelIndex=getObjectIndex(project.models, action.model),
				viewIndex=getObjectIndex(action.model.views, action.view);
			project=project.clone({path: `models.${modelIndex}.views.${viewIndex}.responseFields`});
			mutable.array.remove(project.models[modelIndex].views[viewIndex].responseFields, {
				predicate: {id: action.field.id}
			});
			return projects.update(project);
		}
		case actions.types.UPDATE_MODEL_VIEW_RESPONSE_FIELD: {
			let project=projects.selected;
			const modelIndex=getObjectIndex(project.models, action.model),
				viewIndex=getObjectIndex(action.model.views, action.view);
			project=project.clone({path: `models.${modelIndex}.views.${viewIndex}.responseFields`});
			mutable.array.replace(project.models[modelIndex].views[viewIndex].responseFields, action.field, {
				predicate: {id: action.field.id}
			});
			return projects.update(project);
		}
		case actions.types.SORT_MODEL_VIEW_RESPONSE_FIELDS: {
			let project=projects.selected;
			const modelIndex=getObjectIndex(project.models, action.model),
				viewIndex=getObjectIndex(action.model.views, action.view),
				fieldsPath=`models.${modelIndex}.views.${viewIndex}.responseFields`;
			project=project.clone({path: fieldsPath});
			mutable.array.sort(_.get(project, fieldsPath), action.property, {
				comparer: model_util.fieldNameCompare,
				reverse: action.reverse
			});
			return projects.update(project);
		}
		case actions.types.ADD_PROXY: {
			const project=cloneSelectedProject({path: "proxies"});
			project.proxies.push(action.proxy);
			return projects.update(project);
		}
		case actions.types.DELETE_PROXY: {
			const project=cloneSelectedProject({path: "proxies"});
			mutable.array.remove(project.proxies, {
				predicate: {id: action.proxy.id}
			});
			return projects.update(project);
		}
		case actions.types.UPDATE_PROXY: {
			const project=cloneSelectedProject({path: "proxies"});
			mutable.array.replace(project.proxies, action.proxy, {
				predicate: {id: action.proxy.id}
			});
			return projects.update(project);
		}
		case actions.types.SET_VARIABLE: {
			const project=cloneSelectedProject({path: "variables", deep: true});
			project.variables.set(action.variable);
			return projects.update(project);
		}
		case actions.types.DELETE_VARIABLE: {
			const project=cloneSelectedProject({path: "variables", deep: true});
			project.variables.delete(action.variable);
			return projects.update(project);
		}
		default: {
			return projects;
		}
	}
};

