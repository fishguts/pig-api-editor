/**
 * User: curtis
 * Date: 5/13/18
 * Time: 5:17 PM
 * Copyright @2018 by Xraymen Inc.
 */

const actions=require("../actions/gamuts");
const model_factory=require("../model/factory");

/**
 * @param {GamutsState} state
 * @param {Object} action
 * @returns {ActivityState}
 */
module.exports.gamuts=function(state=model_factory.createGamutsState(), action) {
	switch(action.type) {
		case actions.types.LOAD: {
			state=action.result||state;
			return state;
		}
		default: {
			return state;
		}
	}
};
