/**
 * User: curtis
 * Date: 3/24/18
 * Time: 1:00 PM
 * Copyright @2018 by Xraymen Inc.
 */

const assert=require("pig-core").assert;
const actions=require("../actions/history");

/**
 * We don't want our components to do navigation. I think that is a worse violation than letting your reducer
 * layer manage history.  So we break the rules. We keep the history object in the store and navigate here
 * as per actions.
 * @param {History} state
 * @param {History} action
 * @returns {*}
 */
module.exports.history=function(state=null, action) {
	switch(action.type) {
		case actions.types.NAVIGATE_BACKWARD: {
			state.goBack();
			return state;
		}
		case actions.types.NAVIGATE_FORWARD: {
			state.goForward();
			break;
		}
		case actions.types.NAVIGATE_TO: {
			assert.ok(state!==null);
			if(action.replace) {
				state.replace(action.path, action.state);
			} else {
				state.push(action.path, action.state);
			}
			return state;
		}
		case actions.types.SET_STATE: {
			return action.history;
		}
		default: {
			return state;
		}
	}
};
