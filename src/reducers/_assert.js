/**
 * User: curtis
 * Date: 8/23/18
 * Time: 9:02 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const assert=require("pig-core").assert;

/**
 * @type {module:pig-core/assert}
 */
module.exports=Object.assign(module.exports, assert);


/**
 * Takes a snapshot of the object and returns a function with which you may check the immutability state of <param>object</param>
 * @param {Object} activity
 * @param {Object} object
 * @returns {function():void}
 */
module.exports.actionImmutable=function(activity, object) {
	// this guy is in here to avoid circular references.
	const store=require("../store/index");
	// and we need to wait until the store is built
	if(!_.isFunction(store.getState)) {
		return _.noop;
	}
	const application=store.getState().application;
	// reassign this guy so that all subsequent interaction is direct
	module.exports.actionImmutable=(!application.debug.enabled)
		? _.noop
		: function(activity, object) {
			const check=assert.immutable(object),
				timestamp=new Date();
			return function() {
				try {
					check();
				} catch(error) {
					// the violation will have been logged. Throw this guy out so that they may investigate the details
					// eslint-disable-next-line no-console
					console.warn(`${timestamp.toISOString()} action=`, object.action);
				}
			};
		};
	return module.exports.actionImmutable(activity, object);
};
