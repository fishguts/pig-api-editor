/**
 * User: curtis
 * Date: 3/17/18
 * Time: 2:51 PM
 * Copyright @2018 by Xraymen Inc.
 */

const actions=require("../actions/user");
const constant=require("../common/constant");
const model_factory=require("../model/factory");

/**
 * @param {UserState} state
 * @param {Object} action
 * @returns {UserState}
 */
module.exports.user=function(state=model_factory.createUserState(), action) {
	switch(action.type) {
		case actions.types.LOGIN: {
			if(action.status.value===constant.status.action.SUCCESS) {
				state=state.clone();
				state.loggedIn=action.user;
				state.status=action.status;
			}
			return state;
		}
		case actions.types.LOGOUT: {
			if(action.status.value===constant.status.action.SUCCESS) {
				state=state.clone();
				state.loggedIn=null;
				state.status=action.status;
			}
			return state;
		}
		case actions.types.SHOW: {
			return state;
		}
		default: {
			return state;
		}
	}
};
