/**
 * User: curtis
 * Date: 3/10/18
 * Time: 7:41 PM
 * Copyright @2018 by Xraymen Inc.
 */

const redux=require("redux");
const {activity}=require("./activity");
const {application}=require("./application");
const {gamuts}=require("./gamuts");
const {history}=require("./history");
const {projects}=require("./project");
const {user}=require("./user");

module.exports=redux.combineReducers({
	application,		// application configuration and state
	activity,			// activity (as in log-like activity) queue
	gamuts,
	history,
	projects,			// all loaded projects
	user
});
