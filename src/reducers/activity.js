/**
 * User: curtis
 * Date: 3/10/18
 * Time: 9:45 PM
 * Copyright @2018 by Xraymen Inc.
 */

const actions=require("../actions/activity");
const model_factory=require("../model/factory");

/**
 * @param {ActivityState} state
 * @param {Object} action
 * @returns {ActivityState}
 */
module.exports.activity=function(state=model_factory.createActivityState(), action) {
	switch(action.type) {
		case actions.types.ADD: {
			state=state.clone();
			state.add(action.activity);
			return state;
		}
		case actions.types.CLEAR: {
			state=state.clone();
			state.all=[];
			state.groomed=[];
			return state;
		}
		case actions.types.FILTER: {
			state=state.clone();
			state.filter=action.filter;
			state.groomed=action.filter.apply(state.all);
			return state;
		}
	}
	return state;
};
