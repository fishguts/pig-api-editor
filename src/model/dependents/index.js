/**
 * User: curtis
 * Date: 5/24/18
 * Time: 8:49 PM
 * Copyright @2018 by Xraymen Inc.
 */

const {ProjectDependencyGraph}=require("./project");

/**
 * Our cache is a simple beast that relies on our immutable rules to be accurate.  Our cache keeps track of
 * values as long as their instance does not change (which should mean that they have not changed)
 * @type {{project: {graph: null, value: null}}}
 */
const cache={
	project: {
		graph: null,
		value: null
	}
};

/**
 * Gets project dependency graph
 * @param {Project} project
 * @returns {ProjectDependencyGraph}
 */
function getProjectDependencyGraph(project) {
	if(cache.project.value!==project) {
		cache.project.graph=new ProjectDependencyGraph(project);
		cache.project.value=project;
	}
	return cache.project.graph;
}

/**
 * We export objects for type assertion
 * @type {{
 * 	getProjectDependencyGraph: (function(Project): (ProjectDependencyGraph)),
 * 	ProjectDependencyGraph: ProjectDependencyGraph
 * }}
 */
module.exports={
	getProjectDependencyGraph,
	ProjectDependencyGraph
};
