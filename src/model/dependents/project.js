/**
 * User: curtis
 * Date: 5/24/18
 * Time: 8:47 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");


/**
 * Tracks all dependencies having to do with a project.
 */
class ProjectDependencyGraph {
	/**
	 * Note: you should not allocate these directly. Go through module access functions.
	 * @param {Project} project
	 */
	constructor(project) {
		this.project=project;
		this._fieldGraph={};
		this._serviceGraph={};
	}

	/**
	 * returns all field-reference dependents on field
	 * @param {Field|string} field
	 * @param {DataModel|string} model
	 * @returns {[{hierarchy:[Object], object:Object, path:string}]}
	 */
	fieldToFieldReferences({field, model=undefined}) {
		const fieldId=_.get(field, "id", field);
		if(!this._fieldGraph.hasOwnProperty(fieldId)) {
			this._fieldGraph[fieldId]=[];
			if((model=model || this._fieldIdToModel(fieldId))) {
				model.views.forEach((view)=> {
					view.requestFields.forEach((request)=> {
						if(request.fieldId===fieldId) {
							this._fieldGraph[fieldId].push({
								hierarchy: [this.project, model, view, request],
								object: request,
								path: `project=${this.project.name}/model=${model.name}/view=${view.getName(model)}/request/field=${field.name}`
							});
						}
					});
					view.responseFields.forEach((response)=> {
						if(response.fieldId===fieldId) {
							// todo: what is of value here? path is essential. Probably want the value as well. The object?
							this._fieldGraph[fieldId].push({
								hierarchy: [this.project, model, view, response],
								object: response,
								path: `project=${this.project.name}/model=${model.name}/view=${view.getName(model)}/response/field=${field.name}`
							});
						}
					});
				});
			}
		}
		return this._fieldGraph[fieldId];
	}

	/**
	 * Finds all objects dependent on the specified database
	 * @param {Database} database
	 * @returns {[{hierarchy:[Object], object:Object, path:string}]}
	 */
	databaseToReferences(database) {
		return this._serviceToReferences(_.get(database, "id", database));
	}

	/**
	 * Finds all objects dependent on the specified proxy service
	 * @param {ServiceProxy} proxy
	 * @returns {[{hierarchy:[Object], object:Object, path:string}]}
	 */
	proxyToReferences(proxy) {
		return this._serviceToReferences(_.get(proxy, "id", proxy));
	}

	/**
	 * Get all references to the specified variable name.
	 * @param {string} name - name without template decorations
	 * @returns {[{path:string, value:string}]}
	 */
	variableToReference(name) {
		const references=[];
		const decorated=`{{${name}}}`;
		const inspections=[
			{value: this.project.info.contact, path: ()=>`project=${this.project.name}/core/contact`},
			{value: this.project.info.desc, path: ()=>`project=${this.project.name}/core/description`},
			{value: this.project.info.name, path: ()=>`project=${this.project.name}/core/name`},
			{value: this.project.info.version, path: ()=>`project=${this.project.name}/core/version`},
			{value: this.project.info.legal.terms.name, path: ()=>`project=${this.project.name}/core/legal/terms/name`},
			{value: this.project.info.legal.terms.url, path: ()=>`project=${this.project.name}/core/legal/terms/url`},
			{value: this.project.info.legal.license.name, path: ()=>`project=${this.project.name}/core/legal/license/name`},
			{value: this.project.info.legal.license.url, path: ()=>`project=${this.project.name}/core/legal/license/url`},
			{value: this.project.server.desc, path: ()=>`project=${this.project.name}/core/server/description`},
			{value: this.project.server.url, path: ()=>`project=${this.project.name}/core/server/url`}
		];
		this.project.databases.forEach((db)=> {
			inspections.push({value: db.connection, path: ()=>`project=${this.project.name}/database=${db.name}/connection`});
			inspections.push({value: db.desc, path: ()=>`project=${this.project.name}/database=${db.name}/description`});
			inspections.push({value: db.name, path: ()=>`project=${this.project.name}/database=${db.name}/name`});
		});
		this.project.models.forEach((model)=> {
			inspections.push({value: model.name, path: ()=>`project=${this.project.name}/model=${model.name}`});
			model.fields.forEach((field)=> {
				inspections.push({value: field.desc, path: ()=>`project=${this.project.name}/model=${model.name}/field=${field.name}/description`});
				inspections.push({value: field.name, path: ()=>`project=${this.project.name}/model=${model.name}/field=${field.name}/name`});
			});
			model.views.forEach((view)=> {
				inspections.push({value: view.desc, path: ()=>`project=${this.project.name}/model=${model.name}/view=${view.getName(model)}/description`});
				inspections.push({value: view.name, path: ()=>`project=${this.project.name}/model=${model.name}/view=${view.getName(model)}/name`});
				inspections.push({value: view.path, path: ()=>`project=${this.project.name}/model=${model.name}/view=${view.getName(model)}/path`});
				view.requestFields.forEach((operation)=> {
					inspections.push({value: operation.variable, path: ()=>`${this.project.name}/model=${model.name}/view=${view.getName(model)}/requestField/variable`});
					inspections.push({value: operation.defaultValue, path: ()=>`${this.project.name}/model=${model.name}/view=${view.getName(model)}/requestField/default-value`});
				});
			});
		});
		inspections.forEach((inspect)=> {
			if(inspect.value && inspect.value.indexOf(decorated)>-1) {
				references.push({
					value: inspect.value,
					path: inspect.path()
				});
			}
		});
		return references;
	}

	/**** Private Interface ****/
	/**
	 * @param {string} fieldId
	 * @returns {DataModel|null}
	 * @private
	 */
	_fieldIdToModel(fieldId) {
		for(let modelIndex=this.project.models.length-1; modelIndex> -1; modelIndex--) {
			let field=this.project.models[modelIndex].findField({id: fieldId});
			if(field) {
				return field;
			}
		}
		return null;
	}

	/**
	 * Finds all objects dependent on the specified service-id
	 * @param {string} serviceId
	 * @returns {[{hierarchy:[Object], object:Object, path:string}]}
	 * @private
	 */
	_serviceToReferences(serviceId) {
		if(!this._serviceGraph.hasOwnProperty(serviceId)) {
			this._serviceGraph[serviceId]=[];
			this.project.models.forEach((model)=> {
				model.views.forEach((view)=> {
					if(view.serviceId===serviceId) {
						// todo: what is of value here? path is essential. Probably want the value as well. The object?
						this._serviceGraph[serviceId].push({
							hierarchy: [this.project, model, view],
							object: view,
							path: `project=${this.project.name}/model=${model.name}/view=${view.getName(model)}`
						});
					}
				});
			});
		}
		return this._serviceGraph[serviceId];
	}
}

module.exports={
	ProjectDependencyGraph
};
