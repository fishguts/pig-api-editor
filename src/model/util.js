/**
 * User: curtis
 * Date: 6/27/18
 * Time: 6:28 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const assert=require("pig-core").assert;
const immutable=require("pig-core").mutation.immutable;
const log=require("pig-core").log;
const util=require("pig-core").util;
const model_factory=require("./factory");
const activity_actions=require("../actions/activity");
const constants=require("../common/constant");

/**
 * Simply manages a consistent presentation of any combination of these guys which involves our own custom
 * sort of fields as well as arranging these two guys together.
 * @param {Array<string>} excludeIds
 * @param {GamutsState|undefined} gamuts - will include.gamuts.fields if specified. Note: there is more than one field
 * 	type in gamuts.fields. We don't filter here but you may filter the results if you like.
 * @param {DataModel|undefined} model - will include model.fields if specified.
 * @returns {Array<DataField>}
 */
module.exports.arrangeFields=function({
	excludeIds=[],
	gamuts=undefined,
	model=undefined
}) {
	let result=[];
	[model, gamuts].forEach(object=>{
		if(object) {
			result=result.concat(immutable.array.sort(object.fields, "name", {
				comparer: exports.fieldNameCompare
			}));
		}
	});
	if(excludeIds.length>0) {
		_.remove(result, (field)=>_.includes(excludeIds, field.id));
	}
	return result;
};

/**
 * Find a field by <param>id</param> or <param>name</param>
 * @param {Array<DataField>|undefined} fields - optional source of fields. Must specify fields and/or gamuts and/or model
 * @param {GamutsState|undefined} gamuts - optional source of fields. Must specify fields and/or gamuts and/or model
 * @param {DataModel|undefined} model - optional source of fields. Must specify fields and/or gamuts and/or model
 * @param {Function|null} dispatch - by default this guy will get dispatch directly from the store and record errors.
 * @param {string|undefined} id - optional but must specify id or name
 * @param {string|undefined} name - optional but must specify id or name
 * @returns {DataField|null}
 */
module.exports.findField=function({
	fields=undefined,
	gamuts=undefined,
	model=undefined,
	dispatch=require("../store/index").dispatch,
	id=undefined,
	name=undefined
}) {
	assert.ok(fields || gamuts || model);
	assert.ok(id || name);
	fields=(fields||[])
		.concat(model ? model.fields : [])
		.concat(gamuts ? gamuts.fields : []);
	const result=(id)
		? fields.find(field=>field.id===id)
		: fields.find(field=>field.name===name);
	if(!result) {
		const activity=model_factory.createActivity({
			details: `field ${(id) ? `id=${id}` : `name='${name}'`} cound not be found`,
			message: "Unexpectedly unable to find field",
			severity: constants.severity.ERROR
		});
		if(dispatch) {
			dispatch(activity_actions.add(activity));
		} else {
			log.error(activity.message, {meta: activity});
		}
	}
	return result;
};

/**
 * Compares two field names and applies some of our special conditions
 * @param {string} s1
 * @param {string} s2
 * @return {Boolean}
 */
module.exports.fieldNameCompare=function(s1, s2) {
	const dfault=util.compareStrings(s1, s2, {ignoreCase: true});
	if(dfault!==0 && s1 && s2) {
		if(_.endsWith(s1, "*")) {
			if(s2.startsWith(s1.substr(0, s1.length-1))) {
				return -1;
			}
		}
		if(_.endsWith(s2, "*")) {
			if(s1.startsWith(s2.substr(0, s2.length-1))) {
				return 1;
			}
		}
	}
	return dfault;
};

/**
 * Derives a default variable name for this field. We leave wiggle room in case we need to get fancy
 * such as ensure uniqueness
 * Note: this logic is shared in our server.
 * @param {string} name
 * @returns {string}
 */
module.exports.fieldNameToVariableName=function(name) {
	return (name)
		? _.chain(name)
			.split(".")
			.last()
			.value()
		: null;
};


/**
 * Derives a default variable name for this field. We have it here in the model so that we can verify that it
 * is unique (should we ever need to do so)
 * Note: this logic is shared in our server.
 * @param {string} fieldId
 * @param {GamutsState} gamuts
 * @param {DataModel} model
 * @returns {string}
 */
module.exports.fieldIdToVariableName=function({fieldId, gamuts, model}) {
	const field=exports.findField({
		model, gamuts,
		id: fieldId
	});
	return (field)
		? exports.fieldNameToVariableName(field.name)
		: null;
};

/**
 * Looks at the view'ss path and compares it with its filter params and makes sure that all params are accounted for in the path.
 * @param {GamutsState} gamuts
 * @param {DataModel} model
 * @param {DataView} view
 * @returns {String|null} - that may be dispatched.
 * @private
 */
module.exports.getDataViewPath=function({gamuts, model, view}) {
	try {
		// todo: how do we want to manage the order?
		const fields=view.requestFields
			.filter((field)=>field.location==="path");
		let path=view.path,
			existing=(path.match(/({[^{}]+})(?:[^}]|$)/g) || []);
		// 1. look through all those we expect to exist and add those that we don't already have in our path
		fields.forEach((field)=> {
			const name=field.variable || exports.fieldIdToVariableName({
					fieldId: field.fieldId,
					model,
					gamuts: gamuts
				}),
				param=`{${name}}`,
				index=_.findIndex(existing, param);
			if(index> -1) {
				// he exists. Let's remove the guy from our todo list
				existing.splice(index, 1);
			} else {
				path+=`/${param}`;
			}
		});
		// 2. remove the left overs
		existing.forEach((param)=> {
			path=path.replace(param, "");
		});
		return path.replace(/\/\//g, "/")
			.replace(/\/$/, "");
	} catch(error) {
		log.error(error);
		return null;
	}
};

