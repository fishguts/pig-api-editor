/**
 * User: curtis
 * Date: 2/14/18
 * Time: 1:18 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const assert=require("pig-core").assert;

/**
 * Base class. All work with blob of data.
 */
class Model {
	/**
	 * @param {Object} data - this is what we consider "permanent" storage which means it will be included in <code>raw<code>
	 * @param {Object} session - this is session data and will not be included in <code>raw</code>
	 */
	constructor(data, session=undefined) {
		this._data=data;
		this._session=session;
	}

	/**
	 * Gets a raw object that represents this object. Represents? Yes, it looks for objects that are of type <code>Model</code>
	 * and when it finds them it recursively calls <code>raw</code> so that we don't get our internal containment properties in the mix (_data).
	 * @returns {Object}
	 */
	get raw() {
		return _.mapValues(this._data, (value)=> {
			// note: this doesn't support array of array of Model objects. If this becomes a problem then fix it
			if(_.isArray(value)) {
				return value.map((element)=> {
					return (element instanceof Model)
						? element.raw
						: element;
				});
			} else {
				return (value instanceof Model)
					? value.raw
					: value;
			}
		});
	}

	/**
	 * Clones the original. Either does a deep clone or clones the specified data path
	 * @param {string} path - optional path of the selective hierarchy of objects you would like to clone
	 * @param {Boolean} deep
	 * @returns {exports.Model}
	 */
	clone({path=null, deep=false}={}) {
		const result=new this.constructor();
		if(this._session!=null) {
			result._session=(deep)
				? _.cloneDeep(this._session)
				: _.clone(this._session);
		}
		if(_.isEmpty(path)) {
			result._data=(deep)
				? _.cloneDeep(this._data)
				: _.clone(this._data);
		} else {
			result._data=_.clone(this._data);
			let parts=path.split("."),
				// we want the fork that has the next property. It should either be in _data or in _session.
				parent=result._data.hasOwnProperty(parts[0])
					? result._data
					: result._session;
			// work through everything that is not an instance of Model
			while(!(parent[parts[0]] instanceof Model)) {
				parent[parts[0]]=_.clone(parent[parts[0]]);
				parent=parent[parts[0]];
				parts.shift();
				if(parts.length===0) {
					return result;
				}
			}
			if(parent[parts[0]] instanceof Model) {
				parent[parts[0]]=parent[parts[0]].clone({path: parts.slice(1).join("."), deep: deep});
			} else {
				assert.ok(false, "should not be able to get here?");
			}
		}
		return result;
	}

	/**
	 * Whether this object is trying to be the same object as the other.  We are hoping for an id.  But if
	 * we cannot find one then we do a deep comparision.
	 * @param {Object} other
	 * @returns {Boolean}
	 */
	isEquivelant(other) {
		if(!(other instanceof Model)) {
			return false;
		} else if(this._data.hasOwnProperty("id") && other._data.hasOwnProperty("id")) {
			return this._data.id===other._data.id;
		} else {
			return _.isEqual(this.raw(), other.raw());
		}
	}
}

module.exports.Model=Model;
