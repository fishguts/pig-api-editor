/**
 * User: curtis
 * Date: 5/15/18
 * Time: 12:52 AM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");


/**
 * Determines whether object is a model object or an id and returns an id or null
 * @param {Object|string} model
 * @returns {Object|null}
 */
const modelToId=module.exports.modelToId=(model)=>_.isObject(model) ? model.id : model||null;

/**
 * Are these two model-objects/ids the same in terms of id
 * @param {Object|string} model1
 * @param {Object|string} model2
 * @returns {Boolean}
 */
const isEqualById=module.exports.isEqualById=(model1, model2)=>modelToId(model1)===modelToId(model2);

/**
 * Find
 * @param {Object|string} model - id or model object to search for (by id)
 * @param {Array} containers - one or more containers to search in if not found in container1
 * @returns {Object|null}
 */
module.exports.findById=(model, ...containers)=>{
	for(let index1=0; index1<containers.length; index1++) {
		for(let index2=0; index2<containers[index1].length; index2++) {
			if(isEqualById(model, containers[index1][index2])) {
				return containers[index1][index2];
			}
		}
	}
	return null;
};
