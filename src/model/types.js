/**
 * User: curtis
 * Date: 10/20/18
 * Time: 6:55 PM
 * Copyright @2018 by Xraymen Inc.
 *
 * A collection of JSDoc definitions of various types that we use that do not define themselves
 */


/**
 * A type that is used predominantly where we want be able to plop lists in list/selection controls. But its use
 * extends beyond client code as we use this format in our <link>res/configuration/model-gamuts.yaml</link>
 * @typedef {Array<{text:string, value:string}>} ControlItemList
 */

/**
 * A type the describes the allowed values for a field.
 * @typedef {{regex:(undefined|string), values:(undefined|Array<string>)}} PigFieldAllowed
 */

/**
 * All known pig types that a field type may be. Definitive source is <link>res/configuration/model-gamuts.yaml</link>
 * @typedef {"array"|"boolean"|"date"|"enum"|"id"|"integer"|"object"|"number"|"string"} PigFieldType
 */

/**
 * All functions that may be assigned to a view. Definitive source is <link>res/configuration/model-gamuts.yaml</link>
 * @typedef {"count"|"create-one"|"create-many"|"delete-one"|"delete-many"|"distinct"|"get-one"|"get-many"|"update-one"} PigFunction
 */

/**
 * All operations that may be assigned to a param. Definitive source is <link>res/configuration/model-gamuts.yaml</link>
 * @typedef {"set"|"eq"|"ne"|"lt"|"lte"|"gt"|"gte"} PigOperation
 */

/**
 * Typedef for a validation function. Should return null (or any falsey condition) if okay, otherwise an Error instance.
 * Note: the reason we settled on <code>null</code> as "valid" is so that we can chain conditions and not have to differentiate between
 * 	two different truthy conditions (Error and true)
 * @typedef {function(*):(null|Error)} PigValidationFunction
 */

/**
 * Description of our <link>res/configuration/model-gamuts.yaml</link.
 * @typedef {Object} PigGamuts
 * @property {Object} database
 * @property {ControlItemList} database.types
 * @property {Object} log
 * @property {ControlItemList} log.level
 * @property {Object} model
 * @property {Object} model.field
 * @property {ControlItemList} model.field.attributes
 * @property {ControlItemList} model.field.types
 * @property {Array<DataField>} model.fields
 * @property {Array<DataFieldReference|DataFieldOperation>} model.fieldReferences
 * @property {Array<ControlItemList>} model.operators
 * @property {Object<string, {name:string}>} model.transform
 * @property {Object} model.view
 * @property {ControlItemList} model.view.functions
 * @property {Object} request
 * @property {Object} request.param
 * @property {ControlItemList} request.param.encoding
 * @property {ControlItemList} request.param.location
 * @property {ControlItemList} targets
 * @property {Object} transport
 * @property {ControlItemList} transport.types
 * @property {Object} variable
 * @property {ControlItemList} variable.scope
 */
