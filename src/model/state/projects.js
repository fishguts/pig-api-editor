/**
 * User: curtis
 * Date: 3/10/18
 * Time: 11:26 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const mutable=require("pig-core").mutation.mutable;
const {Model}=require("../_base");

/**
 * @typedef {Model} ProjectsState
 */
class ProjectsState extends Model {
	/**
	 * This is the unaltered list of projects
	 * @returns {[Project]}
	 */
	get all() {return this._data.all;}
	/**
	 * @param {[Project]} value
	 */
	set all(value) {this._data.all=value;}

	/**
	 * This is the filtered and sorted list of projects. We do it on the fly so that we don't
	 * have to manage his nonsense
	 * @returns {[Project]}
	 */
	get groomed() {
		return this.sort.apply(this.filter.apply(this._data.all));
	}

	/**
	 * Gets and sets the currently selected project. We keep it in session data.
	 * @returns {Project}
	 */
	get selected() {return this._data.selected;}
	/**
	 * @param {Project} value
	 */
	set selected(value) {this._data.selected=value;}


	/**
	 * @returns {{add:Function, apply:Function}}
	 */
	get filter() {return this._data.filter;}
	/**
	 * @param {{add:Function, apply:Function}} value
	 */
	set filter(value) {this._data.filter=value;}

	/**
	 * @returns {{property:string, apply:Function}}
	 */
	get sort() {return this._data.sort;}
	/**
	 * @param {{property:string, apply:Function}} value
	 */
	set sort(value) {this._data.sort=value;}

	/**
	 * Get reference to status. If you want to update it then set the status objects within.
	 * Be sure to follow immutability protocol.
	 * @returns {{load:Status}}
	 */
	get status() {return this._data.status;}

	/**
	 * Adds project to <code>all</code> and to our <code>groomed</code> list and will sort if need be
	 * @param {Project|[Project]} project
	 * @param {Boolean} clone whether you want us to make sure that the current instance is unscathed
	 * @param {Boolean} select set as currently selected project
	 * @returns {exports.ProjectsState}
	 */
	add(project, {clone=true, select=true}={}) {
		const result=(clone)
			? this.clone()
			: this;
		result._data.all=result._data.all.concat(project);
		if(select) {
			result.selected=_.isArray(project) ? project[0] : project;
		}
		return result;
	}

	/**
	 * Finds project
	 * @param {string} projectId
	 * @return {Project|null}
	 */
	find(projectId) {
		return _.find(this._data.all, {id: projectId});
	}

	/**
	 * Removes this project from the collection and updates selected if he was the chosen one
	 * @param {Project} project
	 * @returns {exports.ProjectsState}
	 * @param {Boolean} clone whether you want us to make sure that the current instance is unscathed
	 */
	remove(project, {clone=true}={}) {
		const result=(clone)
			? this.clone()
			: this;
		const groomed=result.groomed,
			index=_.findIndex(groomed, {id: project.id});
		result._data.all=mutable.array.remove(result._data.all, {predicate: {id: project.id}});
		if(project.isEquivelant(result.selected)) {
			result.selected=(index<groomed.length-1)
				? groomed[index]
				: (index>0) ? groomed[index-1] : null;
		}
		return result;
	}

	/**
	 * Updates the project with the same id.
	 * This makes sure that both his existence as selected and in the array are properly updated.
	 * @param {Project} project
	 * @param {Boolean} clone - whether you want us to make sure that the current instance is unscathed
	 * @param {Boolean} dirty - whether this update should cause the project to be considered <code>dirty</code>
	 * 	This will cause the last-updated-timestamp to be updated. Note: the only time that you might not want
	 * 	dirty to be set is when updating session data.
	 * @returns {exports.ProjectsState}
	 */
	update(project, {
		clone=true,
		dirty=true
	}={}) {
		const result=(clone)
			? this.clone()
			: this;
		mutable.array.replace(result._data.all, project, {predicate: {id: project.id}});
		if(project.isEquivelant(result.selected)) {
			result.selected=project;
		}
		if(dirty) {
			project.dirty=true;
			project.timestampUpdated=new Date();
		}
		return result;
	}

	/**
	 * Resets this instance with these projects
	 * @param {Project} projects
	 * @param {Boolean} clone
	 * @param {Boolean} select
	 * @returns {Project}
	 */
	set(projects, {clone=true, select=true}={}) {
		const result=(clone)
			? this.clone()
			: this;
		result._data.all=[];
		result.selected=null;
		return result.add(projects, {clone: false, select});
	}
}

module.exports.ProjectsState=ProjectsState;
