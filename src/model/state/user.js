/**
 * User: curtis
 * Date: 3/7/18
 * Time: 4:16 PM
 * Copyright @2018 by Xraymen Inc.
 */

const {Model}=require("../_base");

/**
 * @typedef {Model} UserState
 */
class UserState extends Model {
	get loggedIn() {return this._data.loggedIn;}
	set loggedIn(value) {this._data.loggedIn=value;}

	/**
	 * Get/set login status
	 * @returns {Status}
	 */
	get status() {return this._data.status;}
	set status(value) {this._data.status=value;}
}

module.exports.UserState=UserState;
