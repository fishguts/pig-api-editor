/**
 * User: curtis
 * Date: 3/10/18
 * Time: 9:24 PM
 * Copyright @2018 by Xraymen Inc.
 */

const {Model}=require("../_base");

/**
 * @typedef {Model} ActivityState
 */
class ActivityState extends Model {
	/**
	 * This is the unaltered list of activity
	 * @returns {[ActivityComponent]}
	 */
	get all() {return this._data.all;}
	/**
	 * @param {[ActivityComponent]} value
	 */
	set all(value) {this._data.all=value;}

	/**
	 * This is the filtered and sorted list of projects
	 * @returns {[ActivityComponent]}
	 */
	get groomed() {return this._data.groomed;}
	/**
	 * @param {[ActivityComponent]} value
	 */
	set groomed(value) {this._data.groomed=value;}

	/**
	 * @returns {{add:Function, apply:Function}}
	 */
	get filter() {return this._data.filter;}
	/**
	 * @param {{add:Function, apply:Function}} value
	 */
	set filter(value) {this._data.filter=value;}

	/**
	 * @returns {{apply:Function, property:string}}
	 */
	get sort() {return this._data.sort;}
	/**
	 * @param {Function} value
	 */
	set sort(value) {this._data.sort=value;}

	/**
	 * Adds activity to <code>all</code> and to our <code>groomed</code> list and will sort if need be
	 * @param {ActivityComponent} activity
	 */
	add(activity) {
		const oldGroomed=this._data.groomed;
		this._data.all=this._data.all.concat(activity);
		this._data.groomed=this._data.filter.add(this._data.groomed, activity);
		// if no filtering was applied then we don't need to sort
		if(this._data.groomed!==oldGroomed) {
			this._data.groomed=this._data.sort.apply(this._data.groomed);
		}
	}
}

module.exports.ActivityState=ActivityState;
