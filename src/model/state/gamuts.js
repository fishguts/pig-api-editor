/**
 * User: curtis
 * Date: 5/13/2018
 * Time: 5:23 PM
 * Copyright @2018 by Xraymen Inc.
 */

const factory=require("../factory");
const constant=require("../../common/constant");

/**
 * This guy does not get updated so we don't create a model about around him. We export the gamuts properties directly.
 * See <link>res/configuration/model-gamuts.yaml</link> for details
 * @typedef {PigGamuts} GamutsState
 */
class GamutsState {
	constructor(data) {
		Object.assign(this, data);
		this._session={};
	}

	/**
	 * Returns an array of our very own field library
	 * @returns {Array<DataField>}
	 */
	get fields() {
		if(!this._session.fields) {
			this._session.fields=this.model.fields
				.map(factory.createDataModelField);
		}
		return this._session.fields;
	}

	/**
	 * Returns an array of cached DataFieldOperation
	 * @returns {Array<DataFieldReference>}
	 */
	get queryFields() {
		if(!this._session.queryFields) {
			this._session.queryFields=this.model.fieldReferences
				.filter(field=>field.propertyOf===constant.project.field.propertyOf.QUERY)
				.map(factory.createDataQueryFieldOperation);
		}
		return this._session.queryFields;
	}

	/**
	 * Returns an array of cached DataFieldOperation
	 * @returns {Array<DataFieldOperation>}
	 */
	get resultFields() {
		if(!this._session.resultFields) {
			this._session.resultFields=this.model.fieldReferences
				.filter(field=>field.propertyOf===constant.project.field.propertyOf.RESULT)
				.map(factory.createDataResultFieldReference);
		}
		return this._session.resultFields;
	}
}

exports.GamutsState=GamutsState;
