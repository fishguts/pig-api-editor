/**
 * User: curtis
 * Date: 11/10/17
 * Time: 10:49 PM
 * Copyright @2017 by Xraymen Inc.
 */

const {Model}=require("../_base");

/**
 * Application information. Largely a server for our application configuration info with an application jobQueue.
 * @typedef {Model} ApplicationState
 */
class ApplicationState extends Model {
	constructor(data) {
		super(data, {});
	}

	/**** Application Environment Information ****/
	/**
	 * NODE_ENV or our own default
	 * @returns {string}
	 */
	get nodenv() { return this._data.environment.name; }
	get isLowerEnv() { return this._data.environment.isLower; }
	get isUpperEnv() { return !this._data.environment.isLower; }

	/**
	 * Debug information
	 * @returns {{enabled:Boolean, verbose:Boolean}}
	 */
	get debug() { return this._data.debug; }

	/**
	 * Log configuration information
	 * @returns {{level:string}}
	 */
	get log() { return this._data.log; }

	/**
	 * Name of the project
	 */
	get name() { return this._data.info.name; }

	/**
	 * Description of the project
	 * @returns {string}
	 */
	get desc() { return this._data.info.desc; }

	/**
	 * @returns {string}
	 */
	get version() { return this._data.info.version; }

	/**
	 * Our application server
	 * @returns {{host:string, port:Number}}
	 */
	get server() { return this._data.server; }

	/**
	 * Get/set the applications startup status.
	 * @returns {Status}
	 */
	get status() {return this._data.status;}
	set status(value) {this._data.status=value;}

	/**** Application State ****/
	/**
	 * @return {Boolean}
	 */
	get consoleVisible() {
		return this.getSessionSetting({
			id: "console.visible",
			dfault: false
		});
	}
	/**
	 * @param {Boolean} value
	 */
	set consoleVisible(value) {
		return this.setSessionSetting({
			id: "console.visible",
			value
		});
	}

	/**
	 * Gets the value of a session variable. Useful for keeping info about app, page, component states
	 * @param {string} id
	 * @param {string} context - this is a way to sub-categorize the id. For example: sort, selected, etc.
	 * @param {*} dfault - optional default to return if not found
	 * @returns {*|undefined}
	 */
	getSessionSetting({id,
		context="general",
		dfault=undefined
	}) {
		const property=`settings.${context}.${id}`;
		return this._session.hasOwnProperty(property)
			? this._session[property]
			: dfault;
	}

	/**
	 * Gets the value of a session variable. Useful for keeping info about app, page, component states
	 * @param {string} id
	 * @param {string} context - this is a way to sub-categorize the id. For example: sort, selected, etc.
	 * @param {*} value
	 */
	setSessionSetting({id,
		value,
		context="general"
	}) {
		this._session[`settings.${context}.${id}`]=value;
	}
}

module.exports.ApplicationState=ApplicationState;
