/**
 * User: curtis
 * Date: 3/5/18
 * Time: 9:46 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const assert=require("pig-core").assert;
const date_codec=require("pig-core").date;
const urn=require("pig-core").urn;
const objects=require("./objects");
const state=require("./state");
const constant=require("../common/constant");
const mutators=require("../store/mutators");

let model_defaults, model_gamuts;

// we don't want to have to transpile and build the project to test this stuff
// so if webpack loaders have not already converted this guy then we do it manually
try {
	model_defaults=require("json-loader!yaml-loader!../../res/configuration/model-defaults.yaml");
	model_gamuts=require("json-loader!yaml-loader!../../res/configuration/model-gamuts.yaml");
} catch(e) {
	const file=require("pig-core").file;
	model_defaults=file.readToJSONSync("./res/configuration/model-defaults.yaml");
	model_gamuts=file.readToJSONSync("./res/configuration/model-gamuts.yaml");
}

/**************** Internal Interface ****************/
/**
 * We periodically use a little trickery within our model-defaults configuration and that is refer
 * to to gamut properties using the following convention <code>_gamutsId</code>
 * @param {Object} settings
 * @returns {Object}
 * @private
 */
function _resolveGamutsReferences(settings) {
	if(settings._gamuts) {
		let result=_.get(model_gamuts, settings._gamuts.path);
		if(settings._gamuts.query) {
			result=_.find(result, settings._gamuts.query);
		}
		assert.toLog(result, ()=>`unable to find ${JSON.stringify(settings)}`);
		return result;
	}
	return settings;
}

/**************** Public Interface ****************/
/**
 * @param {string} message
 * @param {Array|string} details
 * @param {constant.severity} severity
 * @param {string} stack
 * @param {Integer} statusCode
 * @param {{out:Array, err:Array}} stdio
 * @returns {ActivityComponent}
 */
module.exports.createActivity=function({
	message,
	id=urn.create(constant.urn.type.ACTIVITY),
	details=undefined,
	error=undefined,
	severity=undefined,
	statusCode=undefined,
	stdio=undefined
}) {
	let stack;
	if(error) {
		assert.ok(details===undefined);
		details=error.message;
		stack=error.stack;
		severity=severity||constant.severity.ERROR;
		statusCode=statusCode||error.statusCode;
	} else if(severity===undefined) {
		severity=constant.severity.INFO;
	}
	return new objects.Activity({
		id,
		details,
		message,
		severity,
		stack,
		statusCode,
		stdio,
		timestamp: new Date()
	});
};

/**
 * @param {Object} settings
 * @returns {Database}
 */
module.exports.createDatabase=function(settings=undefined) {
	if(settings) {
		return new objects.Database(Object.assign({
			id: urn.create(constant.urn.type.DB_INSTANCE)
		}, settings));
	} else {
		return new objects.Database(_.merge({},
			model_defaults.database,
			{id: urn.create(model_defaults.database.id)}
		));
	}
};

/**
 * @param {Object} settings
 * @returns {Environment}
 */
module.exports.createEnvironment=function(settings=undefined) {
	if(settings) {
		// we always assign a unique id to make sure one exists
		return new objects.Environment(Object.assign({
			id: urn.create(constant.urn.type.ENVIRONMENT)
		}, settings));
	} else {
		return new objects.Environment(Object.assign({
			id: urn.create(constant.urn.type.ENVIRONMENT)
		}, model_defaults.environment));
	}
};

/**
 * @param {Object} settings
 * @returns {[Environment]}
 */
module.exports.createEnvironments=function(settings=undefined) {
	return (settings)
		? _.map(settings, exports.createEnvironment)
		: model_defaults.environments.map((environment)=>new objects.Environment(_.merge({}, environment)));
};

/**
 * @param {Object} settings
 * @returns {DataModel}
 */
module.exports.createDataModel=function(settings=undefined) {
	let result;
	if(settings) {
		result=new objects.DataModel(Object.assign({
			id: urn.create(constant.urn.type.DATA_MODEL)
		}, settings));
	} else {
		result=new objects.DataModel(_.merge({
			id: urn.create(constant.urn.type.DATA_MODEL)
		}, model_defaults.dataModel));
	}
	// convert whatever we caught into proper object models
	result.fields=_.map(result.fields, exports.createDataModelField);
	result.views=_.map(result.views, (settings)=>exports.createDataView({settings}));
	return result;
};

/**
 * @param {DataModel} model
 * @param {Object} settings
 * @param {string} path - may specify a path to a specific view type in <link>res/configuration/model-defaults.yaml</link>
 * @returns {DataView}
 */
module.exports.createDataView=function({model=undefined, settings=undefined, path=undefined}={}) {
	let result;
	if(path) {
		settings=settings||_.get(model_defaults, path);
	}
	if(settings) {
		result=new objects.DataView(Object.assign({
			id: urn.create(constant.urn.type.DATA_MODEL_VIEW)
		}, settings));
	} else {
		result=new objects.DataView(_.merge({
			id: urn.create(constant.urn.type.DATA_MODEL_VIEW)
		}, model_defaults.dataModelView["get-one"]));
	}
	// convert whatever we caught into proper object models
	result.requestFields=_.map(result.requestFields, exports.createDataModelFieldOperation);
	result.responseFields=_.map(result.responseFields, exports.createDataModelFieldReference);
	return result;
};

/**
 * @param {Object} settings
 * @returns {DataField}
 */
module.exports.createDataModelField=function(settings=undefined) {
	settings=_resolveGamutsReferences(settings||model_defaults.dataModelField);
	return new objects.DataField(Object.assign({
		id: urn.create(constant.urn.type.DATA_MODEL_FIELD)
	}, settings));
};

/**
 * Creates a field filter/operation based on a model field.
 * He's sort of dumb. We should probably make him smarter
 * @param {Object} settings
 * @returns {DataFieldOperation}
 */
module.exports.createDataModelFieldOperation=function(settings=undefined) {
	settings=_resolveGamutsReferences(settings||model_defaults.dataModelFieldOperation);
	return new objects.DataFieldOperation(Object.assign({
		id: urn.create(constant.urn.type.DATA_MODEL_FIELD_OPERATION)
	}, settings));
};

/**
 * Creates a field filter/operation for views
 * @param {Object} settings
 * @returns {DataFieldOperation}
 */
module.exports.createDataQueryFieldOperation=function(settings=undefined) {
	settings=_resolveGamutsReferences(settings||model_defaults.dataQueryFieldOperation);
	return new objects.DataFieldOperation(Object.assign({
		id: urn.create(constant.urn.type.DATA_MODEL_FIELD_OPERATION)
	}, settings));
};

/**
 * Creates a field field reference for results
 * @param {Object} settings
 * @returns {DataFieldReference}
 */
module.exports.createDataResultFieldReference=function(settings=undefined) {
	settings=_resolveGamutsReferences(settings||model_defaults.dataResultFieldReference);
	return new objects.DataFieldReference(Object.assign({
		id: urn.create(constant.urn.type.DATA_MODEL_FIELD_REFERENCE)
	}, settings));
};

/**
 * @param {Object} settings
 * @returns {DataFieldReference}
 */
module.exports.createDataModelFieldReference=function(settings=undefined) {
	settings=_resolveGamutsReferences(settings||model_defaults.dataModelFieldReference);
	return new objects.DataFieldReference(Object.assign({
		id: urn.create(constant.urn.type.DATA_MODEL_FIELD_REFERENCE)
	}, settings));
};


/**
 * @param {Object} settings
 * @returns {Info}
 */
module.exports.createInfo=function(settings=undefined) {
	return (settings)
		? new objects.Info(settings)
		: new objects.Info(_.merge({}, model_defaults.info));
};

/**
 * @param {Object} settings
 * @returns {ServiceProxy}
 */
module.exports.createServiceProxy=function(settings=undefined) {
	if(settings) {
		return new objects.ServiceProxy(Object.assign({
			id: urn.create(`${constant.urn.type.SVC_INSTANCE}.${settings.type}`)
		}, settings));
	} else {
		return new objects.ServiceProxy(_.merge({
			id: urn.create(constant.urn.type.SVC_INSTANCE)
		}, model_defaults.proxy));
	}
};

/**
 * @param {Object} settings
 * @returns {Server}
 */
module.exports.createServer=function(settings=undefined) {
	return (settings)
		? new objects.Server(settings)
		: new objects.Server(_.merge({}, model_defaults.server));
};

/**
 * @param {Object} settings
 * @returns {User}
 */
module.exports.createUser=function(settings=undefined) {
	if(settings) {
		return new objects.User({
			id: urn.create(constant.urn.type.USER)
		}, settings);
	} else {
		return new objects.User(Object.assign({
			id: urn.create(constant.urn.type.USER)
		}, model_defaults.user));
	}
};

/**
 * If settings are defined then they are used. If they are not then it uses <code>value</code> and <code>error</code>
 * @param {Object} settings
 * @param {string} value
 * @param {Error|string|null} error
 * @param {{out:Array<string>, err:Array<string>}} stdio
 * @param {Date} timestamp
 * @returns {Status}
 */
module.exports.createStatus=function({
	settings=undefined,
	value=undefined,
	error=null,
	stdio={
		out: [],
		err: []
	},
	timestamp=new Date()
}={}) {
	if(value===undefined) {
		value=error ? constant.status.FAILURE : constant.status.NONE;
	}
	if(settings) {
		return new objects.Status(Object.assign({},
			settings,
			{timestamp: date_codec.fromString(settings.timestamp)}
		));
	} else {
		return new objects.Status({
			value,
			error: (error instanceof Error) ? error.message : error,
			stdio,
			timestamp
		});
	}
};

/**
 * The Variables object is sort of a go between object for our model and UI. He is fully self
 * constructed, but for the sake of consistency we are including his factory function here.
 * @param {Object} settings
 * @returns {Variable}
 */
module.exports.createVariable=function(settings=undefined) {
	if(settings) {
		return new objects.Variable(Object.assign({
			id: urn.create(constant.urn.type.VARIABLE)
		}, settings));
	} else {
		return new objects.Variable(Object.assign({
			id: urn.create(constant.urn.type.VARIABLE)
		}, model_defaults.variable));
	}
};

/**
 * @param {Object} settings
 * @returns {Variables}
 */
module.exports.createVariables=function(settings=undefined) {
	return (settings)
		? new objects.Variables(settings)
		: new objects.Variables(_.merge({}, model_defaults.variables));
};

/**
 * Either settings or a user must be specified
 * @param {Object} settings
 * @param {User} user
 * @param {Status} statusBuild
 * @param {Status} statusRun
 * @param {Status} statusSave
 * @param {Status} statusStop
 * @returns {Project}
 */
module.exports.createProject=function({
	settings=undefined,
	user=undefined,
	statusBuild=exports.createStatus(),
	statusSave=exports.createStatus(),
	statusStop=exports.createStatus(),
	statusRun=exports.createStatus()
}) {
	if(settings) {
		return new objects.Project({
			id: _.get(settings, "id", urn.create(constant.urn.type.PROJECT)),
			databases: _.map(settings.databases, (database)=>exports.createDatabase(database)),
			envs: exports.createEnvironments(settings.envs),
			info: exports.createInfo(settings.info),
			models: _.map(settings.models, (model)=>exports.createDataModel(model)),
			owner: exports.createUser(settings.owner),
			proxies: _.map(settings.proxies, (proxy)=>exports.createServiceProxy(proxy)),
			server: exports.createServer(settings.server),
			variables: exports.createVariables(settings.variables),
			targets: settings.targets || model_defaults.targets,
			timestampCreated: date_codec.fromString(settings.timestampCreated),
			timestampUpdated: date_codec.fromString(settings.timestampUpdated)
		}, {
			dirty: false,
			status: {
				build: statusBuild,
				run: statusRun,
				save: statusSave,
				stop: statusStop
			}
		});
	} else {
		assert.ok(user!==undefined);
		const timestamp=new Date();
		return new objects.Project({
			id: urn.create(constant.urn.type.PROJECT),
			databases: [],
			envs: exports.createEnvironments(),
			info: exports.createInfo(),
			models: [],
			owner: user,
			proxies: [],
			server: exports.createServer(),
			variables: exports.createVariables(),
			targets: model_defaults.targets,
			timestampCreated: timestamp,
			timestampUpdated: timestamp
		}, {
			dirty: true,
			status: {
				build: statusBuild,
				run: statusRun,
				save: statusSave,
				stop: statusStop
			}
		});
	}
};

/**** State Factories ****/

/**
 * @param {[ActivityComponent]} all
 * @param {[ActivityComponent]} groomed
 * @param {{apply:Function, test:Function}} filter
 * @param {Function} sort
 * @returns {ActivityState}
 */
module.exports.createActivityState=function({
	all=[],
	groomed=[],
	filter=mutators.filter.createPassThrough(),
	sort=mutators.sort.createByProperty("timestamp:reverse")
}={}) {
	return new state.ActivityState({
		all,
		groomed,
		filter,
		sort
	});
};

/**
 * Creates the application state
 * @param {string} nodenv - by default this will be defined by webpack and the DefinePlugin
 * @param {Status} status
 * @returns {ApplicationState}
 */
module.exports.createApplicationState=function({
	nodenv=process.env.NODE_ENV
}={}) {
	let settings;
	try {
		// this is for prime-time and we've been properly bundled
		settings=require(`json-loader!yaml-loader!../../res/configuration/environment/${nodenv}.yaml`);
	} catch(e) {
		// this is for server side testing
		const file=require("pig-core").file;
		settings=file.readToJSONSync(`./res/configuration/environment/${nodenv}.yaml`);
	}
	// process.env.VERSION - where is it defined? webpack DefinePlugin.
	settings.info.version=process.env.VERSION;
	return new state.ApplicationState(settings);
};

/**
 * Creates the gamuts state
 * @returns {GamutsState}
 */
module.exports.createGamutsState=function() {
	return new state.GamutsState(model_gamuts);
};

/**
 * @param {[Project]} all
 * @param {{apply:Function, test:Function}} filter
 * @param {Function} sort
 * @returns {ProjectsState}
 */
module.exports.createProjectsState=function({
	projects=[],
	filter=mutators.filter.createPassThrough(),
	sort=mutators.sort.createByProperty("timestampUpdated:reverse"),
	status={
		load: exports.createStatus()
	}
}={}) {
	return new state.ProjectsState({
		all: projects,
		filter,
		sort,
		status
	});
};

/**
 * @param {User} param0
 * @returns {UserState}
 */
module.exports.createUserState=function({
	loggedIn=null,
	status=exports.createStatus()
}={}) {
	return new state.UserState({
		loggedIn,
		status
	});
};
