# Models

## What is a "model"?

Our `model` is any object for which we want a clear definition.  Obviously objects such as our data model 
that we use to define a project constitutes such an object.  But perhaps less intuitive objects such as 
_state_ objects and _application_ configuration are also modeled.

## What does it do?

It supports immutability and a good looking JSON hierarchy.  See [_base.js](./_base.js) for more details
on how this works.   

## Breakdown

* `state` - all objects having to do with `redux` states.
* `objects` - the entire suite of model objects from which states are comprised.
