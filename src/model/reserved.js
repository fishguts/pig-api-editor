/**
 * User: curtis
 * Date: 5/22/18
 * Time: 10:46 PM
 * Copyright @2018 by Xraymen Inc.
 */

module.exports={
	fields: {
		"urn:mdl:fld:*": {
			name: "*",
			desc: "Represents the whole of whatever model it is in"
		}
	}
};
