/**
 * User: curtis
 * Date: 3/5/18
 * Time: 9:55 PM
 * Copyright @2018 by Xraymen Inc.
 */

const {Model}=require("../_base");

/**
 * @typedef {Model} ServiceProxy
 */
class ServiceProxy extends Model {
	/**
	 * @returns {string}
	 */
	get id() {return this._data.id;}

	/**
	 * @returns {string}
	 */
	get desc() {return this._data.desc;}
	/**
	 * @param {string} value
	 */
	set desc(value) {this._data.desc=value;}

	/**
	 * @returns {string}
	 */
	get name() {return this._data.name;}
	/**
	 * @param {string} value
	 */
	set name(value) {this._data.name=value;}

	/**
	 * @returns {string}
	 */
	get type() {return this._data.type;}
	/**
	 * @param {string} value
	 */
	set type(value) {this._data.type=value;}

	/**
	 * Configuration details. These will be dependent on the type
	 * @returns {Object}
	 */
	get details() {return this._data.details;}
	/**
	 * @param {Object} value
	 */
	set details(value) {this._data.details=value;}

	/**
	 * @returns {Array<DataFieldOperation>}
	 */
	get substitutions() {return this._data.substitutions;}
	/**
	 * @param {Array<DataFieldOperation>} value
	 */
	set substitutions(value) {this._data.substitutions=value;}
}

module.exports.ServiceProxy=ServiceProxy;
