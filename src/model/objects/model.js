/**
 * User: curtis
 * Date: 3/5/18
 * Time: 9:53 PM
 * Copyright @2018 by Xraymen Inc.
 */

const assert=require("pig-core").assert;
const {Model}=require("../_base");


/**
 * @typedef {Model} DataModel
 */
class DataModel extends Model {
	/**
	 * @returns {string}
	 */
	get id() {return this._data.id;}

	/**
	 * Optional attributes. Just a placeholder. Type is unknown. Most likely a key value set.
	 * @returns {Object<string, *>}
	 */
	get attributes() {return this._data.attributes;}
	set attributes(value) {this._data.attributes=value;}

	/**
	 * @returns {string}
	 */
	get desc() {return this._data.desc;}
	/**
	 * @param {string} value
	 */
	set desc(value) {this._data.desc=value;}

	/**
	 * @returns {[DataField]}
	 */
	get fields() {return this._data.fields;}
	/**
	 * @param {[DataField]} value
	 */
	set fields(value) {this._data.fields=value;}

	/**
	 * @returns {string}
	 */
	get name() {return this._data.name;}
	/**
	 * @param {string} value
	 */
	set name(value) {this._data.name=value;}

	/**
	 * All views built over this model
	 * @returns {[DataView]}
	 */
	get views() {return this._data.views;}
	/**
	 * @param {[DataView]} value
	 */
	set views(value) {this._data.views=value;}

	/**** Functional Support ****/
	/**
	 * Finds field or returns null.
	 * Note: this isn't deprecated but the findField in <link>./model/utils.js</link> is more full functioned
	 * @param {string} id
	 * @param {string} name
	 * @returns {DataField|null}
	 */
	findField({
		id=undefined,
		name=undefined
	}) {
		assert.ok(id || name);
		return (id)
			? this._data.fields.find(field=>field.id===id)
			: this._data.fields.find(field=>field.name===name);
	}

	/**
	 * Finds view or returns null
	 * @param {string} id
	 * @returns {DataView|null}
	 */
	findView(id) {
		return this.views.find((view)=>view.id===id);
	}
}

module.exports.DataModel=DataModel;
