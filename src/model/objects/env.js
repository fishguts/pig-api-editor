/**
 * User: curtis
 * Date: 3/6/18
 * Time: 10:27 PM
 * Copyright @2018 by Xraymen Inc.
 */

const {Model}=require("../_base");

/**
 * Supported environments and variable values within
 * @typedef {Model} Environment
 */
class Environment extends Model {
	/**
	 * Gets the unique, immutable id of this environment
	 * @returns {string}
	 */
	get id() {return this._data.id;}

	/**
	 * Optional attributes.
	 * @returns {Array<string>}
	 */
	get attributes() {return this._data.attributes;}
	/**
	 * @param {Object} value
	 */
	set attributes(value) {this._data.attributes=value;}

	/**
	 * Gets the environment name
	 * @returns {string}
	 */
	get name() {return this._data.name;}
	/**
	 * @param {string} value
	 */
	set name(value) {this._data.name=value;}

	/**
	 * Gets the environment description
	 * @returns {string}
	 */
	get desc() {return this._data.desc;}
	/**
	 * @param {string} value
	 */
	set desc(value) {this._data.desc=value;}

	/**
	 * @returns {Boolean}
	 */
	get debug() {return this._data.debug;}
	/**
	 * @param {Boolean} value
	 */
	set debug(value) {this._data.debug=value;}

	/**
	 * @returns {{level:string, transports:[Object]}}
	 */
	get log() {return this._data.log;}
	/**
	 * @param {{level:string, transports:[Object]}} value
	 */
	set log(value) {this._data.log=value;}

	/**
	 * Gets the environment value. The one that is set in an environment. Could be the name if you chose
	 * @returns {string}
	 */
	get value() {return this._data.value;}
	/**
	 * @param {string} value
	 */
	set value(value) {this._data.value=value;}
}

module.exports.Environment=Environment;
