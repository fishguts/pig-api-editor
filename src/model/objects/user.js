/**
 * User: curtis
 * Date: 3/7/18
 * Time: 4:16 PM
 * Copyright @2018 by Xraymen Inc.
 */

const {Model}=require("../_base");

/**
 * @typedef {Model} User
 */
class User extends Model {
	/**
	 * Identifier of this guy
	 * @returns {string}
	 */
	get id() {return this._data.id;}

	/**
	 * @returns {string}
	 */
	get name() {return this._data.name;}
	/**
	 * @param {string} value
	 */
	set name(value) {this._data.name=value;}

	/**
	 * @returns {Object}
	 */
	get authorization() {return this._data.auth;}
	/**
	 * @param {Object} value
	 */
	set authorization(value) {this._data.auth=value;}
}

module.exports.User=User;
