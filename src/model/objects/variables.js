/**
 * User: curtis
 * Date: 3/7/18
 * Time: 4:22 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const assert=require("pig-core").assert;
const template=require("pig-core").template;
const util=require("pig-core").util;
const {Model}=require("../_base");

/**
 * A well defined type for interacting with <code>Variables</code>
 */
/**
 * @param {string} id
 * @param {string} name
 * @param {string} scope
 * @param {"number"|"string"} type
 * @param {*} value
 * @param {Environment|string} env - id or env
 */
class Variable {
	constructor({id, name, scope, type, value, env=null}) {
		assert.ok(id!=null);
		this.id=id;
		this.name=name;
		this.scope=scope;
		this.type=type;
		this.value=value;
		this.env=env;
	}

	/**
	 * Creates a clone of this variable with options to alter it's value and translate it to another environment
	 * @param {*} value
	 * @param {string} env
	 * @returns {Variable}
	 */
	clone({value=this.value, env=this.env}) {
		return new Variable({
			id: this.id,
			env: env,
			name: this.name,
			scope: this.scope,
			type: this.type,
			value: value
		});
	}
}

/**
 * Manages our <code>variables</code> of a project. See model-defaults.yaml as an example.
 * @typedef {Model} Variables
 */
class Variables extends Model {
	/**
	 * Gets all environments for either the default environment are for a specific environment
	 * @param {Environment|string} env
	 * @returns {[Variable]}
	 */
	all(env=null) {
		return Object.keys(this._data)
			.reduce((result, varId)=> {
				const variable=this.get(varId, env);
				if(variable) {
					result.push(variable);
				}
				return result;
			}, [])
			.sort((a, b)=>util.compareStrings(a.name, b.name));
	}

	/**
	 * Deletes variable.  You may:
	 *  - delete the default in which case it entirely deletes the variable
	 *  - delete a variable from a single environment
	 * @param {Variable} variable
	 */
	delete(variable) {
		if(variable.env) {
			// He is only a environment override so all we do is delete his entry
			util.delete(this._data, this._getPath(variable.id, variable.env));
		} else {
			// He is a "default" so we get rid of him entirely.
			util.delete(this._data, variable.id);
		}
	}

	/**
	 * Gets the current state of the specified variable for the specified environment by ID
	 * @param {string} id - variable id
	 * @param {Environment|string} env - id or object
	 * @returns {Variable}
	 */
	get(id, env=null) {
		const path=this._getPath(id, env),
			varNode=_.get(this._data, path);
		return (varNode)
			? new Variable({
				env: env,
				id: id,
				name: this._data[id].default.name,
				scope: this._data[id].default.scope,
				type: this._data[id].default.type,
				value: varNode.value
			})
			: undefined;
	}

	/**
	 * Updates the current state.
	 * @param {Variable} variable
	 */
	set(variable) {
		assert.ok(!variable.env || this._exists(variable.id), "we never want variables to exist at the env level and not exist at the default level.");
		const path=this._getPath(variable.id, variable.env);
		_.set(this._data, `${variable.id}.default.name`, variable.name);
		_.set(this._data, `${variable.id}.default.scope`, variable.scope);
		_.set(this._data, `${variable.id}.default.type`, variable.type);
		_.set(this._data, `${path}.value`, variable.value);
	}

	/**
	 * Render a template string from our current state.
	 * Note: variables should not be resolved in the client. This should only be used for presentation.
	 * @param {string} string
	 * @param {string} env
	 * @returns {string} will return string with substitutions if they are all found otherwise will return <code>string</code>
	 */
	renderTemplateString(string, env=null) {
		const variables=this.all(env)
			.reduce((result, variable)=> {
				result[variable.name]=variable.value;
				return result;
			}, {});
		return util.try(_.partial(template.render, string, variables), string);
	}

	/**************** Private Interface ****************/
	/**
	 * Gets the property path for the variable
	 * @param {string} id
	 * @param {Environment|string} env
	 * @returns {string}
	 * @private
	 */
	_getPath(id, env=null) {
		return (env)
			? `${id}.${_.isObject(env) ? env.id : env}`
			: `${id}.default`;
	}

	/**
	 * See whether the variable exists
	 * @param {string} id
	 * @param {Environment|string} env
	 * @returns {string}
	 * @private
	 */
	_exists(id, env=null) {
		return _.has(this._data, this._getPath(id, env));
	}
}

module.exports.Variable=Variable;
module.exports.Variables=Variables;
