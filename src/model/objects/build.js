/**
 * User: curtis
 * Date: 3/10/18
 * Time: 8:12 PM
 * Copyright @2018 by Xraymen Inc.
 */

const {Model}=require("../_base");

/**
 * Information about how and where to build this guy
 * @typedef {Model} Build
 */
class Build extends Model {
	/**
	 * Optional attributes.
	 * @returns {Object}
	 */
	get attributes() {return this._data.attributes;}
	/**
	 * @param {Object} value
	 */
	set attributes(value) {this._data.attributes=value;}

	/**
	 * @returns {{execution:string, build:string, validation:string}}
	 */
	get states() {return this._data.states;}
}

module.exports.Build=Build;
