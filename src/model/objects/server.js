/**
 * User: curtis
 * Date: 3/5/18
 * Time: 9:56 PM
 * Copyright @2018 by Xraymen Inc.
 */

const {Model}=require("../_base");

/**
 * @typedef {Model} Server
 */
class Server extends Model {
	/**
	 * @returns {string}
	 */
	get desc() { return this._data.desc; }
	/**
	 * @param {string} value
	 */
	set desc(value) { return this._data.desc=value; }

	/**
	 * Server url. Supports variable substitution
	 * @returns {string}
	 */
	get url() { return this._data.url; }
	/**
	 * @param {string} value
	 */
	set url(value) { this._data.url=value; }
}

module.exports.Server=Server;
