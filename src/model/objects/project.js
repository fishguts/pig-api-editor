/**
 * User: curtis
 * Date: 3/5/18
 * Time: 9:52 PM
 * Copyright @2018 by Xraymen Inc.
 */

const {Model}=require("../_base");


/**
 * Instance of a single project
 * @typedef {Model} Project
 */
class Project extends Model {
	/******************************************* Session Properties *****************************************/
	/* These guys are not part of the spec.  These are exceptional properties that would live in their own
	 * state object if we managed a separate state object for projects. But we are lazy and are keeping it here.
	/********************************************************************************************************/
	/**
	 * Session data that tracks whether edits have been made or not
	 * @return {Boolean}
	 */
	get dirty() {return this._session.dirty;}
	/**
	 * @param {Boolean} value
	 */
	set dirty(value) {this._session.dirty=value;}

	/**
	 * Get reference to status. If you want to update it then set the status objects within.
	 * @returns {{build:Status, run:Status, save:Status, stop:Status}}
	 */
	get status() {return this._session.status;}
	set status(value) {this._session.status=value;}

	/******************************************* Model Properties *******************************************/
	/* The rest of these guys are treated as part of the project spec
	/********************************************************************************************************/
	/**
	 * Id of this project.
	 * @returns {string}
	 */
	get id() {return this._data.id;}

	/**
	 * @returns {[Database]}
	 */
	get databases() {return this._data.databases;}
	/**
	 * @param {[Database]} value
	 */
	set databases(value) {this._data.databases=value;}

	/**
	 * The project description officially lives in <code>info</code>. This guy is only for presentation (makes variable substitutions)
	 * @returns {string}
	 */
	get desc() {return this.variables.renderTemplateString(this.info.desc);}

	/**
	 * @returns {[Environment]}
	 */
	get envs() {return this._data.envs;}
	/**
	 * @param {[Environment]} value
	 */
	set envs(value) {this._data.envs=value;}

	/**
	 * @returns {Info}
	 */
	get info() {return this._data.info;}
	/**
	 * @param {Info} value
	 */
	set info(value) {this._data.info=value;}

	/**
	 * @returns {[DataModel]}
	 */
	get models() {return this._data.models;}
	/**
	 * @param {[DataModel]} value
	 */
	set models(value) {this._data.models=value;}

	/**
	 * The project name officially lives in <code>info</code>.
	 * @returns {string}
	 */
	get name() {return this.info.name;}

	/**
	 * This is a marker used to identify who owns this project. It really should not be a user but rather a group with permissions.
	 * @returns {User}
	 */
	get owner() {return this._data.owner;}
	/**
	 * @param {User} value
	 */
	set owner(value) {this._data.owner=value;}

	/**
	 * @returns {[ServiceProxy]}
	 */
	get proxies() {return this._data.proxies;}
	/**
	 * @param {[ServiceProxy]} value
	 */
	set proxies(value) {this._data.proxies=value;}

	/**
	 * @returns {Server}
	 */
	get server() {return this._data.server;}
	/**
	 * @param {Server} value
	 */
	set server(value) {this._data.server=value;}

	/**
	 * @returns {Variables}
	 */
	get variables() {return this._data.variables;}
	/**
	 * @param {Variables} value
	 */
	set variables(value) {this._data.variables=value;}

	/**
	 * For now this is just an array of strings. I could see that it would be helpful to configure each
	 * one but not today.  Leaving it an array of strings keeps things easy for us (UI-wise)
	 * @returns {[String]}
	 */
	get targets() {return this._data.targets;}
	/**
	 * @param {Variables} value
	 */
	set targets(value) {this._data.targets=value;}

	/**
	 * @returns {Date}
	 */
	get timestampCreated() {return this._data.timestampCreated;}
	/**
	 * @returns {Date}
	 */
	get timestampUpdated() {return this._data.timestampUpdated;}
	/**
	 * @param {Date} value
	 */
	set timestampUpdated(value) {this._data.timestampUpdated=value;}

	/*********************************** Functional Support ***********************************/
	/**
	 * Finds model associated with id
	 * @param {string} id
	 * @returns {DataModel | undefined}
	 */
	findModel(id) {
		return this.models.find((model)=>model.id===id);
	}

	/**
	 * Renders this template with current set of variables
	 * @param {string} template
	 * @returns {string}
	 */
	renderTemplateString(template) {
		return this._data.variables.renderTemplateString(template);
	}
}

module.exports.Project=Project;
