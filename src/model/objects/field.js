/**
 * User: curtis
 * Date: 3/5/18
 * Time: 9:54 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const {Model}=require("../_base");

/**
 * Describes a field. Most likely in a database but could be elsewhere.
 * @typedef {Model} DataField
 */
class DataField extends Model {
	/**
	 * Unique id of this field
	 * @returns {string}
	 */
	get id() {return this._data.id;}

	/**
	 * Allowed values for this field.
	 * @returns {PigFieldAllowed}
	 */
	get allowed() {return this._data.allowed;}
	set allowed(value) {
		this._data.allowed.regex=_.isEmpty(value.regex) ? undefined : value.regex;
		this._data.allowed.values=_.isEmpty(value.values) ? undefined : value.values;
	}

	/**
	 * Optional attributes.
	 * @returns {Array<string>}
	 */
	get attributes() {return this._data.attributes;}
	set attributes(value) {this._data.attributes=value;}

	/**
	 * @returns {string}
	 */
	get desc() {return this._data.desc;}
	/**
	 * @param {string} value
	 */
	set desc(value) {this._data.desc=value;}

	/**
	 * @returns {string}
	 */
	get name() {return this._data.name;}
	/**
	 * @param {string} value
	 */
	set name(value) {this._data.name=value;}

	/**
	 * @returns {string}
	 */
	get subtype() {return this._data.subtype;}
	/**
	 * @param {string} value
	 */
	set subtype(value) {this._data.subtype=value;}

	/**
	 * @returns {string}
	 */
	get type() {return this._data.type;}
	/**
	 * @param {string} value
	 */
	set type(value) {this._data.type=value;}
}

/**
 * Maps to an existing field in a model.  Such fields are used when specifying fields that one wants back in a response.
 * @typedef {Model} DataFieldReference
 */
class DataFieldReference extends Model {
	/**
	 * Unique id of this particular instance
	 * @returns {string}
	 */
	get id() {return this._data.id;}

	/**
	 * Default value if the the value is not specified.
	 * Note: we use defaultValues for single values and arrays (such as sort and projection).
	 * @returns {*|Array<string>}
	 */
	get defaultValue() {return this._data.defaultValue;}
	/**
	 * @param {*|Array<string>} value
	 */
	set defaultValue(value) {this._data.defaultValue=value;}

	/**
	 * Underlying field reference that this instance refers to
	 * @returns {string}
	 */
	get fieldId() {return this._data.fieldId;}
	set fieldId(value) {this._data.fieldId=value;}

	/**
	 * The function to which this reference belongs. It may be a property of a model (field in a model)
	 * or a property of a query (as in limit the response count) or a property of the result (as in # deleted)
	 * @returns {"model"|"query"|"result"}
	 */
	get propertyOf() {return this._data.propertyOf;}
	set propertyOf(value) {this._data.propertyOf=value;}
}

/**
 * Maps to an existing field in a model. It additionally add properties that describe various purposes of this field:
 * - information about where it will be found if used as a request parameter
 * - the default value if not explicitly specified
 * - it's optional "parameter" name, etc..
 * - what type of function that it modifies (propertyOf)
 * Such fields are used when filtering requests.
 * @typedef {DataFieldReference} DataFieldOperation
 */
class DataFieldOperation extends DataFieldReference {
	/**
	 * These are optional defaults that may or may not be set and they only apply to fields
	 * that we create.  Such as view-fields.
	 * @returns {(undefined|{location:(string|undefined), operation:(string|undefined), value:*})}
	 */
	get defaults() {return this._data.defaults;}
	set defaults(value) {this._data.defaults=value;}

	/**
	 * The encoding of the field. See <link>res/configuration/model-gamuts.yaml</link>
	 * @returns {string}
	 */
	get encoding() {return this._data.encoding;}
	set encoding(value) {this._data.encoding=value;}

	/**
	 * Where he may be found
	 * @returns {"body"|"header"|"literal"|"path"|"query"}
	 */
	get location() {return this._data.location;}
	set location(value) {this._data.location=value;}

	/**
	 * What operation to use for comparison
	 * @returns {string}
	 */
	get operation() {return this._data.operation;}
	set operation(value) {this._data.operation=value;}

	/**
	 * Optional name by which he will be identified. Will default to the field name.
	 * @returns {string}
	 */
	get variable() {return this._data.variable;}
	set variable(value) {this._data.variable=_.isEmpty(value) ? undefined : value;}
}

module.exports={
	DataField,
	DataFieldOperation,
	DataFieldReference
};
