/**
 * User: curtis
 * Date: 3/10/18
 * Time: 9:24 PM
 * Copyright @2018 by Xraymen Inc.
 */

const {Model}=require("../_base");

/**
 * @typedef {Model} Activity
 */
class Activity extends Model {
	/**
	 * And id that we create and keep
	 * @returns {string}
	 */
	get id() {return this._data.id;}

	/**
	 * Gets the activity details
	 * @returns {String|Array}
	 */
	get details() {return this._data.details;}

	/**
	 * Gets the activity message
	 * @returns {string}
	 */
	get message() {return this._data.message;}

	/**
	 * Gets the severity of the event.  See
	 * @returns {constant.severity}
	 */
	get severity() {return this._data.severity;}

	/**
	 * Gets the stack should there have been an error
	 * @returns {String|undefined}
	 */
	get stack() {return this._data.stack;}

	/**
	 * Gets the status-code should there be one
	 * @returns {Integer|undefined}
	 */
	get statusCode() {return this._data.statusCode;}

	/**
	 * Gets stdio if there is any
	 * @returns {{err:Array, out:Array}}
	 */
	get stdio() {return this._data.stdio;}

	/**
	 * Gets the activity details
	 * @returns {string}
	 */
	get timestamp() {return this._data.timestamp;}
}

module.exports.Activity=Activity;
