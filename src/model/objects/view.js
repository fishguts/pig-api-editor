/**
 * User: curtis
 * Date: 3/5/18
 * Time: 9:53 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const template=require("pig-core").template;
const {Model}=require("../_base");

/**
 * Base class data view for any data view
 * @typedef {Model} DataView
 */
class DataView extends Model {
	/**
	 * This guy uniquely identifies the view
	 * @returns {string}
	 */
	get id() {return this._data.id;}

	/**
	 * @returns {string}
	 */
	get desc() {return this._data.desc;}
	set desc(value) {this._data.desc=value;}

	/**
	 * The function of the view. See <link>res/configuration/model-defaults.yaml</link> for details
	 * @return {string}
	 */
	get function() {return this._data.function;}
	set function(value) {this._data.function=value;}

	/**
	 * @returns {string}
	 */
	get name() {return this._data.name;}
	set name(value) {this._data.name=_.isEmpty(value) ? undefined : value;}

	/**
	 * @returns {string}
	 */
	get path() {return this._data.path;}
	set path(value) {this._data.path=value;}

	/**
	 * @returns {Array<DataFieldOperation>}
	 * @default {null}
	 */
	get requestFields() {return this._data.requestFields;}
	/**
	 * @param {Array<DataFieldOperation>} value
	 */
	set requestFields(value) {this._data.requestFields=value;}

	/**
	 * Is the response configurable? It will not be for requests such as "count" and "delete"
	 * @return {Boolean}
	 */
	get responseConfigurable() {return _.get(this._data, "responseConfigurable", true);}

	/**
	 * The fields you want to get back. At the time we assume all fields are within the same model.As with sort, though it can be multiple fields we assume
	 * @returns {Array<DataFieldReference>}
	 */
	get responseFields() {return this._data.responseFields;}
	/**
	 * @param {Array<DataFieldReference>} value
	 */
	set responseFields(value) {this._data.responseFields=value;}

	/**
	 * This is the service with which this view will do it's business.  It may be:
	 *  - <empty>
	 *  - database-id
	 *  - proxy-id
	 * @returns {string}
	 */
	get serviceId() {return this._data.serviceId;}
	/**
	 * @param {string} value
	 */
	set serviceId(value) {this._data.serviceId=value;}

	/**
	 * Optional timeout duration. Uses the /\d+(s|ms)/ syntax.
	 * @returns {String|null}
	 * @default {null}
	 */
	get timeout() {return this._data.timeout;}
	/**
	 * @param {string|null} value
	 */
	set timeout(value) {this._data.timeout=value;}

	/**
	 * @returns {{protocol: string, method: string}}
	 */
	get transport() {return this._data.transport;}
	/**
	 * @param {{protocol: string, method: string}} value
	 */
	set transport(value) {this._data.transport=value;}

	/**************** Functional Support ****************/
	/**
	 * This guy gets a name one way or the other. If it does not have a name then he generates a default name
	 * @param {DataModel} model
	 * @returns {string}
	 */
	getName(model) {
		return _.isEmpty(this._data.name)
			? this.generateDefaultName(model)
			: this._data.name;
	}

	/**
	 * Generates a default name for this guy.  We are attempting to mimic what swagger does if a name is not specified
	 * so that the defaults will not surprise folks.
	 * Note: I had started persisting our own version of it but it's a fair amount of work.
	 * @param {DataModel} model
	 * @returns {string}
	 */
	generateDefaultName(model) {
		const path=template.render(this.path, {
				model: model.name
			}),
			split=path.split(/\s*\/+\s*/)
				.filter((value)=>value.length>0)
				.map((value, index)=>(index>0)
					? _.chain(value)
						.trim(["{", "}"])
						.upperFirst(value)
						.value()
					: value);
		return split.concat(this.transport.method.toUpperCase()).join("");
	}
}

module.exports.DataView=DataView;
