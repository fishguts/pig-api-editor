/**
 * User: curtis
 * Date: 3/5/18
 * Time: 9:55 PM
 * Copyright @2018 by Xraymen Inc.
 */

const {Model}=require("../_base");

/**
 * Holds a project's metadata
 * @typedef {Model} Info
 */
class Info extends Model {
	/**
	 * @returns {{name:string, email:string, url:string}}
	 */
	get contact() {return this._data.contact;}
	/**
	 * @param {{name:string, email:string, url:string}} value
	 */
	set contact(value) {this._data.contact=value;}

	/**
	 * @returns {string}
	 */
	get desc() {return this._data.desc;}
	/**
	 * @param {string} value
	 */
	set desc(value) {this._data.desc=value;}

	/**
	 * @returns {{terms:{name:string, url:string}, license:{name:string, url:string}}}
	 */
	get legal() {return this._data.legal;}
	/**
	 * @param {{terms:{name:string, url:string}, license:{name:string, url:string}}} value
	 */
	set legal(value) {this._data.legal=value;}

	/**
	 * @returns {string}
	 */
	get name() {return this._data.name;}
	/**
	 * @param {string} value
	 */
	set name(value) {this._data.name=value;}

	/**
	 * @returns {string}
	 */
	get version() {return this._data.version;}
	/**
	 * @param {string} value
	 */
	set version(value) {this._data.version=value;}
}

module.exports.Info=Info;
