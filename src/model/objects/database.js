/**
 * User: curtis
 * Date: 3/6/18
 * Time: 9:08 PM
 * Copyright @2018 by Xraymen Inc.
 */

const {Model}=require("../_base");

/**
 * Describes a database
 * @typedef {Model} Database
 */
class Database extends Model {
	/**
	 * @returns {string}
	 */
	get id() {return this._data.id;}

	/**
	 * Optional attributes.
	 * @returns {Object}
	 */
	get attributes() {return this._data.attributes;}
	/**
	 * @param {Object} value
	 */
	set attributes(value) {this._data.attributes=value;}

	/**
	 * @returns {string}
	 */
	get connection() {return this._data.connection;}
	/**
	 * @param {string} value
	 */
	set connection(value) {this._data.connection=value;}

	/**
	 * @returns {string}
	 */
	get desc() {return this._data.desc;}
	/**
	 * @param {string} value
	 */
	set desc(value) {this._data.desc=value;}

	/**
	 * @returns {string}
	 */
	get name() {return this._data.name;}
	/**
	 * @param {string} value
	 */
	set name(value) {this._data.name=value;}

	/**
	 * Enum. See <code>constants.js</code>
	 * @returns {string}
	 */
	get type() {return this._data.type;}
	/**
	 * @param {string} value
	 */
	set type(value) {this._data.type=value;}
}

module.exports.Database=Database;
