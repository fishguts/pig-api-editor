/**
 * User: curtis
 * Date: 6/5/18
 * Time: 10:41 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const {PigError}=require("pig-core").error;
const {Model}=require("../_base");

/**
 * This guy is more for duck-typing than anything else but don't let that stop you from creating one.
 * @typedef {Model} StatusModel
 */
class StatusModel extends Model {
	/**
	 * @returns {String|null}
	 */
	get value() { return this._data.value; }
	set value(value) { return this._data.value=value; }

	/**
	 * @returns {String|null}
	 */
	get error() { return this._data.error; }
	set error(value) { this._data.error=value; }

	/**
	 * @returns {{out:Array<string>, error:Array<string>}}
	 */
	get stdio() { return this._data.stdio; }
	/**
	 * @returns {Date}
	 */
	get timestamp() { return this._data.timestamp; }

	/**
	 * Assuming that the status some sort of failure, this guy will create a proper Error out this guy.
	 * @returns {PigError}
	 */
	toError() {
		return new PigError({
			instance: this,
			message: this.error,
			details: _.get(this.stdio, "err", []).join("\n")
		});
	}
}

module.exports.Status=StatusModel;
