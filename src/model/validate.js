/**
 * User: curtis
 * Date: 5/16/18
 * Time: 10:29 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const reserved=require("./reserved");

/**
 * Validates that host is legitimate. Taken directly from spec
 * @param {string} value
 * @returns {null|Error}
 */
function validateHostName(value) {
	return /^[^{}/ :\\]+(?::\d+)?$/.test((value || ""))
		? null
		: new Error("invalid host name");
}

/**
 * Validates legitimate name for an object. Model field names are an exception because of
 * how we use "." delimiters to support nesting. We now support that here.
 * @param {string} value
 * @param {Boolean} dot
 * @param {Boolean} hyphen
 * @param {Boolean} template can value be a template
 * @param {string} type - type of object so that error text can be more specific than "object"
 * @returns {null|Error}
 */
function validateObjectName(value, {
	dot=false,
	hyphen=false,
	template=false,
	type="object"
}={}) {
	value=(template)
		? (value || "").replace(/{{\s*(\S+?)\s*}}/g, "dummy")
		: (value || "");
	let expression;
	if(hyphen && dot) {
		expression=/^\s*[_a-zA-Z]([-.]?\w+){0,}\s*$/;
	} else if(dot) {
		expression=/^\s*[_a-zA-Z](\.?\w+){0,}\s*$/;
	} else if(hyphen) {
		expression=/^\s*[_a-zA-Z](-?\w+){0,}\s*$/;
	} else {
		expression=/^\s*[_a-zA-Z]\w*\s*$/;
	}
	return expression.test(value)
		? null
		: new Error(`invalid ${type} name: ${expression}`);
}

/**
 * Validates not a duplicate of others in the same container
 * @param {[{id:string, name:string}]} container
 * @param {{id:string, name:string}} object
 * @param {string} value
 * @param {string} property property to compare for uniqueness
 * @returns {null|Error}
 */
function validateObjectUniqueness(container, object, value, {
	property="name"
}={}) {
	const duplicates=container.filter((_object)=>_object.id!==object.id && _object[property]===value);
	return duplicates.length>0
		? new Error("duplicate identifier")
		: null;
}

/**
 * Validates that the reference exists in the model. It doesn't do anything regarding uniqueness
 * @param {DataModel} model
 * @param {string} value
 * @returns {null|Error}
 */
function validateModelFieldIdReference(model, value) {
	return model.findField({id: value})
		? null
		: new Error(`field not found in model ${model.name}`);
}

/**
 * Validates that the reference exists in the model
 * @param {DataModel} model
 * @param {string} value
 * @returns {null|Error}
 */
function validateModelFieldNameReference(model, value) {
	return model.findField({name: value})
		? null
		: new Error(`field nout found in model ${model.name}`);
}


/**
 * Validates that the database name is valid and not a duplicate
 * @param {Array} databases
 * @param {Database} database
 * @param {string} value
 * @returns {null|Error}
 */
function validateDatabaseName(databases, database, value) {
	let error=validateObjectName(value, {
		hyphen: true,
		template: true,
		type: "database"
	});
	if(error) {
		return error;
	}
	// note: it's fine if the database names are not unique as long as they are not of the same type. In fact that is probably a good practice when it happens.
	const duplicates=databases.filter((_database)=>_database.id!==database.id && _database.name===database.name && _database.type===database.type);
	return duplicates.length>0
		? new Error("duplicate identifier")
		: null;
}

/**
 * Validates that the env name is not empty and is unique
 * @param {Array} envs
 * @param {Variable} env
 * @param {string} value
 * @returns {null|Error}
 */
function validateEnvName(envs, env, value) {
	return validateObjectName(value, {type: "environment"})
		|| validateObjectUniqueness(envs, env, value, {
			property: "name"
		});
}

/**
 * Validates that the env is not empty and is unique
 * @param {Array} envs
 * @param {Variable} env
 * @param {string} value
 * @returns {null|Error}
 */
function validateEnvValue(envs, env, value) {
	return validateObjectName(value, {type: "environment"})
		|| validateObjectUniqueness(envs, env, value);
}

/**
 * Validates that the variable name is valid and not a duplicate
 * @param {Array} variables
 * @param {Variable} variable
 * @param {string} value
 * @returns {null|Error}
 */
function validateVariableName(variables, variable, value) {
	// I liked having a dot but templates assume that they are tiered properties in an object. Which is right and consistent.
	// So we will adhere to the same convention even though we will always only have one tier of variables
	return validateObjectName(value, {
		dot: false,
		type: "variable"
	}) || validateObjectUniqueness(variables, variable, value);
}


/**
 * Validates that the model name is valid and not a duplicate
 * @param {[DataModel]} models
 * @param {DataModel} model
 * @param {string} value
 * @returns {null|Error}
 */
function validateModelName(models, model, value) {
	return validateObjectName(value, {
		hyphen: true,
		template: true,
		type: "model"
	}) || validateObjectUniqueness(models, model, value);
}

/**
 * Validates that the model/view name is valid and not a duplicate
 * @param {DataModel} model
 * @param {DataView} view
 * @param {string} value
 * @returns {null|Error}
 */
function validateModelViewName(model, view, value) {
	function _validateUniqueness() {
		const duplicates=model.views.filter((_view)=>_view.id!==view.id && (_view.name || _view.generateDefaultName(model))===value);
		return duplicates.length>0
			? new Error("duplicate view name")
			: null;
	}

	if(_.isEmpty(value)) {
		value=view.generateDefaultName(model);
	}
	return validateObjectName(value, {
		hyphen: true,
		template: true,
		type: "view"
	}) || _validateUniqueness();
}

/**
 * Validates that the field name is valid
 * @param {DataModel} model
 * @param {DataField} field
 * @param {string} value
 * @returns {null|Error}
 */
function validateModelFieldName(model, field, value) {
	// we reserve special rights with a few guys
	if(reserved.fields[field.id]) {
		if(value!==reserved.fields[field.id].name) {
			return new Error(`reserved field name must be ${reserved.fields[field.id].name}`);
		} else {
			return validateObjectUniqueness(model.fields, field, value);
		}
	}
	return validateObjectName(value, {
		dot: true,
		hyphen: false,
		type: "field"
	}) || validateObjectUniqueness(model.fields, field, value);
}

/**
 * Validates that all of the attributes are okay and work well with eachother
 * @param {DataModel} model
 * @param {DataView} view
 * @param {Array<string>} value
 * @returns {null|Error}
 */
function validateModelViewAttributes(model, view, value) {
	if(_.intersection(["create", "delete"], value).length>1) {
		return new Error("\"create\" and \"delete\" may note coexist");
	}
	if(_.intersection(["update", "delete"], value).length>1) {
		return new Error("\"update\" and \"update\" may note coexist");
	}
	// note: update and create is equal to an "upsert" and is allowed and encouraged.
	return null;
}

/**
 * Validates the path portion of a url
 * @param {DataModel} model
 * @param {DataView} view
 * @param {string} value
 * @returns {null|Error}
 */
function validateModelViewPath(model, view, value) {
	const encode=(path)=>path.replace(/{[^}]+}+/g, String.fromCharCode(1));

	if((value || "").match(/^\s*(\/?[^<>[\]/?='"`]){1,}\/?\s*$/)===null) {
		return new Error("invalid URL path");
	}
	// it cannot syntactically match any other view for the transport method and protocol (Think about how express resolves a url)
	const encoded=encode(value),
		duplicates=model.views.filter((_view)=>view.id!==_view.id && _.isEqual(view.transport, _view.transport))
			.map((_view)=>_view.path)
			.filter((path)=>encode(path)===encoded);
	return duplicates.length>0
		? new Error("duplicate path")
		: null;
}

/**
 * Validates that the variable name is valid and not a duplicate of a variable used in the same location
 * @param {Array<DataFieldOperation>} fields
 * @param {DataFieldOperation} field
 * @param {string} value
 * @returns {null|Error}
 */
function validateRequestFieldVariableName(fields, field, value) {
	function _validateUniqueness() {
		const duplicates=_.filter(fields, (_filter)=>(
			_filter.id!==field.id
			&& _filter.variable===value
			&& _filter.location===field.location
		));
		return duplicates.length>0
			? new Error("duplicate variable name")
			: null;
	}

	// We default to the field name so we don't force a variable name.
	if(_.isEmpty(value)) {
		return null;
	}
	return validateObjectName(value, {type: "variable"}) || _validateUniqueness();
}

/**
 * Validates that this guy's reference exists and that the reference is unique
 * @param {DataModel} model
 * @param {Array<DataFieldReference>} fields
 * @param {DataFieldReference} field
 * @param {string} value
 * @returns {null|Error}
 */
function validateResponseFieldReference(model, fields, field, value) {
	return validateModelFieldIdReference(model, field.fieldId)
		|| validateObjectUniqueness(fields, field, value, {property: "fieldId"});
}

/**
 * Validates a timeout value. We are assuming that empty is valid.
 * @param {string} value
 * @returns {null|Error}
 */
function validateTimeout(value) {
	return /^s*(\d+(s|ms)?)?\s*$/i.test(value || "")
		? null
		: new Error("invalid timeout: /^s*(\\d+(s|ms)?)?\\s*$/i");
}


module.exports={
	validateHostName,
	validateObjectName,
	validateObjectUniqueness,
	validateModelFieldIdReference,
	validateModelFieldNameReference,
	validateDatabaseName,
	validateEnvName,
	validateEnvValue,
	validateVariableName,
	validateModelName,
	validateModelViewName,
	validateModelFieldName,
	validateModelViewAttributes,
	validateModelViewPath,
	validateRequestFieldVariableName,
	validateResponseFieldReference,
	validateTimeout
};
