/**
 * User: curtis
 * Date: 3/18/18
 * Time: 6:01 PM
 * Copyright @2018 by Xraymen Inc.
 */

const assert=require("pig-core").assert;

module.exports.fs=class {
	readFileSync() {assert.ok(false);}
	readJSONSync() {assert.ok(false);}
};
