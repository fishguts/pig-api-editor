/**
 * User: curtis
 * Date: 3/10/18
 * Time: 10:02 PM
 * Copyright @2018 by Xraymen Inc.
 */


const redux=require("redux");
const reduxThunk=require("redux-thunk").default;
const reduxLogger=require("redux-logger").default;
const reducers=require("../reducers");

module.exports=redux.createStore(reducers, redux.applyMiddleware(reduxThunk, reduxLogger));
