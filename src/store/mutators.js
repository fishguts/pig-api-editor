/**
 * User: curtis
 * Date: 3/10/18
 * Time: 10:39 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const assert=require("pig-core").assert;
const constant=require("../common/constant");

const severityMap={
	[constant.severity.DEBUG]: 1,
	[constant.severity.INFO]: 2,
	[constant.severity.WARN]: 3,
	[constant.severity.ERROR]: 4,
	[constant.severity.FATAL]: 5
};

module.exports.filter={
	/**
	 * creates a general purpose filter
	 * @param {Function} test - take an object and return boolean
	 * @param {string} propertyPath - optional dereference path
	 * @return {{property:string, add: function(*, *=), apply: function(*=)}}
	 */
	create(test, propertyPath=undefined) {
		return {
			add: (list, object)=> {
				if(_.isArray(object)) {
					return list.concat(object.filter((item)=>test(propertyPath ? _.get(item, propertyPath) : item)));
				} else {
					return test(propertyPath ? _.get(object, propertyPath) : object)
						? list.concat(object)
						: list;
				}
			},
			apply: (list)=>_.filter(list, test),
			property: propertyPath
		};
	},

	/**
	 * Creates a pass-through filter. Everybody looks good to him.
	 * @return {{add: function(*, *=), apply: function(*)}}
	 */
	createPassThrough() {
		return {
			add: (list, object)=>list.concat(object),
			apply: (list)=>list,
			property: null
		};
	},

	/**
	 * @param {constant.severity} level
	 * @param {string} propertyPath
	 * @return {{add:Function, apply:Function}}
	 */
	createSeverity(level, propertyPath=undefined) {
		const test=(object)=>(severityMap[object]<=severityMap[level]);
		return this.create(test, propertyPath);
	}
};

module.exports.sort={
	/**
	 * He giveth what he taketh
	 * @return {{property:null, apply:function(*): *}}
	 */
	createPassThrough() {
		return {
			apply: (list)=>list,
			property: null
		};
	},

	/**
	 * @param {string} property - supports our own little syntax to include options:
	 *  - :reverse - to reverse the sort
	 *  - :no-case - to ignore case
	 * @return {{property:string, apply:function(*=): any[]}}
	 */
	createByProperty(property="") {
		let sortPath=property,
			ignoreCase=false,
			reverse=false;
		_.chain(sortPath)
			.split(":")
			.forEach((flag, index)=>{
				if(index===0) {
					sortPath=flag;
				} else if(flag==="reverse") {
					reverse=true;
				} else if(flag==="no-case") {
					ignoreCase=true;
				} else {
					assert.toLog(`flag=${flag}?`);
				}
			})
			.value();
		return {
			apply: (list)=> {
				if(ignoreCase) {
					list=_.sortBy(list, [item=>
						((sortPath.length)
							? _.get(item, sortPath, "")
							: (item || "")).toString().toLocaleLowerCase()
					]);
				} else {
					list=(sortPath.length>0)
						? _.sortBy(list, [sortPath])
						: _.sortBy(list);
				}
				if(reverse) {
					list.reverse();
				}
				return list;
			},
			property
		};
	}
};
