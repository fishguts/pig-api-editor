/**
 * User: curtis
 * Date: 3/18/18
 * Time: 9:45 PM
 * Copyright @2018 by Xraymen Inc.
 */

const http=require("../common/http");
const model_factory=require("../model/factory");

/**
 * Builds specified project
 * @param {Project} project
 * @param {User} user
 * @returns {Promise}
 */
module.exports.build=function(project, user) {
	return http.put(`api/project/build/${project.id}`, {
		headers: {
			user
		}
	}).then((result)=>result.json());
};

/**
 * Deletes this fella
 * @param {Project} project
 * @param {User} user
 * @returns {Promise}
 */
module.exports.delete=function(project, user) {
	return http.delete(`api/project/delete/${project.id}`, {
		headers: {
			user
		}
	});
};

/**
 * Loads projects for the specified user
 * @param {User} user
 * @returns {Promise}
 */
module.exports.load=function(user) {
	return http.get("api/projects/get", {
		headers: {
			user
		}
	})
		.then((result)=>result.json())
		.then((projects)=>projects.map((project)=>model_factory.createProject({settings: project})));
};

/**
 * Run's the specified project
 * @param {Project} project
 * @param {User} user
 * @returns {Promise}
 */
module.exports.run=function(project, user) {
	return http.put(`api/project/run/${project.id}`, {
		headers: {
			user
		}
	}).then((result)=>result.json());
};

/**
 * Saves project for the specified user
 * @param {Project} project
 * @param {"create"|"update"} mode
 * @param {User} user
 * @returns {Promise}
 */
module.exports.save=function(project, mode, user) {
	const encoded=project.raw;
	if(mode==="create") {
		return http.post("api/project/create", {
			body: encoded,
			headers: {
				user
			}
		});
	} else {
		return http.put("api/project/update", {
			body: encoded,
			headers: {
				user
			}
		});
	}
};

/**
 * Gets status of the specified project
 * @param {Project} project
 * @param {User} user
 * @returns {Promise}
 */
module.exports.status=function(project, user) {
	return http.put(`api/project/status/${project.id}`, {
		headers: {
			user
		}
	}).then((result)=>result.json());
};

/**
 * Stops execution of the specified project
 * @param {Project} project
 * @param {User} user
 * @returns {Promise}
 */
module.exports.stop=function(project, user) {
	return http.put(`api/project/stop/${project.id}`, {
		headers: {
			user
		}
	}).then((result)=>result.json());
};

