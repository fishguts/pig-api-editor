/**
 * User: curtis
 * Date: 3/15/18
 * Time: 9:17 PM
 * Copyright @2018 by Xraymen Inc.
 */

const store=require("./store");
const actions=require("./actions");

import React from "react";
import ReactDOM from "react-dom";
import {Provider, connect} from "react-redux";
import {BrowserRouter, Redirect, Switch} from "react-router-dom";
import {Activity} from "./components/activity";
import {Header} from "./components/header";
import {ProjectEditor} from "./components/project";
import {Projects} from "./components/projects";
import {PigComponent} from "./components/common/component";
import {Route} from "./components/common/route";


// create a dependency on our css so webpack picks it up
require("../res/css/main.css");


/* eslint-disable react/prop-types */
class AppComponent extends PigComponent {
	constructor(props) {
		super(props);
	}

	/**** React Lifecycle ****/
	/**
	 * Hook us into the matrix
	 */
	componentDidMount() {
		this.props.onLoaded();
	}

	render() {
		const {application, onSetHistory}=this.props,
			bodyClass=application.consoleVisible ? "pig-body-short" : "pig-body-tall",
			consoleClass=application.consoleVisible ? "pig-console-opened" : "pig-console-closed";
		return (
			<div className="pig-application">
				<Header/>
				<div className={bodyClass}>
					<BrowserRouter ref={(instance)=>!instance||onSetHistory(instance.history)}>
						<Switch>
							<Route exact path="/project/:projectId" render={({match})=>(
								<Redirect to={`/project/${match.params.projectId}/info`}/>
							)}/>
							<Route exact
								path="/project/:projectId/:subView"
								component={ProjectEditor}
								onMatch={this.props.onProject}/>
							<Route exact
								path="/projects"
								component={Projects}
								onMatch={this.props.onProjects}/>
							<Redirect path="/" to="/projects"/>
						</Switch>
					</BrowserRouter>
				</div>
				<Activity className={consoleClass}/>
			</div>
		);
	}
}

const mapStateToProps=(state)=>({
	application: state.application
});

const mapDispatchToProps=(dispatch)=>({
	onLoaded: ()=> {
		dispatch(actions.application.loaded());
		dispatch(actions.gamuts.load());
		// todo: for now we are going to load projects here. Ultimately it should be from login
		dispatch(actions.project.load());
	},
	onProject: (match)=> {
		dispatch(actions.project.edit(match.params.projectId));
	},
	onProjects: (match)=> {
		// don't think we need to do anything here
	},
	onSetHistory: (history)=> {
		dispatch(actions.history.set(history));
	}
});

const App=connect(
	mapStateToProps,
	mapDispatchToProps
)(AppComponent);

/**
 * Get the party started: Before starting the app we want to:
 *    1. make sure our env is fully configured
 *    2. let everybody know we are about to load the app.
 **/
store.dispatch(actions.application.configure());
store.dispatch(actions.application.load());

ReactDOM.render(
	<Provider store={store}>
		<App/>
	</Provider>,
	document.getElementById("container")
);

