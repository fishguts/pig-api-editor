/**
 * User: curtis
 * Date: 3/25/18
 * Time: 1:28 AM
 * Copyright @2018 by Xraymen Inc.
 */

import React from "react";
import {Button} from "semantic-ui-react";
import propTypes from "prop-types";
import {ToolBar} from "../common/containers";

/**
 * A project toolbar
 * @param {Function} onBack
 * @param {Function} onSave
 * @return {ReactElement}
 */
const ProjectToolbarComponent=function({onBack, onSave}) {
	return (
		<ToolBar className="pig-toolbar">
			<div className="pig-left">
				<Button
					icon="arrow left"
					// Why onMouseDown?  Because of focus issues. See notes within <code>PigInputControl</code>
					onMouseDown={onBack}
				/>
			</div>
			<div className="pig-right">
				<Button
					icon="save"
					onMouseDown={onSave}
				/>
			</div>
		</ToolBar>
	);
};

ProjectToolbarComponent.propTypes={
	onBack: propTypes.func,
	onSave: propTypes.func
};

module.exports.ProjectToolbar=ProjectToolbarComponent;
