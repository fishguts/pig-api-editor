/**
 * User: curtis
 * Date: 3/28/18
 * Time: 7:04 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const propTypes=require("prop-types");
const actions=require("../../actions");
const constant=require("../../common/constant");
const model_factory=require("../../model/factory");

import React from "react";
import {connect} from "react-redux";
import {
	Button,
	Header,
	Table, TableBody, TableCell, TableHeader, TableHeaderCell, TableRow
} from "semantic-ui-react";
import {PigComponent} from "../common/component";
import {ConfirmModal} from "../common/modals/confirm";
import {
	validateValue,
	PigInput,
	PigSearch,
	ValueMediator
} from "../common/controls";
import {getProjectDependencyGraph} from "../../model/dependents";
import {validateDatabaseName} from "../../model/validate";

/**
 * Databases editor.  It is flawed:
 *  1. we have defaults dependent on the type. Ideally a user would select the type before it is
 *   added to the databases list so that we could present type friendly values and attributes
 *  2. Not a flaw but we don't currently have any assignable attributes.
 */
class ProjectDatabasesComponent extends PigComponent {
	constructor(props) {
		super(props, {
			deleteDatabase: null
		});
	}

	/**
	 * Assumes that a confirmation modal dialog is up. The user has chosen the projects fate and
	 * we are the executioners. And whether we delete or not the dialog is closed and life goes on.
	 * @param {Boolean} deleteDatabase
	 * @private
	 */
	_closeConfirmDelete(deleteDatabase=false) {
		if(deleteDatabase) {
			this.props.onDeleteDatabase(this.state.deleteDatabase);
		}
		this.setState({
			deleteDatabase: null
		});
	}

	/**
	 * First we make sure there are no dependencies. If there are not then we give the user a chance to reflect on this move.
	 * @param {Database} database
	 * @private
	 */
	_openConfirmDelete(database) {
		const project=this.props.project,
			graph=getProjectDependencyGraph(project),
			dependents=graph.databaseToReferences(database);
		if(dependents.length===0) {
			this.setState({
				deleteDatabase: database
			});
		} else {
			this.props.onAddActivity(model_factory.createActivity({
				details: _.map(dependents, "path"),
				message: `Cannot delete due to dependencies on database "${project.renderTemplateString(database.name)}"`,
				severity: constant.severity.ERROR
			}));
		}
	}

	render() {
		if(this.props.project) {
			return this._renderDatabasesTable();
		}
		return null;
	}

	_renderDatabasesTable() {
		const {gamuts, project, onAddDatabase, onUpdateDatabase}=this.props;
		return (
			<div className="project-databases">
				<Header>Databases</Header>
				<Table celled selectable>
					<TableHeader>
						<TableRow key="header">
							<TableHeaderCell key="name">Name</TableHeaderCell>
							<TableHeaderCell key="type">Type</TableHeaderCell>
							<TableHeaderCell key="desc">Description</TableHeaderCell>
							<TableHeaderCell key="connect">Connect</TableHeaderCell>
							<TableHeaderCell key="attributes">Attributes</TableHeaderCell>
							<TableHeaderCell key="action" style={{width: 65}}>
								<Button icon="add" onClick={onAddDatabase}/>
							</TableHeaderCell>
						</TableRow>
					</TableHeader>
					<TableBody>
						{
							project.databases.map(database=> {
								return (
									<TableRow key={database.id}>
										<TableCell key="name">
											<PigInput
												// we want to force validation whenever the name or type changes
												key={`${database.type}:${database.name}`}
												className="pig-input-trim"
												mediator={new ValueMediator({
													isValid: _.partial(validateDatabaseName, project.databases, database),
													onCommit: onUpdateDatabase,
													sourceObject: database,
													sourceField: "name",
													title: "name",
													titleType: "placeholder"
												})}
											/>
										</TableCell>
										<TableCell key="type">
											<PigSearch
												className="pig-input-trim"
												mediator={new ValueMediator({
													onCommit: (database)=>{
														onUpdateDatabase(Object.assign(
															database,
															_.find(gamuts.database.types, {value: database.type}).defaults
														));
													},
													sourceObject: database,
													sourceField: "type",
													sourceValues: gamuts.database.types
												})}
											/>
										</TableCell>
										<TableCell key="desc">
											<PigInput
												className="pig-input-chubby"
												mediator={new ValueMediator({
													onCommit: onUpdateDatabase,
													sourceObject: database,
													sourceField: "desc",
													title: "description",
													titleType: "placeholder"
												})}
											/>
										</TableCell>
										<TableCell key="connect">
											<PigInput
												className="pig-input-url-fat"
												mediator={new ValueMediator({
													isValid: (value)=>validateValue.url(project.variables.renderTemplateString(value)),
													onCommit: onUpdateDatabase,
													sourceObject: database,
													sourceField: "connection",
													title: "name",
													titleType: "placeholder"
												})}
											/>
										</TableCell>
										<TableCell key="atts">
											<PigSearch
												className="pig-flex-average"
												multiple
												placeholder="attributes"
												mediator={new ValueMediator({
													onCommit: onUpdateDatabase,
													sourceObject: database,
													sourceField: "attributes",
													sourceValues: _.find(gamuts.database.types, {value: database.type}).attributes
												})}
											/>
										</TableCell>
										<TableCell key="action">
											<Button icon="delete" onClick={this._openConfirmDelete.bind(this, database)}/>
										</TableCell>
									</TableRow>
								);
							})
						}
					</TableBody>
				</Table>
				<ConfirmModal
					heading="Delete Database"
					open={this.state.deleteDatabase!==null}
					text={`Are you sure you want to delete "${project.renderTemplateString(_.get(this.state.deleteDatabase, "name"))}"?`}
					onNo={this._closeConfirmDelete.bind(this, false)}
					onYes={this._closeConfirmDelete.bind(this, true)}
				/>
			</div>
		);
	}
}

ProjectDatabasesComponent.propTypes={
	project: propTypes.object,
	onAddActivity: propTypes.func,
	onAddDatabase: propTypes.func,
	onDeleteDatabase: propTypes.func,
	onUpdateDatabase: propTypes.func
};

const mapStateToProps=(state)=>({
	gamuts: state.gamuts,
	project: state.projects.selected
});

const mapDispatchToProps=(dispatch)=>({
	onAddActivity: (activity)=>dispatch(actions.activity.add(activity)),
	onAddDatabase: ()=>dispatch(actions.project.addDatabase()),
	onDeleteDatabase: (database)=>dispatch(actions.project.deleteDatabase(database)),
	onUpdateDatabase: (database)=>dispatch(actions.project.updateDatabase(database))
});

module.exports.ProjectDatabases=connect(
	mapStateToProps,
	mapDispatchToProps
)(ProjectDatabasesComponent);
