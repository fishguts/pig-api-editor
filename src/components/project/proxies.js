/**
 * User: curtis
 * Date: 3/28/18
 * Time: 7:04 PM
 * Copyright @2018 by Xraymen Inc.
 */

const propTypes=require("prop-types");

import React from "react";
import {connect} from "react-redux";
import {Header} from "semantic-ui-react";
import {PigComponent} from "../common/component";


class ProjectProxiesComponent extends PigComponent {
	render() {
		if(this.props.project) {
			return (
				<Header>Proxies</Header>
			);
		}
		return null;
	}
}

ProjectProxiesComponent.propTypes={
	project: propTypes.object
};

const mapStateToProps=(state)=>({
	project: state.projects.selected
});

const mapDispatchToProps=(dispatch, props)=>({

});

module.exports.ProjectProxies=connect(
	mapStateToProps,
	mapDispatchToProps
)(ProjectProxiesComponent);
