/**
 * User: curtis
 * Date: 3/28/18
 * Time: 7:04 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const propTypes=require("prop-types");
const actions=require("../../actions");
const constant=require("../../common/constant");
const model_factory=require("../../model/factory");

import React from "react";
import {connect} from "react-redux";
import {
	Button,
	Header,
	Table,
	TableBody,
	TableCell,
	TableHeader,
	TableHeaderCell,
	TableRow
} from "semantic-ui-react";
import {PigComponent} from "../common/component";
import {
	sourceValues,
	validateValue,
	PigInput,
	PigSearch,
	ValueMediator
} from "../common/controls";
import {ConfirmModal} from "../common/modals/confirm";
import {sortPropertyToValue, sortRequestToUpdateAction} from "../mediator";
import {getProjectDependencyGraph} from "../../model/dependents";
import {
	validateEnvName,
	validateEnvValue,
	validateVariableName
} from "../../model/validate";


class ProjectEnvsComponent extends PigComponent {
	constructor(props) {
		super(props, {
			deleteEnvironment: null,
			deleteVariable: null
		});
	}

	/**
	 * Assumes that a confirmation modal dialog is up. The user has chosen the projects fate and
	 * we are the executioners. And whether we delete or not the dialog is closed and life goes on.
	 * @param {Boolean} deleteEnv
	 * @private
	 */
	_closeConfirmDeleteEnvironment(deleteEnv=false) {
		if(deleteEnv) {
			this.props.onDeleteEnvironment(this.state.deleteEnvironment);
		}
		this.setState({
			deleteEnvironment: null
		});
	}

	/**
	 * Assumes that a confirmation modal dialog is up. The user has chosen the projects fate and
	 * we are the executioners. And whether we delete or not the dialog is closed and life goes on.
	 * @param {Boolean} deleteVariable
	 * @private
	 */
	_closeConfirmDeleteVariable(deleteVariable=false) {
		if(deleteVariable) {
			this.props.onDeleteVariable(this.state.deleteVariable);
		}
		this.setState({
			deleteVariable: null
		});
	}

	/**
	 * We give the user a chance to reflect on this move.
	 * @param {Environment} env
	 * @private
	 */
	_openConfirmDeleteEnvironment(env) {
		this.setState({
			deleteEnvironment: env
		});
	}

	/**
	 * We give the user a chance to reflect on this move.
	 * @param {Variable} variable
	 * @private
	 */
	_openConfirmDeleteVariable(variable) {
		const project=this.props.project,
			graph=getProjectDependencyGraph(project),
			dependents=graph.variableToReference(variable.name);
		if(dependents.length===0) {
			this.setState({
				deleteVariable: variable
			});
		} else {
			this.props.onAddActivity(model_factory.createActivity({
				details: _.map(dependents, "path"),
				message: `Cannot delete due to dependencies on variable "${variable.name}"`,
				severity: constant.severity.ERROR
			}));
		}
	}

	render() {
		const {project}=this.props;
		if(project) {
			return (
				<div className="project-envs">
					{this._renderEnvironmentTable()}
					{this._renderVariablesTable()}
				</div>
			);
		}
		return null;
	}

	_renderEnvironmentTable() {
		const {
			application, gamuts, project,
			onAddEnvironment, onUpdateEnvironment, onSortEnvironments
		}=this.props;
		return (
			<div key="environments" className="project-envs-environments">
				<Header>Environments</Header>
				<Table celled selectable sortable>
					<TableHeader>
						<TableRow key="header">
							<TableHeaderCell key="name"
								sorted={sortPropertyToValue({application, majorId: project.id, minorId: "envs", property: "name"})}
								onClick={_.partial(onSortEnvironments, application, project, "name")}
							>
								Name
							</TableHeaderCell>
							<TableHeaderCell key="value"
								sorted={sortPropertyToValue({application, majorId: project.id, minorId: "envs", property: "value"})}
								onClick={_.partial(onSortEnvironments, application, project, "value")}
							>
								Value
							</TableHeaderCell>
							<TableHeaderCell key="desc"
								sorted={sortPropertyToValue({application, majorId: project.id, minorId: "envs", property: "desc"})}
								onClick={_.partial(onSortEnvironments, application, project, "desc")}
							>
								Description
							</TableHeaderCell>
							<TableHeaderCell key="debug"
								sorted={sortPropertyToValue({application, majorId: project.id, minorId: "envs", property: "debug"})}
								onClick={_.partial(onSortEnvironments, application, project, "debug")}
							>
								Debug
							</TableHeaderCell>
							<TableHeaderCell key="log"
								sorted={sortPropertyToValue({application, majorId: project.id, minorId: "envs", property: "log.level"})}
								onClick={_.partial(onSortEnvironments, application, project, "log.level")}
							>
								Log Level
							</TableHeaderCell>
							<TableHeaderCell key="action" style={{width: 65}}>
								<Button icon="add" onClick={onAddEnvironment}/>
							</TableHeaderCell>
						</TableRow>
					</TableHeader>
					<TableBody>
						{
							project.envs.map((env)=>
								<TableRow
									disabled={_.includes(env.attributes, "read-only")}
									key={env.id}
								>
									<TableCell key="name">
										<PigInput
											className="pig-input-trim"
											mediator={new ValueMediator({
												isValid: _.partial(validateEnvName, project.envs, env),
												onCommit: onUpdateEnvironment,
												sourceObject: env,
												sourceField: "name",
												title: "name",
												titleType: "placeholder"
											})}
										/>
									</TableCell>
									<TableCell key="value">
										<PigInput
											className="pig-input-trim"
											mediator={new ValueMediator({
												isValid: _.partial(validateEnvValue, project.envs, env),
												onCommit: onUpdateEnvironment,
												sourceObject: env,
												sourceField: "value",
												title: "value",
												titleType: "placeholder"
											})}
										/>

									</TableCell>
									<TableCell key="desc">
										<PigInput
											className="pig-input-fat"
											mediator={new ValueMediator({
												onCommit: onUpdateEnvironment,
												sourceObject: env,
												sourceField: "desc",
												title: "description",
												titleType: "placeholder"
											})}
										/>
									</TableCell>
									<TableCell key="debug">
										<PigSearch
											className="pig-input-trim"
											mediator={new ValueMediator({
												onCommit: onUpdateEnvironment,
												sourceObject: env,
												sourceField: "debug",
												sourceValues: sourceValues.boolean
											})}
										/>
									</TableCell>
									<TableCell key="log">
										<PigSearch
											className="pig-input-trim"
											mediator={new ValueMediator({
												onCommit: onUpdateEnvironment,
												sourceObject: env,
												sourceField: "log.level",
												sourceValues: gamuts.log.level
											})}
										/>
									</TableCell>
									<TableCell key="action">
										<Button
											icon="delete"
											onClick={this._openConfirmDeleteEnvironment.bind(this, env)}
										/>
									</TableCell>
								</TableRow>
							)
						}
					</TableBody>
				</Table>
				<ConfirmModal
					heading="Delete Environment"
					open={this.state.deleteEnvironment!==null}
					text={`Are you sure you want to delete "${_.get(this.state.deleteEnvironment, "name")}"?`}
					onNo={this._closeConfirmDeleteEnvironment.bind(this, false)}
					onYes={this._closeConfirmDeleteEnvironment.bind(this, true)}
				/>
			</div>
		);
	}

	_renderVariablesTable() {
		const {gamuts, project, onAddVariable, onUpdateVariable}=this.props,
			envsData=[{id: "default", name: "default"}]
				.concat(project.envs.map((env)=>({
					env,
					id: env.id,
					name: env.name
				}))),
			defaultVariables=project.variables.all();
		return (
			<div key="variables" className="project-envs-variables">
				<Header>Variables</Header>
				<Table celled selectable>
					<TableHeader>
						<TableRow key="header">
							<TableHeaderCell key="name">name</TableHeaderCell>
							<TableHeaderCell key="scope">scope</TableHeaderCell>
							{
								envsData.map((envData)=><TableHeaderCell key={envData.id}>{envData.name}</TableHeaderCell>)
							}
							<TableHeaderCell key="action" style={{width: 65}}>
								<Button icon="add" onClick={onAddVariable}/>
							</TableHeaderCell>
						</TableRow>
					</TableHeader>
					<TableBody>
						{
							defaultVariables.map((defaultVariable)=>
								<TableRow key={defaultVariable.id}>
									<TableCell key="name">
										<PigInput
											className="pig-input-trim"
											mediator={new ValueMediator({
												isValid: _.partial(validateVariableName, defaultVariables, defaultVariable),
												onCommit: onUpdateVariable,
												sourceObject: defaultVariable,
												sourceField: "name",
												title: "name",
												titleType: "placeholder"
											})}
										/>
									</TableCell>
									<TableCell key="scope">
										<PigSearch
											className="pig-input-skinny"
											mediator={new ValueMediator({
												onCommit: onUpdateVariable,
												sourceObject: defaultVariable,
												sourceField: "scope",
												sourceDefault: "global",
												sourceValues: gamuts.variable.scope
											})}
										/>
									</TableCell>
									{
										envsData.map((envData)=> {
											// We need to give our mediator an object whether one exists or not so that he has something
											// to write to. So, if the variable for the current environment does not exist then we create
											// an empty guy that will only get populated if it's value changes.
											const variable=project.variables.get(defaultVariable.id, envData.env)
												|| defaultVariable.clone({value: "", env: envData.env});
											return (
												<TableCell key={envData.name}>
													<PigInput
														// Note: I introduced type and we have parsing to support it but empty numbers become
														// NaN and it quickly become more trouble than it is worth so we are going to commit
														// everything as strings.
														mediator={new ValueMediator({
															isValid: (envData.env) ? undefined : validateValue.notEmpty,
															onCommit: onUpdateVariable,
															sourceObject: variable,
															sourceField: "value",
															title: defaultVariable.value,
															titleType: "placeholder"
														})}
													/>
												</TableCell>
											);
										})
									}
									<TableCell key="action">
										<Button icon="delete" onClick={this._openConfirmDeleteVariable.bind(this, defaultVariable)}/>
									</TableCell>
								</TableRow>
							)
						}
					</TableBody>
				</Table>
				<ConfirmModal
					heading="Delete Variable"
					open={this.state.deleteVariable!==null}
					text={`Are you sure you want to delete "${_.get(this.state.deleteVariable, "name")}"?`}
					onNo={this._closeConfirmDeleteVariable.bind(this, false)}
					onYes={this._closeConfirmDeleteVariable.bind(this, true)}
				/>
			</div>
		);
	}
}

ProjectEnvsComponent.propTypes={
	project: propTypes.object,
	onAddActivity: propTypes.func,
	onAddEnvironment: propTypes.func,
	onDeleteEnvironment: propTypes.func,
	onUpdateEnvironment: propTypes.func,
	onSortEnvironments: propTypes.func,
	onAddVariable: propTypes.func,
	onDeleteVariable: propTypes.func,
	onUpdateVariable: propTypes.func
};

const mapStateToProps=(state)=>({
	application: state.application,
	gamuts: state.gamuts,
	project: state.projects.selected
});

const mapDispatchToProps=(dispatch)=>({
	onAddActivity: (activity)=>dispatch(actions.activity.add(activity)),
	onAddEnvironment: ()=>dispatch(actions.project.addEnvironment()),
	onDeleteEnvironment: (env)=>dispatch(actions.project.deleteEnvironment(env)),
	onUpdateEnvironment: (env)=>dispatch(actions.project.updateEnvironment(env)),
	onSortEnvironments: (application, project, property)=> {
		const action=sortRequestToUpdateAction({application, majorId: project.id, minorId: "envs", property});
		dispatch(actions.project.sortEnvironments(project.envs, action.value.property, action.value.reverse));
		dispatch(actions.application.setSessionSetting(action));
	},
	onAddVariable: ()=>dispatch(actions.project.addVariable()),
	onDeleteVariable: (variable)=>dispatch(actions.project.deleteVariable(variable)),
	onUpdateVariable: (variable)=> {
		// we want all env variables to default to the global defaults. We don't explicity have a UI mechanism
		// for deleting them. The implication is that once a field is stripped of non-whitespace that it's a goner.
		if(variable.env && _.isEmpty(variable.value)) {
			dispatch(actions.project.deleteVariable(variable));
		} else {
			dispatch(actions.project.setVariable(variable));
		}
	}
});

module.exports.ProjectEnvs=connect(
	mapStateToProps,
	mapDispatchToProps
)(ProjectEnvsComponent);
