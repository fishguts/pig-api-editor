/**
 * User: curtis
 * Date: 3/17/18
 * Time: 12:49 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const actions=require("../../actions");
const log=require("pig-core").log;

import React from "react";
import {connect} from "react-redux";
import {Icon, Menu, MenuItem} from "semantic-ui-react";
import {ProjectDatabases} from "./databases";
import {ProjectEnvs} from "./envs";
import {ProjectCore} from "./core";
import {ProjectModels} from "./models";
import {ProjectProxies} from "./proxies";
import {ProjectToolbar} from "./toolbar";
import {PigComponent} from "../common/component";

const avatarStyle={
	color: "#889",
	backgroundColor: "#eee"
};

/**
 * This is the sub-view editor itself. He just finds out who should be displayed and
 * @param {string} subView
 * @returns {ReactElement}
 * @constructor
 */
const EditorComponent=function({subView}) {
	switch(subView) {
		case "core":
			return <ProjectCore/>;
		case "databases":
			return <ProjectDatabases/>;
		case "envs":
			return <ProjectEnvs/>;
		case "models":
			return <ProjectModels/>;
		case "proxies":
			return <ProjectProxies/>;
		default: {
			log.error(`components.project(): unknown subview type='${subView}`);
			return <ProjectCore/>;
		}
	}
};

class ProjectEditorComponent extends PigComponent {
	/**
	 * Our save model is automated with the option to save as they go.  So here we are. They are leaving. We are saving.
	 */
	componentWillUnmount() {
		const {project, onSave}=this.props;
		if(_.get(project, "dirty")) {
			onSave(project);
		}
	}

	render() {
		const {match, onBack, onSave}=this.props,
			{subView}=this.props.match.params;
		return [
			<ProjectToolbar
				key="toolbar"
				onBack={onBack}
				onSave={onSave}
			/>,
			<div key="body" className="project-body">
				<Menu className="project-menu" compact icon="labeled" vertical>
					<MenuItem key="core" active={subView==="core"} onClick={this.props.onSubview.bind(this, "core")}>
						<Icon name="content"/>
						Core
					</MenuItem>
					<MenuItem key="databases" active={subView==="databases"} onClick={this.props.onSubview.bind(this, "databases")}>
						<Icon name="database"/>
						Databases
					</MenuItem>
					<MenuItem key="envs" active={subView==="envs"} onClick={this.props.onSubview.bind(this, "envs")}>
						<Icon name="envira"/>
						Envs
					</MenuItem>
					<MenuItem key="models" active={subView==="models"} onClick={this.props.onSubview.bind(this, "models")}>
						<Icon name="table"/>
						Models
					</MenuItem>
					<MenuItem key="proxies" active={subView==="proxies"} onClick={this.props.onSubview.bind(this, "proxies")}>
						<Icon name="intersex"/>
						Proxies
					</MenuItem>
				</Menu>
				<div className="project-editor">
					<EditorComponent {...this.props.match.params}/>
				</div>
				<div className="pig-clear"/>
			</div>
		];
	}
}

const mapDispatchToProps=(dispatch, props)=>({
	onSubview: (view)=>dispatch(actions.history.goToProject(props.match.params.projectId, view)),
	onBack: ()=> {
		// We are deferring execution to get around commit on loss of focus race conditions
		setTimeout(()=> {
			dispatch(actions.project.save({mode: "update"}));
			dispatch(actions.history.goToProjects());
		}, 0);
	},
	onSave: ()=> {
		// We are deferring execution to get around commit on loss of focus race conditions
		setTimeout(()=> {
			dispatch(actions.project.save({mode: "update"}));
		}, 0);
	}
});


module.exports.ProjectEditor=connect(
	null,
	mapDispatchToProps
)(ProjectEditorComponent);
