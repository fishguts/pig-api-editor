/**
 * User: curtis
 * Date: 3/28/18
 * Time: 7:04 PM
 * Copyright @2018 by Xraymen Inc.
 */

const propTypes=require("prop-types");
const actions=require("../../actions/project");

import React from "react";
import {connect} from "react-redux";
import {
	Form, FormInput, FormDropdown, FormTextArea,
	Grid, GridColumn, GridRow,
	Header
} from "semantic-ui-react";
import {validateValue, ValueMediator, PigInput, PigSearch} from "../common/controls";
import {PigComponent} from "../common/component";


class ProjectCoreComponent extends PigComponent {
	render() {
		const {gamuts, project, onUpdateInfo, onUpdateServer, onUpdateTargets}=this.props;
		if(!this.props.project) {
			return null;
		}
		return (
			<Grid className="project-core">
				<GridRow>
					<GridColumn width={5}>
						<Header className="pig-column-header">Properties</Header>
						<Form>
							<PigInput
								elementType={FormInput}
								mediator={new ValueMediator({
									isValid: validateValue.notEmpty,
									onCommit: onUpdateInfo,
									sourceObject: project.info,
									sourceField: "name",
									title: "Name"
								})}
							/>
							<PigInput
								elementType={FormTextArea}
								mediator={new ValueMediator({
									onCommit: onUpdateInfo,
									sourceObject: project.info,
									sourceField: "desc",
									title: "Description"
								})}
							/>
							<PigInput
								elementType={FormInput}
								mediator={new ValueMediator({
									isValid: validateValue.semver,
									onCommit: onUpdateInfo,
									sourceObject: project.info,
									sourceField: "version",
									title: "Version"
								})}
							/>
						</Form>
					</GridColumn>
					<GridColumn width={7}>
						<Header className="pig-column-header">Legal</Header>
						<Form>
							<PigInput
								elementType={FormInput}
								mediator={new ValueMediator({
									isValid: validateValue.urlFactory({allowEmpty: true}),
									onCommit: onUpdateInfo,
									sourceObject: project.info,
									sourceField: "legal.terms.url",
									title: "Terms URL"
								})}
							/>
							<PigInput
								elementType={FormInput}
								mediator={new ValueMediator({
									isValid: validateValue.notEmpty,
									onCommit: onUpdateInfo,
									sourceObject: project.info,
									sourceField: "legal.license.name",
									title: "License"
								})}
							/>
							<PigInput
								elementType={FormInput}
								mediator={new ValueMediator({
									isValid: validateValue.urlFactory({allowEmpty: true}),
									onCommit: onUpdateInfo,
									sourceObject: project.info,
									sourceField: "legal.license.url",
									title: "License URL"
								})}
							/>
						</Form>
					</GridColumn>
					<GridColumn width={4}>
						<Header className="pig-column-header">Information</Header>
						<Form>
							<PigInput
								disabled={true}
								elementType={FormInput}
								mediator={new ValueMediator({
									format: (value)=>value.toLocaleString(),
									sourceObject: project,
									sourceField: "timestampCreated",
									title: "Create"
								})}
							/>
							<PigInput
								disabled={true}
								elementType={FormInput}
								mediator={new ValueMediator({
									format: (value)=>value.toLocaleString(),
									sourceObject: project,
									sourceField: "timestampUpdated",
									title: "Updated"
								})}
							/>
						</Form>
					</GridColumn>
				</GridRow>
				<GridRow>
					<GridColumn width={12}>
						<Header className="pig-column-header">Targets</Header>
						<Form>
							<PigSearch
								elementType={FormDropdown}
								multiple
								mediator={new ValueMediator({
									isValid: validateValue.notEmpty,
									onCommit: onUpdateTargets,
									sourceObject: project,
									sourceField: "targets",
									sourceValues: gamuts.targets,
									title: "Languages"
								})}
							/>
						</Form>
					</GridColumn>
				</GridRow>
				<GridRow>
					<GridColumn width={12}>
						<Header className="pig-column-header">Server</Header>
						<Form>
							<PigInput
								key="url"
								elementType={FormInput}
								mediator={new ValueMediator({
									isValid: (value)=>validateValue.url(project.variables.renderTemplateString(value)),
									onCommit: onUpdateServer,
									sourceObject: project.server,
									sourceField: "url",
									title: "Url"
								})}
							/>
							<PigInput
								key="desc"
								elementType={FormTextArea}
								mediator={new ValueMediator({
									onCommit: onUpdateServer,
									sourceObject: project.server,
									sourceField: "desc",
									title: "Description"
								})}
							/>
						</Form>
					</GridColumn>
				</GridRow>
			</Grid>
		);
	}
}

ProjectCoreComponent.propTypes={
	project: propTypes.object,
	onUpdateInfo: propTypes.func,
	onUpdateServer: propTypes.func
};

const mapStateToProps=(state)=>({
	gamuts: state.gamuts,
	project: state.projects.selected
});

const mapDispatchToProps=(dispatch)=>({
	onUpdateInfo: (info)=>dispatch(actions.updateInfo(info)),
	onUpdateServer: (server)=>dispatch(actions.updateServer(server)),
	onUpdateTargets: (targets)=>dispatch(actions.updateProject(targets))
});

module.exports.ProjectCore=connect(
	mapStateToProps,
	mapDispatchToProps
)(ProjectCoreComponent);
