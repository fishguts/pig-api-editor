/**
 * User: curtis
 * Date: 3/28/18
 * Time: 7:04 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const propTypes=require("prop-types");
const actions=require("../../../actions/project");
const model_factory=require("../../../model/factory");

import React from "react";
import {
	Button,
	Table, TableBody, TableCell, TableHeader, TableHeaderCell, TableRow
} from "semantic-ui-react";
import {connect} from "react-redux";
import {ViewSelector} from "./modals";
import {ModelEditor} from "./model";
import {ViewEditor} from "./view";
import {sortModels, sortViews} from "../../mediator";
import {PigComponent} from "../../common/component";
import {ConfirmModal} from "../../common/modals/confirm";
import {PigInput, ValueMediator} from "../../common/controls";
import {findById, isEqualById, modelToId} from "../../../model";
import {validateModelName, validateModelViewName} from "../../../model/validate";


class ProjectModelsComponent extends PigComponent {
	constructor(properties) {
		super(properties);
		this.state={
			deleteModel: null,
			deleteView: null,
			selectedModelId: null,
			selectedViewId: null,
			viewSelector: {
				opened: false,
				model: null
			}
		};
	}

	_addModel(model=model_factory.createDataModel()) {
		this.props.onAddModel(model);
		this._selectModel(model);
	}

	_addView({model, path, serviceId}) {
		const view=model_factory.createDataView({model, path});
		view.serviceId=serviceId;
		this.setState({
			selectedViewId: view.id,
			viewSelector: {
				opened: false
			}
		});
		this.props.onAddView(model, view);
	}

	/**
	 * Assumes that a confirmation modal dialog is up. The user has chosen the model's fate and
	 * we are the executioners. And whether we delete or not the dialog is closed and life goes on.
	 * @param {Boolean} deleteModel
	 * @private
	 */
	_closeConfirmDeleteModel(deleteModel=false) {
		if(deleteModel) {
			// if he is currently selected then let's move the selection to another model
			if(isEqualById(this.state.selectedModelId, this.state.deleteModel)) {
				this._selectModel(this._getNextModel(this.state.deleteModel));
			}
			this.props.onDeleteModel(this.state.deleteModel);
		}
		this.setState({
			deleteModel: null
		});
	}

	/**
	 * Assumes that a confirmation modal dialog is up. The user has chosen the model's fate and
	 * we are the executioners. And whether we delete or not the dialog is closed and life goes on.
	 * @param {Boolean} deleteView
	 * @private
	 */
	_closeConfirmDeleteView(deleteView=false) {
		if(deleteView) {
			const model=_.find(this.props.project.models, {id: this.state.selectedModelId});
			// if he is currently selected then let's move the selection to another model
			if(isEqualById(this.state.selectedViewId, this.state.deleteView)) {
				this._selectView(this._getNextView(model, this.state.deleteView));
			}
			this.props.onDeleteView(model, this.state.deleteView);
		}
		this.setState({
			deleteView: null
		});
	}

	/**
	 * Because we sort models by title the order in the UI doesn't necessarily match that in our DB. This guy
	 * returns the model that is next by sort order
	 * @param {DataModel} model
	 * @returns {DataModel|undefined}
	 * @private
	 */
	_getNextModel(model) {
		const models=sortModels(this.props.project.models),
			index=_.findIndex(models, {id: model.id});
		return models[(index+1<models.length) ? index+1 : index-1];
	}

	/**
	 * Because we sort views by title the order in the UI doesn't necessarily match that in our DB. This guy
	 * returns the view that is next by sort order
	 * @param {DataModel} model
	 * @param {DataView} view
	 * @returns {DataView|undefined}
	 * @private
	 */
	_getNextView(model, view) {
		const views=sortViews(model),
			index=_.findIndex(views, {id: view.id});
		return views[(index+1<model.views.length) ? index+1 : index-1];
	}

	/**
	 * Assumes that the view is in the currently selected model
	 * @param {DataView} view
	 * @returns {string}
	 * @private
	 */
	_getViewName(view) {
		return (view)
			? view.getName(_.find(this.props.project.models, {id: this.state.selectedModelId}))
			: "";
	}

	/**
	 * Give the user a chance to reflect on this move.
	 * @param {DataModel} model
	 * @private
	 */
	_openConfirmDeleteModel(model) {
		this.setState({
			deleteModel: model
		});
	}

	/**
	 * Give the user a chance to reflect on this move.
	 * @param {DataView} view
	 * @private
	 */
	_openConfirmDeleteView(view) {
		this.setState({
			deleteView: view
		});
	}

	_openViewSelector(model) {
		this.setState({
			viewSelector: {
				opened: true,
				// there can only one view selected at a time and due to the way we render the modal we always
				// know the model so this isn't really needed. But I like explicit things.
				model: model
			}
		});
	}

	_selectModel(model) {
		if(!isEqualById(this.state.selectedModelId, model)) {
			if(model && model.views.length>0) {
				this.setState({
					selectedModelId: modelToId(model),
					selectedViewId: sortViews(model,
						_(model.views)
							.groupBy("transport.method")
							.sortBy("0.transport.method")
							.get(0)
					)[0].id
				});
			} else {
				this.setState({
					selectedModelId: modelToId(model),
					selectedViewId: null
				});
			}
		}
	}

	_selectView(view) {
		if(!isEqualById(this.state.selectedViewId, view)) {
			this.setState({
				selectedViewId: modelToId(view)
			});
		}
	}

	render() {
		const {project}=this.props;
		if(project) {
			const model=findById(this.state.selectedModelId, project.models),
				view=(model)
					? findById(this.state.selectedViewId, model.views)
					: null;
			return (
				<div key={`models.${project.id}`} className="project-models">
					<div key="model" className="pig-row">
						{this._renderModelManager()}
						<ModelEditor key="editor" model={model}/>
					</div>
					<div key="view" className="pig-row">
						{this._renderViewManager()}
						<ViewEditor key="editor" model={model} view={view}/>
					</div>
				</div>
			);
		}
		return null;
	}

	_renderModelManager() {
		const {project, onUpdateModel}=this.props;
		return (
			<div className="project-models-menu">
				<Table selectable>
					<TableHeader>
						<TableRow>
							<TableHeaderCell key="name" style={{minWidth: 125}}>Models</TableHeaderCell>
							<TableHeaderCell key="action" style={{width: 65}}>
								<Button icon="add" onClick={()=>this._addModel()}/>
							</TableHeaderCell>
						</TableRow>
					</TableHeader>
					<TableBody>
						{
							sortModels(project.models)
								.map((model)=>
									<TableRow
										key={model.id}
										active={isEqualById(this.state.selectedModelId, model)}
										onClick={()=>this._selectModel(model)}
									>
										<TableCell>
											<PigInput
												className="pig-input-average"
												mediator={new ValueMediator({
													isValid: _.partial(validateModelName, project.models, model),
													onCommit: onUpdateModel,
													sourceObject: model,
													sourceField: "name",
													title: "name",
													titleType: "placeholder"
												})}
											/>
										</TableCell>
										<TableCell>
											<Button key="action" icon="delete" size="small" onClick={this._openConfirmDeleteModel.bind(this, model)}
											/>
										</TableCell>
									</TableRow>
								)
						}
					</TableBody>
				</Table>
				{
					<ConfirmModal
						heading="Delete Model"
						open={this.state.deleteModel!==null}
						text={`Are you sure you want to delete "${_.get(this.state.deleteModel, "name")}"?`}
						onNo={this._closeConfirmDeleteModel.bind(this, false)}
						onYes={this._closeConfirmDeleteModel.bind(this, true)}
					/>
				}
			</div>
		);
	}

	_renderViewManager() {
		const {project, onUpdateView}=this.props,
			model=findById(this.state.selectedModelId, project.models);
		if(model) {
			const viewGroups=_.chain(model.views)
				.groupBy("transport.method")
				.sortBy("0.transport.method")
				.map(_.partial(sortViews, model))
				.value();
			return (
				<div className="project-models-menu">
					<Table attached="top">
						<TableHeader>
							<TableRow>
								<TableHeaderCell key="name" style={{minWidth: 125}}>Views</TableHeaderCell>
								<TableHeaderCell key="action" style={{width: 65}}>
									<Button icon="add" onClick={()=>this._openViewSelector(model)}/>
								</TableHeaderCell>
							</TableRow>
						</TableHeader>
					</Table>
					{
						viewGroups.map((group)=> {
							return (
								<Table key={group[0].transport.method} attached selectable>
									<TableHeader>
										<TableRow>
											<TableHeaderCell key="name">{group[0].transport.method.toUpperCase()}</TableHeaderCell>
											<TableHeaderCell key="action"/>
										</TableRow>
									</TableHeader>
									<TableBody>
										{
											group.map((view)=>
												<TableRow
													key={view.id}
													active={isEqualById(this.state.selectedViewId, view)}
													onClick={()=>this.setState({selectedViewId: view.id})}
												>
													<TableCell>
														<PigInput
															className="pig-input-average"
															mediator={new ValueMediator({
																isValid: _.partial(validateModelViewName, model, view),
																onCommit: _.partial(onUpdateView, model),
																sourceDefault: view.generateDefaultName(model),
																sourceObject: view,
																sourceField: "name",
																title: view.generateDefaultName(model),
																titleType: "placeholder"
															})}
														/>
													</TableCell>
													<TableCell>
														<Button
															key="action"
															icon="delete"
															onClick={this._openConfirmDeleteView.bind(this, view)}
														/>
													</TableCell>
												</TableRow>
											)
										}
									</TableBody>
								</Table>
							);
						})
					}
					{this._renderViewSelector(model)}
					{
						<ConfirmModal
							heading="Delete View"
							open={this.state.deleteView!==null}
							text={`Are you sure you want to delete "${this._getViewName(this.state.deleteView)}"?`}
							onNo={this._closeConfirmDeleteView.bind(this, false)}
							onYes={this._closeConfirmDeleteView.bind(this, true)}
						/>
					}
				</div>
			);
		}
		return null;
	}

	_renderViewSelector(model) {
		if(this.state.viewSelector.opened) {
			return (
				<ViewSelector
					gamuts={this.props.gamuts}
					project={this.props.project}
					onSelect={({path, serviceId})=>this._addView({model, path, serviceId})}
					onClose={()=>this.setState({
						viewSelector: {
							opened: false
						}
					})}
				/>
			);
		}
		return null;
	}
}

ProjectModelsComponent.propTypes={
	gamuts: propTypes.object,
	project: propTypes.object,
	onAddModel: propTypes.func,
	onDeleteModel: propTypes.func,
	onUpdateModel: propTypes.func,
	onAddView: propTypes.func,
	onDeleteView: propTypes.func,
	onUpdateView: propTypes.func
};

const mapStateToProps=(state)=>({
	project: state.projects.selected,
	gamuts: state.gamuts
});

const mapDispatchToProps=(dispatch)=>({
	onAddModel: (model)=>dispatch(actions.addDataModel(model)),
	onDeleteModel: (model)=>dispatch(actions.deleteDataModel(model)),
	onUpdateModel: (model)=>dispatch(actions.updateDataModel(model)),
	onAddView: (model, view)=>dispatch(actions.addDataView(model, view)),
	onDeleteView: (model, view)=>dispatch(actions.deleteDataView(model, view)),
	onUpdateView: (model, view)=>dispatch(actions.updateDataView(model, view))
});

module.exports.ProjectModels=connect(
	mapStateToProps,
	mapDispatchToProps
)(ProjectModelsComponent);
