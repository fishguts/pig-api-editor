/**
 * User: curtis
 * Date: 5/14/18
 * Time: 9:48 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const propTypes=require("prop-types");
const constant=require("../../../common/constant");
const actions=require("../../../actions/index");
const model_util=require("../../../model/util");

import React from "react";
import {connect} from "react-redux";
import {
	Button, Header,
	Form, FormDropdown, FormInput, FormTextArea,
	Table, TableBody, TableCell, TableHeader, TableHeaderCell, TableRow
} from "semantic-ui-react";
import {
	fieldToAllowedRequestLocationsSelect,
	fieldsToSelect,
	servicesToSelect,
	sortPropertyToValue,
	sortRequestToUpdateAction,
	viewRequestToFieldSelect,
	viewRequestFieldSelectionToField,
	viewResponseFieldSelectionToField
} from "../../mediator";
import {
	fieldReferenceToInputControl
} from "../../factory";
import {
	PigComponent
} from "../../common/component";
import {
	validateValue,
	PigInput,
	PigSearch,
	ValueMediator
} from "../../common/controls";
import {
	validateModelViewPath,
	validateTimeout,
	validateRequestFieldVariableName
} from "../../../model/validate";


class ViewEditorComponent extends PigComponent {
	render() {
		if(this.props.view) {
			return (
				<div className="project-models-view-editor pig-row">
					{this._renderFormEditor()}
					<div key={`${this.props.view.id}.c2`}>
						{this._renderRequestEditor()}
						{this._renderResponseEditor()}
					</div>
				</div>
			);
		}
		return null;
	}

	_renderFormEditor() {
		const {gamuts, model, project, view}=this.props,
			onUpdate=_.partial(this.props.onUpdateView, model);
		return (
			// this guy is a real pain in the @ss. He tenaciously caches the contents of this form.
			<Form key={view.id}
				className="project-models-view-form"
			>
				<PigInput
					key="desc"
					elementType={FormTextArea}
					placeholder="Don't just say no to documentation."
					rows={3}
					mediator={new ValueMediator({
						onCommit: onUpdate,
						sourceObject: view,
						sourceField: "desc",
						title: "Description"
					})}
				/>
				<PigInput
					key="path"
					elementType={FormInput}
					mediator={new ValueMediator({
						isValid: _.partial(validateModelViewPath, model, view),
						onCommit: onUpdate,
						sourceObject: view,
						sourceField: "path",
						title: "Path"
					})}
				/>
				<PigSearch
					key="function"
					elementType={FormDropdown}
					mediator={new ValueMediator({
						onCommit: onUpdate,
						sourceObject: view,
						sourceField: "function",
						sourceValues: gamuts.model.view.functions,
						title: "Function"
					})}
				/>
				<PigSearch
					key="service"
					elementType={FormDropdown}
					placeholder="none"
					mediator={new ValueMediator({
						onCommit: onUpdate,
						sourceObject: view,
						sourceField: "serviceId",
						sourceValues: servicesToSelect(project),
						title: "Service"
					})}
				/>
				<PigSearch
					key="method"
					elementType={FormDropdown}
					mediator={new ValueMediator({
						onCommit: onUpdate,
						sourceObject: view,
						sourceField: "transport.method",
						sourceValues: _.find(gamuts.transport.types, {value: view.transport.protocol}).methods,
						title: "Method"
					})}
				/>
				<PigSearch
					key="protocol"
					elementType={FormDropdown}
					mediator={new ValueMediator({
						onCommit: onUpdate,
						sourceObject: view,
						sourceField: "transport.protocol",
						sourceValues: gamuts.transport.types,
						title: "Protocol"
					})}
				/>
				<PigInput
					key="timeout"
					elementType={FormInput}
					placeholder="Default"
					mediator={new ValueMediator({
						isValid: validateTimeout,
						onCommit: onUpdate,
						sourceObject: view,
						sourceField: "timeout",
						title: "Timeout"
					})}
				/>
			</Form>
		);
	}

	_renderRequestEditor() {
		let disabled;
		const {
				application, gamuts, model, view,
				onAddRequestField, onDeleteRequestField, onSortRequestFieldsByProperty, onSortRequestFieldsByName
			}=this.props,
			onUpdateRequestField=_.partial(this.props.onUpdateRequestField, model, view);
		return (
			<div key={`${view.id}.request`}
				className="project-models-view-request">
				<Header>Request</Header>
				<Table selectable sortable>
					<TableHeader key="header">
						<TableRow>
							<TableHeaderCell key="property"
								sorted={sortPropertyToValue({application, majorId: view.id, minorId: "request", property: "fieldId"})}
								onClick={_.partial(onSortRequestFieldsByName, application, gamuts, model, view)}
							>
								Property
							</TableHeaderCell>
							<TableHeaderCell key="variable"
								sorted={sortPropertyToValue({application, majorId: view.id, minorId: "request", property: "variable"})}
								onClick={_.partial(onSortRequestFieldsByProperty, application, model, view, "variable")}
							>
								Variable
							</TableHeaderCell>
							<TableHeaderCell key="location"
								sorted={sortPropertyToValue({application, majorId: view.id, minorId: "request", property: "location"})}
								onClick={_.partial(onSortRequestFieldsByProperty, application, model, view, "location")}
							>
								Location
							</TableHeaderCell>
							<TableHeaderCell key="encoding"
								sorted={sortPropertyToValue({application, majorId: view.id, minorId: "request", property: "encoding"})}
								onClick={_.partial(onSortRequestFieldsByProperty, application, model, view, "encoding")}
							>
								Encoding
							</TableHeaderCell>
							<TableHeaderCell key="operation"
								sorted={sortPropertyToValue({application, majorId: view.id, minorId: "request", property: "operation"})}
								onClick={_.partial(onSortRequestFieldsByProperty, application, model, view, "operation")}
							>
								Operator
							</TableHeaderCell>
							<TableHeaderCell key="value"
								sorted={sortPropertyToValue({application, majorId: view.id, minorId: "request", property: "defaultValue"})}
								onClick={_.partial(onSortRequestFieldsByProperty, application, model, view, "defaultValue")}
							>
								Default Value
							</TableHeaderCell>
							<TableHeaderCell key="action" style={{width: 65}}>
								<Button icon="add" onClick={()=>onAddRequestField(model, view)}/>
							</TableHeaderCell>
						</TableRow>
					</TableHeader>
					<TableBody key="body">
						{
							view.requestFields.map(requestField=>{
								// note: if we don't find him then we have integrity issues. We leave him here to be recognized and deleted.
								const referencedField=model_util.findField({
										gamuts, model,
										id: requestField.fieldId
									}),
									locations=(referencedField)
										? fieldToAllowedRequestLocationsSelect({
											field: referencedField,
											gamuts
										})
										: [];

								return <TableRow
									error={!referencedField}
									key={requestField.id}
								>
									<TableCell key="property">
										<PigSearch
											className="pig-input-average"
											mediator={new ValueMediator({
												onCommit: (field)=>onUpdateRequestField(viewRequestFieldSelectionToField({field, gamuts, model})),
												isValid: ()=>(referencedField)
													? null
													: new Error(`${requestField.fieldId} could not be found`),
												sourceObject: requestField,
												sourceField: "fieldId",
												sourceValues: viewRequestToFieldSelect({gamuts, model, view, exclude: requestField, filter: true})
											})}
										/>
									</TableCell>
									<TableCell key="variable">
										<PigInput
											className="pig-input-trim"
											disabled={(disabled=_.includes([
												constant.project.request.param.location.BODY,
												constant.project.request.param.location.LITERAL
											], requestField.location))}
											mediator={new ValueMediator({
												isValid: _.partial(validateRequestFieldVariableName, view.requestFields, requestField),
												onCommit: onUpdateRequestField,
												sourceObject: requestField,
												sourceField: "variable",
												title: disabled
													? null
													: model_util.fieldIdToVariableName({
														fieldId: requestField.fieldId,
														gamuts,
														model
													}),
												titleType: "placeholder"
											})}
										/>
									</TableCell>
									<TableCell key="location">
										<PigSearch
											className="pig-input-skinny"
											mediator={new ValueMediator({
												isValid: (value)=>_.find(locations, {value})
													? null
													: new Error(`allowed values: ${_.map(locations, "text").join(", ")}`),
												onCommit: onUpdateRequestField,
												sourceObject: requestField,
												sourceField: "location",
												sourceValues: locations
											})}
										/>
									</TableCell>
									<TableCell key="encoding">
										<PigSearch
											className="pig-flex-trim"
											disabled={(disabled=requestField.location===constant.project.request.param.location.BODY)}
											mediator={new ValueMediator({
												onCommit: onUpdateRequestField,
												sourceObject: (disabled) ? {encoding: "none"} : requestField,
												sourceField: "encoding",
												sourceValues: gamuts.request.param.encoding
											})}
										/>
									</TableCell>
									<TableCell key="operation">
										<PigSearch
											className="pig-input-anorexic"
											disabled={requestField.propertyOf===constant.project.field.propertyOf.QUERY}
											mediator={new ValueMediator({
												onCommit: onUpdateRequestField,
												sourceObject: requestField,
												sourceField: "operation",
												sourceValues: gamuts.model.operators
											})}
										/>
									</TableCell>
									<TableCell key="value">
										{
											fieldReferenceToInputControl({
												defaultClassName: "pig-input-trim",
												fieldReference: requestField,
												gamuts,
												model,
												onCommit: onUpdateRequestField,
												sourceField: "defaultValue",
												sourceObject: requestField
											})
										}
									</TableCell>
									<TableCell key="action">
										<Button icon="delete" onClick={()=>onDeleteRequestField(model, view, requestField)}/>
									</TableCell>
								</TableRow>;
							})
						}
					</TableBody>
				</Table>
			</div>
		);
	}

	_renderResponseEditor() {
		const {
				application, gamuts, model, view,
				onAddResponseField, onDeleteResponseField, onSortResponseFieldsByProperty
			}=this.props,
			onUpdateResponseField=_.partial(this.props.onUpdateResponseField, model, view);
		return (
			<div key={`${view.id}.response`}
				className="project-models-view-response">
				<Header>Response</Header>
				<Table collapsing selectable sortable>
					<TableHeader key="header">
						<TableRow>
							<TableHeaderCell key="property"
								sorted={sortPropertyToValue({application, majorId: view.id, minorId: "response", property: "fieldId"})}
								onClick={_.partial(onSortResponseFieldsByProperty, application, model, view, "fieldId")}
							>
								Property
							</TableHeaderCell>
							<TableHeaderCell key="type"
								sorted={sortPropertyToValue({application, majorId: view.id, minorId: "response", property: "type"})}
								onClick={_.partial(onSortResponseFieldsByProperty, application, model, view, "type")}
							>
								Type
							</TableHeaderCell>
							<TableHeaderCell key="subtype"
								sorted={sortPropertyToValue({application, majorId: view.id, minorId: "response", property: "subtype"})}
								onClick={_.partial(onSortResponseFieldsByProperty, application, model, view, "subtype")}
							>
								Subtype
							</TableHeaderCell>
							<TableHeaderCell key="value"
								sorted={sortPropertyToValue({application, majorId: view.id, minorId: "response", property: "defaultValue"})}
								onClick={_.partial(onSortResponseFieldsByProperty, application, model, view, "defaultValue")}
							>
								Default Value
							</TableHeaderCell>
							<TableHeaderCell key="action" style={{width: 65}}>
								<Button
									disabled={!view.responseConfigurable}
									icon="add"
									onClick={()=>onAddResponseField(model, view)}
								/>
							</TableHeaderCell>
						</TableRow>
					</TableHeader>
					<TableBody
						key="body"
					>
						{
							view.responseFields.map(responseField=>{
								let field, fields;
								const configurable=Boolean(view.responseConfigurable);
								// there are two types of responses (at the moment):
								// - configurable (default): this is business as usual and used by all functions with configurable responses
								// - non-configurable: these guys have fixed response schemas. They are "count" and "delete"
								if(configurable) {
									field=model_util.findField({model, id: responseField.fieldId});
									fields=fieldsToSelect({gamuts, model, modelFields: model.fields});
								} else {
									field=model_util.findField({gamuts, id: responseField.fieldId});
									fields=fieldsToSelect({gamuts, model, resultFields: gamuts.resultFields});
								}
								return <TableRow
									disabled={!configurable}
									key={responseField.id}
								>
									<TableCell key="property">
										<PigSearch
											className="pig-input-average"
											mediator={new ValueMediator({
												onCommit: (field)=>onUpdateResponseField(viewResponseFieldSelectionToField({field, gamuts, model})),
												sourceObject: responseField,
												sourceField: "fieldId",
												sourceValues: fields
											})}
										/>
									</TableCell>
									<TableCell>
										<PigInput
											className="pig-input-skinny"
											readOnly={true}
											mediator={new ValueMediator({
												sourceObject: field,
												sourceField: "type"
											})}
										/>
									</TableCell>
									<TableCell>
										<PigInput
											className="pig-input-skinny"
											placeholder="none"
											readOnly={true}
											mediator={new ValueMediator({
												sourceObject: field.type===constant.project.field.type.ARRAY ? field : {},
												sourceField: "subtype"
											})}
										/>
									</TableCell>
									<TableCell key="value">
										<PigInput
											className="pig-input-trim"
											placeholder="none"
											mediator={new ValueMediator({
												onCommit: onUpdateResponseField,
												sourceObject: responseField,
												sourceField: "defaultValue"
											})}
										/>
									</TableCell>
									<TableCell key="action">
										<Button icon="delete" onClick={()=>onDeleteResponseField(model, view, responseField)}/>
									</TableCell>
								</TableRow>;
							})
						}
					</TableBody>
				</Table>
			</div>
		);
	}
}

ViewEditorComponent.propTypes={
	application: propTypes.object,
	gamuts: propTypes.object,
	model: propTypes.object,
	project: propTypes.object,
	view: propTypes.object,
	onUpdateView: propTypes.func,
	onAddRequestField: propTypes.func,
	onDeleteRequestField: propTypes.func,
	onUpdateRequestField: propTypes.func,
	onSortRequestFieldsByProperty: propTypes.func,
	onAddResponseField: propTypes.func,
	onDeleteResponseField: propTypes.func,
	onUpdateResponseField: propTypes.func,
	onSortResponseFieldsByProperty: propTypes.func
};

const mapStateToProps=(state)=>{
	return {
		application: state.application,
		gamuts: state.gamuts,
		project: state.projects.selected
	};
};

const mapDispatchToProps=(dispatch)=>{
	return {
		onUpdateView: (model, view)=>dispatch(actions.project.updateDataView(model, view)),
		onAddRequestField: (model, view)=>dispatch(actions.project.addDataViewRequestField(model, view)),
		onDeleteRequestField: (model, view, field)=>dispatch(actions.project.deleteDataViewRequestField(model, view, field)),
		onUpdateRequestField: (model, view, field)=>dispatch(actions.project.updateDataViewRequestField(model, view, field)),
		onSortRequestFieldsByProperty: (application, model, view, property)=>{
			const action=sortRequestToUpdateAction({application, majorId: view.id, minorId: "request", property});
			dispatch(actions.project.sortDataViewRequestFields({
				model, view,
				property: action.value.property,
				reverse: action.value.reverse
			}));
			dispatch(actions.application.setSessionSetting(action));
		},
		onSortRequestFieldsByName: (application, gamuts, model, view)=>{
			const action=sortRequestToUpdateAction({application, majorId: view.id, minorId: "request", property: "fieldId"}),
				fieldMap=_.keyBy(viewRequestToFieldSelect({gamuts, model, view}), "value"),
				sorted=view.requestFields
					.slice()
					.sort((r1, r2)=>{
						const result=model_util.fieldNameCompare(_.get(fieldMap[r1.fieldId], "text"), _.get(fieldMap[r2.fieldId], "text"));
						return action.value.reverse ? 0-result : result;
					});
			dispatch(actions.project.sortDataViewRequestFields({model, view, sorted}));
			dispatch(actions.application.setSessionSetting(action));
		},
		onAddResponseField: (model, view)=>dispatch(actions.project.addDataViewResponseField(model, view)),
		onDeleteResponseField: (model, view, field)=>dispatch(actions.project.deleteDataViewResponseField(model, view, field)),
		onUpdateResponseField: (model, view, field)=>dispatch(actions.project.updateDataViewResponseField(model, view, field)),
		onSortResponseFieldsByProperty: (application, model, view, property)=>{
			const action=sortRequestToUpdateAction({application, majorId: view.id, minorId: "response", property});
			dispatch(actions.project.sortDataViewResponseFields(model, view, action.value.property, action.value.reverse));
			dispatch(actions.application.setSessionSetting(action));
		}
	};
};

module.exports.ViewEditor=connect(
	mapStateToProps,
	mapDispatchToProps
)(ViewEditorComponent);
