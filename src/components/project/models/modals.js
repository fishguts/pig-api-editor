/**
 * User: curtis
 * Date: 7/17/18
 * Time: 10:35 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const React=require("react");
const propTypes=require("prop-types");
import {
	Dropdown,
	Modal,
	Button
} from "semantic-ui-react";
import {
	servicesToSelect
} from "../../mediator";


class ViewSelectorModal extends React.Component {
	/**
	 * Presents select view modal
	 * @param {GamutsState} gamuts
	 * @param {Project} project
	 * @param {Function} onSelect - returns data path within <link>res/configuration/model-defaults.yaml</link>
	 * @param {Function} onClose - called when dialog is closed in one way or another
	 * @constructor
	 */
	constructor({gamuts, project, onClose, onSelect}) {
		super(arguments[0]);
		this._selection="dataModelView.create-one";
		this._services=servicesToSelect(project);
		this._serviceId=_.get(this._services, "1.value");
	}

	render() {
		const {gamuts, onClose, onSelect}=this.props;
		return (
			<Modal size="tiny" open={true} onClose={onClose}>
				<Modal.Header>Select View Type</Modal.Header>
				<Modal.Content>
					<Dropdown
						defaultValue={this._selection}
						options={gamuts.model.view.functions.map(item=>{
							return {
								text: item.text,
								value: `dataModelView.${item.value}`
							};
						})}
						onChange={(event, data)=>this._selection=data.value}
					/>
					<br/>
					{
						(this._services.length>1)
							? <Dropdown
								defaultValue={this._serviceId}
								options={this._services}
								onChange={(event, data)=>this._serviceId=data.value}
							/>
							: null
					}
				</Modal.Content>
				<Modal.Actions>
					<Button
						content="Select"
						positive
						onClick={()=>onSelect({
							path: this._selection,
							serviceId: this._serviceId
						})}
					/>
					<Button
						content="Cancel"
						negative
						onClick={onClose}
					/>
				</Modal.Actions>
			</Modal>
		);
	}
}

ViewSelectorModal.propTypes={
	gamuts: propTypes.object.isRequired,
	project: propTypes.object.isRequired,
	onClose: propTypes.func.isRequired,
	onSelect: propTypes.func.isRequired
};

exports.ViewSelector=ViewSelectorModal;
