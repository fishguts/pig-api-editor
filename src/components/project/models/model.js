/**
 * User: curtis
 * Date: 5/14/18
 * Time: 9:48 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const propTypes=require("prop-types");
const constant=require("../../../common/constant");
const actions=require("../../../actions/index");
const model_factory=require("../../../model/factory");

import React from "react";
import {connect} from "react-redux";
import {
	Button,
	Table, TableBody, TableCell, TableHeader, TableHeaderCell, TableRow
} from "semantic-ui-react";
import {PigComponent} from "../../common/component";
import {
	validateValue,
	PigInput,
	PigSearch,
	PigTagEditor,
	ValueMediator
} from "../../common/controls";
import {
	fieldToSubtypes,
	sortPropertyToValue,
	sortRequestToUpdateAction
} from "../../mediator";
import {
	getProjectDependencyGraph
} from "../../../model/dependents";
import {
	validateModelFieldName
} from "../../../model/validate";


/**
 * Single model editor
 */
class ModelEditorComponent extends PigComponent {
	/**
	 * @param {DataModel} model
	 * @param {DataField} field
	 * @private
	 */
	_onDeleteField(model, field) {
		const graph=getProjectDependencyGraph(this.props.project),
			dependents=graph.fieldToFieldReferences({field, model});
		if(dependents.length===0) {
			this.props.onDeleteField(model, field);
		} else {
			this.props.onAddActivity(model_factory.createActivity({
				details: _.map(dependents, "path"),
				message: `Cannot delete due to dependencies on field "${model.name}.${field.name}"`,
				severity: constant.severity.ERROR
			}));
		}
	}

	/**
	 * Render this beast
	 * @returns {ReactElement}
	 */
	render() {
		const {
			application, gamuts, model,
			onAddField, onSortFields
		}=this.props;
		if(model) {
			return (
				<div key={model.id}
					className="project-models-model-editor">
					<Table celled selectable sortable>
						<TableHeader>
							<TableRow key="header">
								<TableHeaderCell key="name"
									sorted={sortPropertyToValue({application, majorId: model.id, minorId: "fields", property: "name"})}
									onClick={_.partial(onSortFields, application, model, "name")}
								>
									Name
								</TableHeaderCell>
								<TableHeaderCell key="type"
									sorted={sortPropertyToValue({application, majorId: model.id, minorId: "fields", property: "type"})}
									onClick={_.partial(onSortFields, application, model, "type")}
								>
									Type
								</TableHeaderCell>
								<TableHeaderCell key="subtype"
									sorted={sortPropertyToValue({application, majorId: model.id, minorId: "fields", property: "subtype"})}
									onClick={_.partial(onSortFields, application, model, "subtype")}
								>
									Subtype
								</TableHeaderCell>
								<TableHeaderCell key="desc"
									sorted={sortPropertyToValue({application, majorId: model.id, minorId: "fields", property: "desc"})}
									onClick={_.partial(onSortFields, application, model, "desc")}
								>
									Description
								</TableHeaderCell>
								<TableHeaderCell key="regex"
									sorted={sortPropertyToValue({application, majorId: model.id, minorId: "fields", property: "regex"})}
									onClick={_.partial(onSortFields, application, model, "allowed.regex")}
								>
									Pattern
								</TableHeaderCell>
								<TableHeaderCell key="values"
									sorted={sortPropertyToValue({application, majorId: model.id, minorId: "fields", property: "values"})}
									onClick={_.partial(onSortFields, application, model, "allowed.values")}
								>
									Values
								</TableHeaderCell>
								<TableHeaderCell key="attributes">
									Attributes
								</TableHeaderCell>
								<TableHeaderCell key="action">
									<Button icon="add" onClick={()=>onAddField(model)}/>
								</TableHeaderCell>
							</TableRow>
						</TableHeader>
						<TableBody>
							{
								model.fields.map((field)=>{
									const isEnableTypeModifier=(type)=>field.type===type || (field.type===constant.project.field.type.ARRAY && field.subtype===type),
										enumEnabled=isEnableTypeModifier(constant.project.field.type.ENUM),
										patternEnabled=isEnableTypeModifier(constant.project.field.type.STRING),
										onUpdateField=_.partial(this.props.onUpdateField, model, field);
									return <TableRow
										key={field.id}
										disabled={_.includes(field.attributes, "read-only")}
									>
										<TableCell key="name">
											<PigInput
												className="pig-input-trim"
												mediator={new ValueMediator({
													isValid: _.partial(validateModelFieldName, model, field),
													onCommit: onUpdateField,
													sourceObject: field,
													sourceField: "name",
													title: "name",
													titleType: "placeholder"
												})}
											/>
										</TableCell>
										<TableCell key="type">
											<PigSearch
												className="pig-input-skinny"
												mediator={new ValueMediator({
													onCommit: onUpdateField,
													sourceObject: field,
													sourceField: "type",
													sourceValues: gamuts.model.field.types
												})}
											/>
										</TableCell>
										<TableCell key="subtype">
											<PigSearch
												className="pig-input-skinny"
												disabled={field.type!=="array"}
												mediator={new ValueMediator({
													onCommit: onUpdateField,
													sourceObject: field,
													sourceField: "subtype",
													sourceValues: fieldToSubtypes({field, gamuts})
												})}
											/>
										</TableCell>
										<TableCell key="desc">
											<PigInput
												className="pig-input-fat"
												mediator={new ValueMediator({
													onCommit: onUpdateField,
													sourceObject: field,
													sourceField: "desc",
													title: "description",
													titleType: "placeholder"
												})}
											/>
										</TableCell>
										<TableCell key="regex">
											<PigInput
												className="pig-input-average"
												disabled={!patternEnabled}
												mediator={new ValueMediator({
													isValid: validateValue.regex,
													onCommit: onUpdateField,
													sourceObject: patternEnabled ? field : null,
													sourceField: "allowed.regex",
													title: "none",
													titleType: "placeholder"
												})}
											/>
										</TableCell>
										<TableCell key="values">
											<PigTagEditor
												className="pig-flex-average"
												disabled={!enumEnabled}
												mediator={new ValueMediator({
													onCommit: onUpdateField,
													sourceObject: enumEnabled ? field : null,
													sourceField: "allowed.values",
													title: "allowed",
													titleType: "placeholder"
												})}
											/>
										</TableCell>
										<TableCell key="attributes">
											<PigSearch
												className="pig-flex-average"
												multiple
												placeholder="attributes"
												mediator={new ValueMediator({
													onCommit: onUpdateField,
													sourceObject: field,
													sourceField: "attributes",
													sourceValues: gamuts.model.field.attributes
												})}
											/>
										</TableCell>
										<TableCell key="action">
											<Button icon="delete" onClick={()=>this._onDeleteField(model, field)}/>
										</TableCell>
									</TableRow>;
								})
							}
						</TableBody>
					</Table>
				</div>
			);
		}
		return null;
	}
}

ModelEditorComponent.propTypes={
	application: propTypes.object,
	gamuts: propTypes.object,
	model: propTypes.object,
	project: propTypes.object,
	onAddActivity: propTypes.func,
	onAddField: propTypes.func,
	onDeleteField: propTypes.func,
	onUpdateField: propTypes.func,
	onSortFields: propTypes.func
};

const mapStateToProps=(state)=> {
	return {
		application: state.application,
		gamuts: state.gamuts,
		project: state.projects.selected
	};
};

const mapDispatchToProps=(dispatch)=> {
	return {
		onAddActivity: (activity)=>dispatch(actions.activity.add(activity)),
		onAddField: (model, field)=>dispatch(actions.project.addDataField(model, field)),
		onDeleteField: (model, field)=>dispatch(actions.project.deleteDataField(model, field)),
		onUpdateField: (model, fieldOld, fieldNew)=>dispatch(actions.project.updateDataField(model, fieldOld, fieldNew)),
		onSortFields: (application, model, property)=>{
			const action=sortRequestToUpdateAction({application, majorId: model.id, minorId: "fields", property});
			dispatch(actions.project.sortDataFields(model, action.value.property, action.value.reverse));
			dispatch(actions.application.setSessionSetting(action));
		}
	};
};

module.exports.ModelEditor=connect(
	mapStateToProps,
	mapDispatchToProps
)(ModelEditorComponent);

