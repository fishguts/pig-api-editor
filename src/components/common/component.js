/**
 * User: curtis
 * Date: 3/17/18
 * Time: 12:50 PM
 * Copyright @2018 by Xraymen Inc.
 */

import React from "react";

/**
 * @typedef {React.Component} PigComponent
 * @abstract
 */
class PigComponent extends React.Component {
	/**
	 * @param {Object} properties
	 * @param {Object} state
	 */
	constructor(properties, state={}) {
		super(properties);
		this.state=state;
		this._listeners=[];
	}

	/**** Pig API ****/
	/**
	 * Adds a listener that will get removed upon un-mount
	 * @param {HTMLElement} element
	 * @param {Function} listener
	 * @param {Object} options
	 * @param {string} type
	 */
	addEventListener({element=window, listener, options=undefined, type}) {
		element.addEventListener(type, listener, options);
		this._listeners.push(()=>element.removeEventListener(type, listener));
	}

	/**** React Lifecycle ****/
	componentWillUnmount() {
		this._listeners.forEach((listener)=>listener());
	}

	/**
	 * Doesn't do anything
	 * @returns {null}
	 */
	render() {
		return null;
	}
}

module.exports.PigComponent=PigComponent;
