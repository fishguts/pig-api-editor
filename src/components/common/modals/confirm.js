/**
 * User: curtis
 * Date: 7/17/18
 * Time: 10:35 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const React=require("react");
const propTypes=require("prop-types");
const {
	Button,
	Modal
}=require("semantic-ui-react");

/**
 * Confirm something with Yes or No
 * @param {string} heading
 * @param {Boolean} open - whether his state should be open or not
 * @param {string} text
 * @param {Function} onNo
 * @param {Function} onYes
 * @returns {ReactElement}
 * @constructor
 */
function ConfirmModal({
	heading,
	open=true,
	text,
	onYes,
	onNo=_.noop
}) {
	if(!open) {
		return null;
	} else {
		return (
			<Modal size="tiny" open={true}>
				<Modal.Header>{heading}</Modal.Header>
				<Modal.Content>
					<p>{text}</p>
				</Modal.Content>
				<Modal.Actions>
					<Button
						content="Yes"
						positive
						onClick={onYes}
					/>
					<Button
						content="No"
						negative
						onClick={onNo}
					/>
				</Modal.Actions>
			</Modal>
		);
	}
}

ConfirmModal.propTypes={
	heading: propTypes.string.isRequired,
	open: propTypes.bool,
	text: propTypes.string.isRequired,
	onNo: propTypes.func,
	onYes: propTypes.func.isRequired
};

module.exports.ConfirmModal=ConfirmModal;
