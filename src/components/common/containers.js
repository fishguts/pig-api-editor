/**
 * User: curtis
 * Date: 5/6/18
 * Time: 10:37 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const propTypes=require("prop-types");
const shortid=require("shortid");

import React from "react";

/* eslint-disable react/prop-types */

/**
 * He's a pretty dumb beast that makes sure floating doesn't spoil the component height.
 * To make sure the height is always correct he adds a 0x0 element at the end and clears "both"
 */
class ToolBarComponent extends React.Component {
	render() {
		const inner=(_.isArray(this.props.children) ? this.props.children : [this.props.children])
			.concat(<div key={shortid.generate()} style={{clear: "both"}}/>)
			.map((element)=> {
				// We are turning these guys into an array of children. If they don't have a unique <code>key</code> property
				// then react will complain in our console.
				if(element.key==null) {
					element=Object.assign({}, element);
					element.key=shortid.generate();
				}
				return element;
			});
		return React.createElement(this.props.elementType||"div", this.props, inner);
	}
}

ToolBarComponent.propTypes={
	elementType: propTypes.string
};

module.exports.ToolBar=ToolBarComponent;
