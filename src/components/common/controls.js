/**
 * User: curtis
 * Date: 3/29/18
 * Time: 10:10 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const assert=require("pig-core").assert;
const util=require("pig-core").util;
import React from "react";
import {Dropdown, Input, Popup} from "semantic-ui-react";

/* eslint-disable no-use-before-define */
/* eslint-disable react/prop-types */

/**
 * This guy mediates between a single value between a control and dispatch
 */
class ValueMediator {
	/**
	 * @param {function(*, *)} compare - compare updates to old value
	 * @param {function(*)} format - optional function to format the value for presentation.
	 *   The default tries to be smart and not default to parsing if there is are sourceValues
	 * @param {function(value:string)} parse - optional function to convert presentation value to internal value
	 *   The default tries to be smart and not default to parsing if there is are sourceValues
	 * @param {PigValidationFunction} isValid - optional function to validate the initial and dynamically updated value
	 * @param {function(Object)} onCommit - called back with clone of sourceObject with updated field
	 * @param {*} sourceDefault - default value. <code>format</code> will be applied to this guy
	 * @param {string} sourceField - property path of the value in <param>sourceObject</param>
	 * @param {Object} sourceObject - object that is the source of data and the object that will be updated
	 * @param {[*]} sourceValues - values to select from. format will be applied to these for presentation.
	 * @param {Boolean} usePlaceholder - whether control should use title as a label or placeholder
	 * @param {string|undefined} title - title of the control
	 * @param {"label"|"placeholder"|"none"} titleType - how to present the title
	 */
	constructor({
		compare=compareValue.loose,
		format=(arguments[0].sourceValues===undefined) ? formatValue.toString : formatValue.identity,
		parse=(arguments[0].sourceValues===undefined) ? parseValue.toString : parseValue.identity,
		isValid=null,
		onCommit,
		sourceField,
		sourceObject,
		sourceDefault=undefined,
		sourceValues=undefined,
		title=undefined,
		titleType="label"
	}) {
		this.compare=compare,
		this.isValid=isValid;
		this.format=format;
		this.parse=parse;
		this.title=title;
		this.titleType=titleType;
		this.sourceField=sourceField;
		this.sourceObject=sourceObject;
		this.sourceValues=sourceValues;
		this.onCommit=onCommit;
		this._parsedValue=_.get(this.sourceObject, this.sourceField, sourceDefault);
	}

	/**
	 * Gets the current error state
	 * @returns {null|Error}
	 */
	get error() {
		return this.isValid
			? this.isValid(this._parsedValue)
			: null;
	}

	/**
	 * This is the <code>format</code> value is either what gets presented to a user (as in an input control)
	 * Or is the value used to select the currently selected item (as in a dropdown)
	 * @returns {*}
	 */
	get presentationValue() {
		return _.isArray(this._parsedValue)
			? this._parsedValue.map((value)=>this.format(value))
			: this.format(this._parsedValue);
	}
	set presentationValue(value) {
		this._parsedValue=_.isArray(value)
			? value.map((_value)=>this.parse(_value))
			: this.parse(value);
	}

	/**
	 * This is the native, <code>parse</code> value.
	 * It updates the value locally. Does not commit it. See <code>commit</code>
	 * @returns {*}
	 */
	get parsedValue() {return this._parsedValue;}
	set parsedValue(value) {this._parsedValue=value;}

	/**
	 * Commits the current state of value to the sourceObject.
	 */
	commit() {
		if(!this.compare(this._parsedValue, _.get(this.sourceObject, this.sourceField))) {
			let clone;
			if(_.isFunction(this.sourceObject.clone)) {
				// we don't need to clone the last element 'cause that's the fellow we are here to replace
				const path=_(this.sourceField)
					.split(".")
					.initial()
					.join(".");
				clone=this.sourceObject.clone({path});
			} else {
				clone=_.clone(this.sourceObject);
			}
			_.set(clone, this.sourceField, this._parsedValue);
			this.onCommit(clone);
		}
	}
}

/**
 * @typedef {Object} ValueTranslatorRule
 * @property {*} control
 * @property {*} mediator
 * @property {boolean|undefined} reciprocal - whether conversion back to mediator may be assumed. Defaults to true.
 */

/**
 * Translates values between the mediator state and the control state. Handy for converting undefined and nulls
 * to and from other values that behave better in controlled components. Otherwise we get warnings from
 * react about having both controlled and uncontrolled behavior...which we don't want. We want controlled.
 */
class ValueTranslator {
	/**
	 * @param {Array<ValueTranslatorRule>} list
	 */
	constructor(list) {
		this._list=list
	}

	/**
	 * @param {*} value
	 * @return {*}
	 */
	toControl(value) {
		for(let index=this._list.length-1; index>-1; index--) {
			const rule=this._list[index];
			if(rule.mediator===value) {
				return rule.control;
			}
		}
		return value;
	}

	/**
	 * @param {*} value
	 * @return {*}
	 */
	toMediator(value) {
		for(let index=this._list.length-1; index>-1; index--) {
			const rule=this._list[index];
			if(rule.control===value && (rule.reciprocal||rule.reciprocal===undefined)) {
				return rule.mediator;
			}
		}
		return value;
	}
}

/**
 * Renders a text field. We default <code>fullWidth</code> to true since we more often than
 * not plan to use them in columns.  But you may override it.
 * @typedef {React.Component} PigInputControl
 */
class PigInputControl extends React.Component {
	/**
	 * @param {string} className
	 * @param {string|Function} elementType
	 * @param {ValueMediator} mediator
	 * @param {ValueTranslator} translator - translate values to and from the control space
	 * @param {Object} properties - additional optional properties
	 */
	constructor({
		className=PigInputControl.defaultProps.className,
		elementType=PigInputControl.defaultProps.elementType,
		mediator,
		translator=PigInputControl.defaultProps.translator,
		...properties
	}) {
		super(arguments[0]);
		this.state={
			error: mediator.error,
			hover: false,
			value: translator.toControl(mediator.presentationValue)
		};
	}

	static defaultProps={
		className: "pig-input",
		elementType: Input,
		translator: new ValueTranslator([
			{control: "", mediator: undefined, reciprocal: true}
		])
	};

	/**
	 * Align our state and properties cause our properties may have been updated by changes to our model. Strange that this happens
	 * after update? I think so but they are deprecating the oldies such as componentWillUpdateProps which would have been the perfect place.
	 * @param {{mediator: ValueMediator}} prevProps
	 * @param {Object} prevState
	 * @param {Object} prevContext
	 */
	componentDidUpdate(prevProps, prevState, prevContext) {
		const mediator=this.props.mediator,
			prevMediator=prevProps.mediator,
			translator=this.props.translator;
		if(!_.isEqual(mediator.presentationValue, prevMediator.presentationValue)) {
			this.setState({
				error: mediator.error,
				value: translator.toControl(mediator.presentationValue)
			});
		}
	}

	/**
	 * Draw the beast
	 */
	render() {
		const {mediator, className, elementType, translator, ...properties}=this.props;
		const control=React.createElement(elementType, {
			className,
			value: this.state.value,
			error: Boolean(this.state.error),
			label: (mediator.titleType==="label") ? mediator.title : undefined,
			placeholder: (mediator.titleType==="placeholder") ? mediator.title : undefined,
			// This guy is causing us a bit some grief. When the commit happens then the app is re-rendered and it
			// seems that button clicks are being re-rendered between the refresh and not being captured. So, if you are
			// experiencing this problem then use onMouseDown.
			onBlur: ()=>{
				mediator.commit()
			},
			onChange: (event, data)=> {
				mediator.presentationValue=translator.toMediator(data.value);
				this.setState({
					error: mediator.error,
					value: data.value
				});
			},
			onKeyPress: (event)=> {
				if(event.key==="Enter") {
					event.defaultPrevented=true;
					mediator.commit();
				}
			},
			...properties
		});
		return <Popup
			content={_.get(this.state.error, "message")}
			header="Validation Failed"
			on="hover"
			onOpen={()=>this.setState({hover: true})}
			onClose={()=>this.setState({hover: false})}
			open={this.state.hover && this.state.error}
			trigger={control}
		/>;
	}
}

/**
 * A dropdown search control
 * @param {ValueMediator} mediator
 * @param {string} className
 * @param {string|Function} elementType
 * @param {Boolean} scrolling
 * @param {Object} properties - additional optional properties
 * @returns {ReactElement}
 * @constructor
 */
const PigSearchControl=({mediator, className="pig-input", elementType=Dropdown, scrolling=false, ...properties})=> {
	const error=mediator.error;
	properties={
		className,
		error: Boolean(error),
		label: (mediator.titleType==="label") ? mediator.title : undefined,
		// react grumbles if options is not defined
		options: mediator.sourceValues||null,
		placeholder: (mediator.titleType==="placeholder") ? mediator.title : undefined,
		scrolling,
		selection: true,
		// note: we only are called back on <code>onChange</code> when the selection has changed
		onChange: (event, data)=> {
			mediator.presentationValue=data.value;
			mediator.commit();
		},
		...properties
	};
	// semantic complains if we explicitly specify a value of undefined (which means we want the placeholder to be rendered).
	if(mediator.presentationValue!=null) {
		properties.value=mediator.presentationValue;
	}
	const control=React.createElement(elementType, properties);
	return (error)
		? <Popup
			content={error.message}
			header="Validation Failed"
			trigger={control}
		/>
		: control;
};


/**
 * It's an input editor that allows one to create and remove "tags" which are just user created strings.
 * @param {ValueMediator} mediator
 * @param {string} className
 * @param {string|Function} elementType
 * @param {Object} properties - additional optional properties
 * @returns {ReactElement}
 * @constructor
 */
const PigTagEditorControl=({mediator, className="pig-input", elementType=Dropdown, ...properties})=> {
	const error=mediator.error;
	properties={
		allowAdditions: true,
		className,
		error: Boolean(error),
		label: (mediator.titleType==="label") ? mediator.title : undefined,
		multiple: true,
		options: _.map(mediator.presentationValue, (value)=>({text: value, value: value})),
		placeholder: (mediator.titleType==="placeholder") ? mediator.title : undefined,
		search: true,
		selection: true,
		// note: we only are called back on <code>onChange</code> when the selection has changed
		onChange: (event, data)=> {
			mediator.presentationValue=data.value;
			mediator.commit();
		},
		...properties
	};
	// semantic complains if we explicitly specify a value of undefined (which means we want the placeholder to be rendered).
	if(mediator.presentationValue!=null) {
		properties.value=mediator.presentationValue;
	}
	const control=React.createElement(elementType, properties);
	return (error)
		? <Popup
			content={error.message}
			header="Validation Failed"
			trigger={control}
		/>
		: control;
};


/**************** Common Control Utilities ****************/
/**
 * Pre-baked compare functions
 */
const compareValue={
	loose: (v1, v2)=>_.isArray(v1)
		? v1.length===_.size(v2) && _.every(v1, (value, index)=>value==v2[index])
		: v1==v2,
	strict: (v1, v2)=>_.isEqual(v1, v2)
};

/**
 * Pre-baked format functions
 */
const formatValue={
	identity: (value)=>value,
	toString: (value)=>(value==null)
		? value
		: value.toString()
};

/**
 * Pre-baked value parse functions
 */
const parseValue={
	identity: (value)=>value,
	toString: (value)=>_.toString(value).trim(),
	/**
	 * Parsed to type
	 * @param {"number"|"string"} type
	 * @param {string} value
	 * @returns {*}
	 */
	toType: (type, value)=> {
		if(type==="number") {
			return parseFloat(value);
		} else {
			assert.toLog(type==="string");
			return parseValue.toString(value);
		}
	}
};

/**
 * Pre-baked validation functions
 */
const validateValue={
	/**
	 * @type {PigValidationFunction}
	 */
	empty: (value)=>{
		return (value==null || _.toString(value).trim().length===0)
			? null
			: new Error("should be empty")
	},
	/**
	 * @type {PigValidationFunction}
	 */
	notEmpty: (value)=>{
		return (value==null || _.toString(value).trim().length===0)
			? new Error("should not be empty")
			: null;
	},
	/**
	 * @type {PigValidationFunction}
	 */
	integer: (value)=>{
		const validator=validateValue.integerFactory();
		return validator(value);
	},
	/**
	 * This guy is a little different in that he builds a validation function
	 * @returns {PigValidationFunction}
	 */
	integerFactory: ({minLength=1, maxLength=""}={})=>{
		const regex=new RegExp(`^\\s*\\d{${minLength},${maxLength}}\\s*$`);
		return (value)=>{
			return regex.test((value||""))
				? null
				: new Error(`invalid integer: ${regex}`)
		};
	},
	/**
	 * @type {PigValidationFunction}
	 */
	number: (value)=>{
		return Number.isNaN(_.toNumber(value))
			? new Error("invalid number")
			: null;
	},
	/**
	 * @type {PigValidationFunction}
	 */
	regex: (value)=>{
		if(_.isEmpty(value)) {
			return null;
		}
		return util.try(()=>new RegExp(value), false)
			? null
			: new Error("invalid regular expression")
	},
	/**
	 * @type {PigValidationFunction}
	 */
	semver: (value)=>{
		const regex=/^\s*\d+\.\d+\.\d+\s*$/;
		return regex.test(value)
			? null
			: new Error(`invalid semantic version: ${regex}`);
	},
	/**
	 * @type {PigValidationFunction}
	 */
	url: (value)=>{
		try {
			// note: should always throw exception if it cannot be created.
			new URL(value);
			return null;
		} catch(error) {
			return new Error("invalid URL");
		}
	},
	/**
	 * @type {PigValidationFunction}
	 */
	urlFactory: ({allowEmpty=false})=>{
		return (value)=>{
			if(allowEmpty && (value==null || _.toString(value).trim().length===0)) {
				return null;
			}
			try {
				// note: should always throw exception if it cannot be created.
				new URL(value);
				return null;
			} catch(error) {
				return new Error("invalid URL");
			}
		};
	}
};


/**************** Exports ****************/
module.exports.compareValue=compareValue;
module.exports.formatValue=formatValue;
module.exports.parseValue=parseValue;
module.exports.validateValue=validateValue;

/**
 * Pre-baked source values collections
 */
module.exports.sourceValues={
	boolean: [
		{text: "false", value: false},
		{text: "true", value: true}
	]
};
module.exports.PigInput=PigInputControl;
module.exports.PigSearch=PigSearchControl;
module.exports.PigTagEditor=PigTagEditorControl;
module.exports.ValueMediator=ValueMediator;

