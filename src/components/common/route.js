/**
 * User: curtis
 * Date: 3/25/18
 * Time: 8:08 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const log=require("pig-core").log;
const propTypes=require("prop-types");
import {Route} from "react-router-dom";

/**
 * @typedef {Route} PigRoute
 */
class PigRoute extends Route {
	render() {
		// this appears to be the most reliable place to dispatch. And it appears to only be called when the match has changed.
		const match=this.state.match;
		log.debug(`PigRoute.render(${match.path}): ${JSON.stringify(_.omit(match, "path"))}`);
		if(this.props.onMatch) {
			// to avoid react's warning about updating during a transition
			setTimeout(()=>this.props.onMatch(this.state.match), 0);
		}
		return super.render();
	}
}

PigRoute.propTypes={
	/**
	 * @param {{path: string, url: string, isExact: Boolean, params: Object}}
	 */
	onMatch: propTypes.func
};

module.exports.Route=PigRoute;
