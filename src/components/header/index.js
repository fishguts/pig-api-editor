/**
 * User: curtis
 * Date: 3/17/18
 * Time: 12:49 PM
 * Copyright @2018 by Xraymen Inc.
 */

import React from "react";
import {connect} from "react-redux";
import {Button, Header} from "semantic-ui-react";
import {PigComponent} from "../common/component";
import actions from "../../actions";


/**
 * Application header
 */
class HeaderComponent extends PigComponent {
	render() {
		const {user, onLogin}=this.props;
		// todo: add the menu back

		return (
			<div className="pig-header">
				<div className="pig-left">
					<Header size="huge">Pig API Editor</Header>
				</div>
				<div className="pig-right">
					<Button onClick={onLogin.bind(null, user)}>{(user.loggedIn) ? "Logout" : "Login"}</Button>
				</div>
			</div>
		);
	}
}

const mapStateToProps=(state)=>({
	application: state.application,
	user: state.user
});

const mapDispatchToProps=(dispatch)=>({
	onLogin: (user)=>dispatch((user.loggedIn) ? actions.user.logout(user.loggedIn) : actions.user.login())
});

module.exports.Header=connect(
	mapStateToProps,
	mapDispatchToProps
)(HeaderComponent);
