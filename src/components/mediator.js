/**
 * User: curtis
 * Date: 5/23/18
 * Time: 5:48 PM
 * Copyright @2018 by Xraymen Inc.
 *
 * What is a mediator within components? It is a simple layer that translates values from our model
 * to our presentation layer and vice versa if necessary.
 */

const _=require("lodash");
const immutable=require("pig-core").mutation.immutable;
const constant=require("../common/constant");
const actions=require("../actions/index");
const {DataFieldReference}=require("../model/objects/index");
const model_util=require("../model/util");

/**
 * Build set of supported parameter locations that are supported by <param>field</param>.
 * Note: locations will be used by
 * @param {DataField} field
 * @param {GamutsState} gamuts
 * @returns {ControlItemList}
 */
function fieldToAllowedRequestLocationsSelect({field, gamuts}) {
	return (field.type===constant.project.field.type.OBJECT)
		? gamuts.request.param.location.filter((location)=>location.value==="body" || location.value==="header")
		: gamuts.request.param.location;
}

/**
 * Creates a list suitable for a selection control
 * @param {DataField} field
 * @param {Boolean} none - whether to include "None" in the mix.
 * @returns {ControlItemList}
 */
function fieldToAllowedValuesSelect(field, none=false) {
	const result=(field.allowed.values || []).map((value)=>({
		text: value,
		value: value
	}));
	if(none) {
		result.unshift({
			text: "none",
			value: undefined
		});
	}
	return result;
}

/**
 * Build a set of legitimate subtypes for the specified field
 * @param {DataField} field
 * @param {GamutsState} gamuts
 * @returns {[{text:string, value:string}]}
 */
function fieldToSubtypes({field, gamuts}) {
	if(field.type===constant.project.field.type.ARRAY) {
		return gamuts.model.field.types.filter((item)=>!_.includes(["array"], item.value));
	} else {
		return [];
	}
}

/**
 * Creates an array of fields for selection in a selectable control
 * @param {DataModel} model - we use him for finding references
 * @param {GamutsState} gamuts - we use him for finding references
 * @param {Array<DataField>} modelFields
 * @param {Array<DataFieldOperation>} queryFields
 * @param {Array<DataFieldReference>} resultFields
 * @returns {[{data: (DataField|DataFieldReference|DataFieldOperation), text:string, value:string}]}
 */
function fieldsToSelect({
	model, gamuts,
	modelFields=undefined,
	queryFields=undefined,
	resultFields=undefined
}) {
	function _adapt(fields) {
		return _.reduce(fields, (result, field)=>{
			// If <code>field</code> is a reference then we want to pull name info from it.
			const modelField=(field instanceof DataFieldReference)
				? model_util.findField({
					gamuts, model,
					id: field.fieldId
				})
				: field;
			// If we were unable to find the model field then we have some integrity issues. It's been logged, but he's useless to us.
			if(modelField) {
				result.push({
					data: field,
					text: modelField.name,
					value: modelField.id
				});
			}
			return result;
		}, []);
	}

	modelFields=immutable.array.sort(modelFields, "name", {
		comparer: model_util.fieldNameCompare
	});
	return _adapt(modelFields)
		.concat(_adapt(queryFields))
		.concat(_adapt(resultFields));
}

/**
 * Creates an array of services for selection in a selectable control
 * @param {Project} project
 * @returns {[{text:string, Value:DataField}]}
 */
function servicesToSelect(project) {
	return [
		{
			text: "none",
			value: null
		}
	]
		.concat(project.databases.map((database)=>({
			text: `DATABASE: ${project.renderTemplateString(database.name)} (${database.type})`,
			value: database.id
		})))
		.concat(project.proxies.map((proxy)=>({
			text: `PROXY: ${project.renderTemplateString(proxy.name)}`,
			value: proxy.id
		})));
}

/**
 * Create an array of models for selection in a selector
 * @param {[DataModel]} models
 * @returns {[DataModel]}
 */
function sortModels(models) {
	return _.sortBy(models, "name");
}

/**
 * Create an array of models for selection in a selector
 * @param {DataModel} model
 * @param {[DataView]} views
 * @returns {[DataView]}
 */
function sortViews(model, views=model.views) {
	return _.sortBy(views, (view)=>view.getName(model));
}

/**
 * Looks among our session data and responds with the current state of the specified property
 * @param {ApplicationState} application
 * @param {string} majorId - id of whatever object it is we may be sorting
 * @param {string} minorId - optional id that should be used if <param>majorId</param> does not ensure uniqueness
 * @param {string} property - property by which to sort on the object identified by <param>id</param>
 * @returns {"ascending"|"descending"|undefined}
 */
function sortPropertyToValue({application, majorId, minorId=undefined, property}) {
	const id=(minorId) ? `${majorId}-${minorId}` : majorId,
		value=application.getSessionSetting({id, context: "sort"});
	return (value && value.property===property)
		? value.reverse ? "descending" : "ascending"
		: undefined;
}

/**
 * Finds the current value in the application's session data and based on what he finds he translates this request into
 * an object that you may use to sort and may also used to update application session settings.
 * @param {ApplicationState} application
 * @param {string} majorId - unique id of whatever object it is we may be sorting
 * @param {string} minorId - optional id that should be used if <param>majorId</param> does not ensure uniqueness
 * @param {string} property - property by which to sort on the object identified by <param>id</param>
 * @returns {{type: string, id: string, context: string, value: {property: string, reverse: boolean}}}
 */
function sortRequestToUpdateAction({application, majorId, minorId=undefined, property}) {
	const id=(minorId) ? `${majorId}-${minorId}` : majorId,
		value=application.getSessionSetting({
			id,
			context: "sort",
			dfault: {
				property,
				reverse: true
			}
		});
	if(value.property===property) {
		value.reverse= !value.reverse;
	} else {
		value.property=property;
		value.reverse=false;
	}
	return actions.application.setSessionSetting({id, context: "sort", value});
}

/**
 * He uses our friends in here to build a master list. And then he will:
 *  - remove already referenced fields IF filter==true
 *  - sort from view to model fields
 * @param {GamutsState} gamuts
 * @param {DataModel} model
 * @param {DataView} view
 * @param {Boolean} filter - filter the results such that all results are not yet in view.requestFields
 * @param {DataFieldOperation} exclude - optional field to exclude from filter (as in - don't exclude the field's own field)
 * @returns {[{text:string, value:string}]}
 */
function viewRequestToFieldSelect({gamuts, model, view, exclude=null, filter=false}) {
	let selections=fieldsToSelect({
		gamuts, model,
		modelFields: model.fields,
		queryFields: gamuts.queryFields
	});
	if(filter) {
		const fieldIdException=_.get(exclude, "fieldId");
		selections=selections.filter((selection)=>{
			return fieldIdException===selection.value || !_.find(view.requestFields, {fieldId: selection.value});
		});
	}
	return selections;
}

/**
 * This guy translates the details of a selection from <code>fieldsToSelect</code> or <code>viewRequestToFieldSelect</code> into <param>field</param>.
 * Note: we assume that the field has been cloned (coming from <code>ValueMediator</code>). If not, then clone it.
 * @param {DataFieldOperation} field - result returned by selection control
 * @param {GamutsState} gamuts
 * @param {DataModel} model
 * @returns {DataFieldOperation}
 */
function viewRequestFieldSelectionToField({field, gamuts, model}) {
	/**
	 * We are digging up the field upon which this is based. If it's a reference to a model then there is no predefined DataFieldOperation
	 * @type {DataFieldReference|null}
	 */
	const referenceField=_.find(gamuts.queryFields, {fieldId: field.fieldId});
	/**
	 * The field pointed to by the selection. It is either a data model field or one of our canned fields. It should never be able
	 * to be missing at this point since we filter out integrity issues when we create the selection.
	 * @type {DataField}
	 */
	const referencedField=model_util.findField({
		gamuts, model,
		id: field.fieldId
	});
	field.fieldId=referencedField.id;
	field.propertyOf=_.get(referenceField, "propertyOf", constant.project.field.propertyOf.MODEL);
	if(_.hasIn(referenceField, "defaults")) {
		field.defaults=referenceField.defaults;
		field.variable=referenceField.variable;
		field.defaultValue=_.get(referenceField.defaults, "value");
		field.encoding=_.get(referenceField.defaults, "encoding", field.encoding);
		field.location=_.get(referenceField.defaults, "location", field.location);
		field.operation=_.get(referenceField.defaults, "operation", field.operation);
	} else {
		// as long as we don't have defaults our policy is to reset these guys otherwise we are potentially left with unrelated settings
		field.defaults=undefined;
		field.defaultValue=undefined;
		field.variable=undefined;
	}
	return field;
}

/**
 * This guy translates the details of a selection from <code>fieldsToSelect</code> or <code>viewRequestToFieldSelect</code> into <param>field</param>.
 * Note: we assume that the field has been cloned (coming from <code>ValueMediator</code>). If not, then clone it.
 * @param {DataFieldReference} field - result returned by selection control
 * @param {GamutsState} gamuts
 * @param {DataModel} model
 * @returns {DataFieldReference}
 */
function viewResponseFieldSelectionToField({field, gamuts, model}) {
	/**
	 * We are digging up the field upon which this is based. If it's a reference to a model then there is no predefined DataFieldOperation
	 * @type {DataFieldReference|null}
	 */
	const referenceField=_.find(gamuts.queryFields, {fieldId: field.fieldId});
	field.propertyOf=_.get(referenceField, "propertyOf", constant.project.field.propertyOf.MODEL);
	return field;
}

module.exports={
	fieldToAllowedRequestLocationsSelect,
	fieldToAllowedValuesSelect,
	fieldToSubtypes,
	fieldsToSelect,
	servicesToSelect,
	sortModels,
	sortViews,
	sortPropertyToValue,
	sortRequestToUpdateAction,
	viewRequestToFieldSelect,
	viewRequestFieldSelectionToField,
	viewResponseFieldSelectionToField
};
