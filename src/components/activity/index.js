/**
 * User: curtis
 * Date: 3/17/18
 * Time: 12:49 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const classNames=require("classnames");
const util=require("pig-core").util;
const propTypes=require("prop-types");
const actions=require("../../actions");
const constant=require("../../common/constant");

import React from "react";
import {connect} from "react-redux";
import {
	Button, Header,
	List, ListItem, ListHeader, ListDescription
} from "semantic-ui-react";
import {PigComponent} from "../common/component";
import {ToolBar} from "../common/containers";


class ActivityComponent extends PigComponent {
	/**
	 * See whether the state of this guy is opened or closed
	 * @param {Activity} item
	 * @returns {Boolean}
	 * @private
	 */
	_getItemOpened(item) {
		const application=this.props.application;
		return application.getSessionSetting({
			id: item.id,
			context: "opened",
			dfault: true
		});
	}

	/**
	 * Set whether the state of this guy is opened or closed
	 * @param {Activity} item
	 * @param {Boolean} state
	 * @private
	 */
	_setItemOpened(item, state) {
		const application=this.props.application;
		// I know, I know - I am updating his state directly. It is fine for now....and probably always.
		application.setSessionSetting({
			id: item.id,
			context: "opened",
			value: state
		});
		this.forceUpdate();
	}

	render() {
		const {className}=this.props;
		return (
			<div className={classNames("activity", className)}>
				{this._renderToolbar()}
				{this._renderBody()}
			</div>
		);
	}

	_renderToolbar() {
		const {application, onSetVisible, onClear}=this.props;
		return (
			<ToolBar className="activity-toolbar">
				<Header className="pig-left">Console</Header>
				<Button className="pig-right" size="tiny"
					onClick={()=>onSetVisible(!application.consoleVisible)}>
					{application.consoleVisible ? "Close" : "Open"}
				</Button>
				<Button className="pig-right" size="tiny"
					onClick={onClear}>
					Clear
				</Button>
			</ToolBar>
		);
	}

	_renderBody() {
		const {activity, application}=this.props;
		if(application.consoleVisible) {
			return (
				<List className="activity-body" selection>
					{activity.groomed.map(this._renderActivityItem.bind(this))}
				</List>
			);
		}
		return null;
	}

	_renderActivityItem(item) {
		function _renderDetail(detail) {
			return {
				__html: detail
					.replace(/(ftp|http|https):\/(\/[\w@&-=:.?]+){1,}[/a-zA-Z0-9]+/, '<a target="_blank" href="$&">$&</a>')
					// would be nice to have files but we would need something more sophisticated than the following. Plus browser security doesn't let us open them.
					// .replace(/\s(\/[\w-.:]+){1,}[/a-zA-Z0-9]+/, `<a target="_blank" href="file://$&">$&</a>`)
			};
		}

		function _renderDetails() {
			let index=1;
			return util.ensureArray(item.details)
				.map((detail)=>(<ListDescription key={index++} className={item.severity} dangerouslySetInnerHTML={_renderDetail(detail)}/>))
				.concat(_.get(item.stdio, "out", [])
					.map((line)=>(<ListDescription key={index++} className={constant.severity.INFO} dangerouslySetInnerHTML={_renderDetail(line)}/>)))
				.concat(_.get(item.stdio, "err", [])
					.map((line)=>(<ListDescription key={index++} className={constant.severity.ERROR} dangerouslySetInnerHTML={_renderDetail(line)}/>)));
		}

		const itemOpened=this._getItemOpened(item);
		return (
			<ListItem
				key={item.id}
				onDoubleClick={this._setItemOpened.bind(this, item, !itemOpened)}
			>
				<ListHeader>
					<ListHeader>
						<span className="timestamp">{item.timestamp.toLocaleTimeString()}</span>
						<span className="severity">{item.severity}: </span>
						{item.message}
					</ListHeader>
				</ListHeader>
				{(itemOpened) ? _renderDetails() : null}
			</ListItem>
		);
	}
}

ActivityComponent.propTypes={
	activity: propTypes.object,
	application: propTypes.object,
	className: propTypes.string,
	onSetVisible: propTypes.func,
	onClear: propTypes.func
};

const mapStateToProps=(state)=> {
	return {
		activity: state.activity,
		application: state.application
	};
};

const mapDispatchToProps=(dispatch)=> {
	return {
		onClear: ()=>dispatch(actions.activity.clear()),
		onSetVisible: (state)=>dispatch(actions.application.console(state))
	};
};

module.exports.Activity=connect(
	mapStateToProps,
	mapDispatchToProps
)(ActivityComponent);
