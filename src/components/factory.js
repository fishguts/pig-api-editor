/**
 * User: curtis
 * Date: 10/21/18
 * Time: 8:00 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const model_util=require("../model/util");
const constant=require("../common/constant");

import React from "react";
import {
	fieldToAllowedValuesSelect
} from "./mediator";
import {
	FormInput, FormDropdown
} from "semantic-ui-react";
import {
	validateValue,
	PigInput, PigSearch, PigTagEditor,
	ValueMediator
} from "./common/controls";

/* eslint-disable react/prop-types */

/********************* Public Interface *********************/
/**
 * Translates a model field into an input control suitable for accepting data for the field
 * @param {Boolean} allowEmpty - defaults to existence of "required" property in field.attributes
 * @param {DataField} field - field on which the created control is based.
 * @param {Boolean} isFormElement - whether this control is in a form or not
 * @param {function(value)} onCommit - update handler
 * @param {string} sourceField - property path of the value in <param>sourceObject</param>. By default it will be set to <param>field.name</param>
 * @param {Object} sourceObject - object that will be updated
 * @param {function(value):Boolean} - validateValue
 * @param {string} defaultClassName - will be used if you do not override with inputClassName, searchClassName and/or tagClassName
 * @param {string} inputClassName - if control ends up being an Input type control then this class will be the preferred class
 * @param {string} searchClassName - if control ends up being a Search type control then this class will be the preferred class
 * @param {string} tagClassName - if control ends up being a Tag editor type control then this class will be the preferred class
 * @returns {PigInput|PigSearch|PigTagEditor}
 */
function fieldToInputControl({
	allowEmpty=undefined,
	field,
	isFormElement=false,
	onCommit,
	sourceObject,
	sourceField=undefined,
	defaultClassName="pig-input",
	inputClassName=undefined,
	searchClassName=undefined,
	tagClassName=undefined
}) {
	if(allowEmpty===undefined) {
		allowEmpty= !_.includes(field.attributes, constant.project.field.attribute.REQUIRED);
	}
	sourceField=sourceField || field.name;
	inputClassName=inputClassName || defaultClassName;
	searchClassName=searchClassName || defaultClassName;
	tagClassName=tagClassName || defaultClassName;
	if(field.type===constant.project.field.type.OBJECT) {
		// We don't support object editing. It should be broken up into fields within the model if that functionality is desired.
		// If it is a "default value" editor for a request param...well it's weird that they have put an object in a param.
		return <PigInput
			className={inputClassName}
			disabled={true}
			mediator={new ValueMediator({
				sourceField: field.id,
				title: "[object]",
				titleType: "placeholder"
			})}
		/>;
	} else {
		const scalar=(field.type===constant.project.field.type.ARRAY),
			type=(scalar) ? field.subtype : field.type,
			isValid=_isValid.bind(null, {
				allowed: field.allowed,
				allowEmpty,
				scalar,
				type
			});
		if(type===constant.project.field.type.ENUM) {
			if(scalar) {
				return <PigTagEditor
					className={searchClassName}
					elementType={isFormElement ? FormDropdown : undefined}
					placeholder="none"
					mediator={new ValueMediator({
						isValid,
						onCommit,
						sourceField,
						sourceObject,
						sourceValues: fieldToAllowedValuesSelect(field)
					})}
				/>;
			} else {
				return <PigSearch
					className={tagClassName}
					elementType={isFormElement ? FormDropdown : undefined}
					placeholder="none"
					mediator={new ValueMediator({
						isValid,
						onCommit,
						sourceField,
						sourceObject,
						sourceValues: fieldToAllowedValuesSelect(field, field.attributes.indexOf(constant.project.field.attribute.REQUIRED)<0)
					})}
				/>;
			}
		} else {
			if(scalar) {
				return <PigTagEditor
					className={searchClassName}
					elementType={isFormElement ? FormDropdown : undefined}
					placeholder="none"
					mediator={new ValueMediator({
						isValid,
						onCommit,
						sourceField,
						sourceObject
					})}
				/>;
			} else {
				return <PigInput
					className={tagClassName}
					elementType={isFormElement ? FormInput : undefined}
					placeholder="none"
					mediator={new ValueMediator({
						isValid,
						onCommit,
						sourceField,
						sourceObject
					})}
				/>;
			}
		}
	}
}

/**
 * Translates a reference field into a suitable input control or error control
 * @param {DataFieldReference} fieldReference
 * @param {GamutsState} gamuts - include if the field can be a gamuts field
 * @param {Boolean} isFormElement - whether this control is in a form or not
 * @param {DataModel} model
 * @param {function(value)} onCommit - update handler
 * @param {function(value):Boolean} - validateValue
 * @param {string} sourceField - property path of the value in <param>sourceObject</param>.
 * @param {Object} sourceObject - object that will be updated. By default it is <param>fieldReference</param>
 * @param {string} defaultClassName - will be used if you do not override with inputClassName, searchClassName and/or tagClassName
 * @param {string} inputClassName - if control ends up being an Input type control then this class will be the preferred class
 * @param {string} searchClassName - if control ends up being a Search type control then this class will be the preferred class
 * @param {string} tagClassName - if control ends up being a Tag editor type control then this class will be the preferred class
 * @returns {PigInput|PigSearch|PigTagEditor}
 */
function fieldReferenceToInputControl({
	fieldReference,
	gamuts=undefined,
	isFormElement=false,
	model,
	onCommit,
	sourceField="defaultValue",
	sourceObject=undefined,
	defaultClassName=undefined,
	inputClassName=undefined,
	searchClassName=undefined,
	tagClassName=undefined
}) {
	const field=model_util.findField({id: fieldReference.fieldId, gamuts, model});
	if(field) {
		return fieldToInputControl({
			allowEmpty: (fieldReference.location!==constant.project.request.param.location.LITERAL),
			field,
			isFormElement,
			onCommit,
			sourceField,
			sourceObject: sourceObject || fieldReference,
			defaultClassName,
			inputClassName,
			searchClassName,
			tagClassName
		});
	} else {
		return unresolvedFieldToInputControl({
			className: defaultClassName,
			fieldId: fieldReference.fieldId,
			isFormElement,
			sourceField,
			sourceObject
		});
	}
}

/**
 * Creates a control that reveals that there are issues with the field and info about that fields
 * @param {string} className
 * @param {Error|undefined} error - optional validation error that will be shown during validation
 * @param {string} fieldId
 * @param {Boolean} isFormElement - whether this control is in a form or not
 * @param {string} sourceField
 * @param {Object} sourceObject
 * @returns {PigInput}
 */
function unresolvedFieldToInputControl({
	className="pig-input",
	error=undefined,
	fieldId,
	isFormElement=false,
	sourceField,
	sourceObject
}) {
	return <PigInput
		className={className}
		disabled={true}
		elementType={(isFormElement) ? FormInput : undefined}
		mediator={new ValueMediator({
			isValid: ()=>error,
			sourceField,
			sourceObject
		})}
	/>;
}

/********************* Private Interface *********************/
/* eslint-disable valid-jsdoc */
/**
 * Tests whether the <param>value</param> is valid for the conditions established by the configuration params
 * @param {PigFieldAllowed} allowed
 * @param {Boolean} allowEmpty
 * @param {Boolean} scalar
 * @param {string} type
 * @param {*} value
 * @return {null|Error}
 * @private
 */
function _isValid({
	allowed,
	allowEmpty,
	scalar,
	type
}, value) {
	let error;
	if(allowEmpty) {
		if(validateValue.empty(value)===null) {
			return null;
		}
	} else if((error=validateValue.notEmpty(value))) {
		return error;
	}
	let validator;
	if(type===constant.project.field.type.ENUM) {
		validator=(value)=>_.includes(allowed.values, value)
			? null
			: new Error(`Allowed values: ${_.join(allowed.values, ", ")}`);
	} else {
		if(_.get(allowed, "regex")) {
			validator=(value)=>new RegExp(allowed.regex).test(value);
		} else if(type===constant.project.field.type.INTEGER) {
			validator=validateValue.integer;
		} else if(type===constant.project.field.type.NUMBER) {
			validator=validateValue.number;
		}
	}
	if(!validator) {
		return null;
	} else if(scalar) {
		value=value || [];
		for(let index=0; index<value.length; index++) {
			if((error=validator(value[index]))) {
				return error;
			}
		}
		return null;
	} else {
		return validator(value);
	}
}

module.exports={
	fieldToInputControl,
	fieldReferenceToInputControl,
	unresolvedFieldToInputControl
};

