/**
 * User: curtis
 * Date: 3/17/18
 * Time: 12:49 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
import React from "react";
import {connect} from "react-redux";
import {List} from "semantic-ui-react";
import {Project} from "./project";
import {Toolbar} from "./toolbar";
import {PigComponent} from "../common/component";
import {ConfirmModal} from "../common/modals/confirm";
import actions from "../../actions/project";

class ProjectsComponent extends PigComponent {
	constructor(props) {
		super(props);
		this.state={
			deleteProject: null
		};
		this._closeConfirmDelete=this._closeConfirmDelete.bind(this);
		this._openConfirmDelete=this._openConfirmDelete.bind(this);
	}

	/**
	 * Assumes that a confirmation modal dialog is up. The user has chosen the project's fate and
	 * we are the executioners. And whether we delete or not the dialog is closed and life goes on.
	 * @param {Boolean} deleteProject
	 * @private
	 */
	_closeConfirmDelete(deleteProject=false) {
		if(deleteProject) {
			this.props.onDelete(this.state.deleteProject);
		}
		this.setState({
			deleteProject: null
		});
	}

	/**
	 * Give the user a chance to reflect on this move.
	 * @param {Project} project
	 * @private
	 */
	_openConfirmDelete(project) {
		this.setState({
			deleteProject: project
		});
	}

	render() {
		const {projects, onBuild, onCreate, onEdit, onRun, onSort, onStop}=this.props;
		return (
			<div>
				<Toolbar projects={projects} onCreate={_.partial(onCreate)} onSort={onSort}/>
				<List className="projects-list" divided selection>
					{
						projects.groomed.map((project)=> {
							return <Project
								key={project.id}
								project={project}
								onBuild={onBuild.bind(project)}
								onDelete={this._openConfirmDelete}
								onEdit={onEdit.bind(project)}
								onRun={onRun.bind(project)}
								onStop={onStop.bind(project)}
							/>;
						})
					}
				</List>
				{
					<ConfirmModal
						heading="Delete Project"
						open={this.state.deleteProject!==null}
						text={`Are you sure you want to delete "${_.get(this.state.deleteProject, "info.name")}"?`}
						onNo={_.partial(this._closeConfirmDelete, false)}
						onYes={_.partial(this._closeConfirmDelete, true)}
					/>
				}
			</div>
		);
	}
}

const mapStateToProps=(state)=> {
	return {
		projects: state.projects
	};
};

const mapDispatchToProps=(dispatch)=> {
	return {
		onBuild: (project)=>dispatch(actions.build(project)),
		onCreate: ()=>dispatch(actions.create()),
		onDelete: (project)=>dispatch(actions.delete(project)),
		onEdit: (project)=>dispatch(actions.edit(project)),
		onRun: (project)=>dispatch(actions.run(project)),
		onSort: (property)=>dispatch(actions.sort(property)),
		onStop: (project)=>dispatch(actions.stop(project))
	};
};

module.exports.Projects=connect(
	mapStateToProps,
	mapDispatchToProps
)(ProjectsComponent);
