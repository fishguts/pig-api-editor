/**
 * User: curtis
 * Date: 3/17/18
 * Time: 12:49 PM
 * Copyright @2018 by Xraymen Inc.
 */

import React from "react";
import {
	Button,
	Dropdown
} from "semantic-ui-react";
import propTypes from "prop-types";
import {ToolBar} from "../common/containers";

/**
 * the Projects toolbar
 * @param {ProjectsState} projects
 * @param {Function} onCreate
 * @param {Function} onSort
 * @return {*}
 */
const ProjectsToolbarComponent=function({projects, onCreate, onSort}) {
	return (
		<ToolBar className="pig-toolbar">
			<div className="pig-left">
				<Dropdown
					header="Sort Projects"
					onChange={(event, data)=>onSort(data.value)}
					options={[
						{value: "info.name:no-case", text: "Name"},
						{value: "timestampCreated:reverse", text: "Created"},
						{value: "timestampUpdated:reverse", text: "Updated"}
					]}
					placeholder='Sort'
					style={{marginTop: 10}}
					value={projects.sort.property}
				/>
			</div>
			<div className="pig-right">
				<Button
					icon="add"
					onClick={onCreate}
				/>
			</div>
		</ToolBar>
	);
};

ProjectsToolbarComponent.propTypes={
	onCreate: propTypes.func,
	onSort: propTypes.func,
	projects: propTypes.object
};

module.exports.Toolbar=ProjectsToolbarComponent;
