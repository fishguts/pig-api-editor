/**
 * User: curtis
 * Date: 3/17/18
 * Time: 6:59 PM
 * Copyright @2018 by Xraymen Inc.
 */

const constant=require("../../common/constant");

import React from "react";
import {
	Button,
	ListContent,
	ListDescription,
	ListHeader,
	ListItem
} from "semantic-ui-react";
import propTypes from "prop-types";

/**
 * Presentation project component
 * @param {Project} project
 * @param {Function} onBuild
 * @param {Function} onDelete
 * @param {Function} onEdit
 * @param {Function} onRun
 * @param {Function} onStop
 * @returns {Component}
 */
const ProjectComponent=function({project, onBuild, onDelete, onEdit, onRun, onStop}) {
	return (
		<ListItem className="projects-project">
			<ListContent floated="left" onClick={()=>onEdit(project)}>
				<ListHeader>{project.name}</ListHeader>
				<ListDescription>{project.desc}</ListDescription>
			</ListContent>
			<ListContent floated="right">
				<Button className="projects-project-edit" onClick={()=>onEdit(project)}>Edit</Button>
				<Button className="projects-project-build"
					loading={project.status.build.value===constant.status.build.EXECUTE}
					onClick={()=>onBuild(project)}
				>
					Build
				</Button>
				{
					(project.status.run.value===constant.status.run.SUCCESS)
						? <Button className="projects-project-stop" onClick={()=>onStop(project)}>Stop</Button>
						: <Button className="projects-project-run"
							disabled={project.status.build.value!==constant.status.run.SUCCESS}
							loading={project.status.run.value===constant.status.run.EXECUTE}
							onClick={()=>onRun(project)}
						>
							Run
						</Button>
				}
				<Button className="projects-project-delete" negative onClick={()=>onDelete(project)}>Delete</Button>
			</ListContent>
		</ListItem>
	);
};

ProjectComponent.propTypes={
	project: propTypes.object.isRequired,
	onBuild: propTypes.func.isRequired,
	onDelete: propTypes.func.isRequired,
	onEdit: propTypes.func.isRequired,
	onRun: propTypes.func.isRequired,
	onStop: propTypes.func.isRequired
};

module.exports.Project=ProjectComponent;
