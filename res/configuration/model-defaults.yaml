---
build:
  attributes:

database:
  id: urn:db
  connection: "mongodb://{{mongoHost}}:{{mongoPort}}"
  desc: Document storage
  name: "{{projectName}}"
  type: mongo

dataModel:
  name: untitled
  fields:
    - id: urn:mdl:fld:*
      allowed: {}
      attributes:
        - read-only
      desc: Represents the entire model
      name: "*"
      type: object
    - id: urn:mdl:fld:id
      allowed: {}
      desc: Automatically generated unique ID
      attributes:
        - auto-id
      name: _id
      type: id
  views: []

dataModelView:
  count:
    desc: "Count number of objects matching the specified query"
    function: count
    name: ""
    path: "/{{model}}/count"
    requestFields: []
    responseConfigurable: false
    responseFields:
      - _gamuts:
          path: "model.fieldReferences"
          query: {id: "urn:mdl:fld:ref:count"}
    transport:
      method: get
      protocol: http
  create-one: &view-create
    desc: "Create a {{model}}"
    function: create-one
    name: ""
    path: "/{{model}}/create"
    requestFields:
      - fieldId: urn:mdl:fld:*
        encoding: none
        location: body
        operation: set
        propertyOf: model
    responseFields:
      - fieldId: urn:mdl:fld:*
        propertyOf: model
    transport:
      method: post
      protocol: http
  create-many:
    <<: *view-create
    desc: "Create one or more {{model}}s"
    function: create-many
  delete-one: &view-delete
    desc: "Delete a {{model}}"
    function: delete-one
    name: ""
    path: "/{{model}}/delete/{id}"
    requestFields:
      - fieldId: urn:mdl:fld:id
        encoding: string
        location: path
        operation: eq
        propertyOf: model
        variable: id
    responseConfigurable: false
    responseFields:
      - _gamuts:
          path: "model.fieldReferences"
          query: {id: "urn:mdl:fld:ref:deleted"}
    transport:
      method: delete
      protocol: http
  delete-many:
    <<: *view-delete
    desc: "Delete zero or more {{model}}s"
    function: delete-many
    path: "/{{model}}/delete/{ids}"
    requestFields:
      - fieldId: urn:mdl:fld:id
        encoding: comma-delimited
        location: path
        operation: eq
        propertyOf: model
        variable: ids
  distinct:
    desc: "Get distinct values for the specified response field. The response field should be a simple type."
    function: distinct
    name: ""
    path: "/{{model}}/distinct"
    requestFields: []
    responseFields: []
    transport:
      method: get
      protocol: http
  get-one: &view-get
    desc: "Retrieve a {{model}}"
    function: get-one
    name: ""
    path: "/{{model}}/get/{id}"
    requestFields:
      - fieldId: urn:mdl:fld:id
        encoding: string
        location: path
        operation: eq
        propertyOf: model
        variable: id
    responseFields:
      - fieldId: urn:mdl:fld:*
        propertyOf: model
    transport:
      method: get
      protocol: http
  get-many:
    <<: *view-get
    desc: "Retrieve zero or more {{model}}s"
    function: get-many
    path: "/{{model}}/get/{ids}"
    requestFields:
      - fieldId: urn:mdl:fld:id
        encoding: comma-delimited
        location: path
        operation: eq
        propertyOf: model
        variable: ids
  upsert-one: &view-upsert
    desc: "Update a {{model}}. If it doesn't exist then create it."
    function: upsert-one
    name: ""
    path: "/{{model}}/upsert/{id}"
    requestFields:
      - fieldId: urn:mdl:fld:id
        encoding: string
        location: path
        operation: eq
        propertyOf: model
        variable: id
      - fieldId: urn:mdl:fld:*
        encoding: string
        location: body
        operation: set
        propertyOf: model
    responseFields:
      - fieldId: urn:mdl:fld:*
        propertyOf: model
    transport:
      method: post
      protocol: http
  update-one: &view-update
    desc: "Update a {{model}}"
    function: update-one
    name: ""
    path: "/{{model}}/update/{id}"
    requestFields:
      - fieldId: urn:mdl:fld:id
        encoding: string
        location: path
        operation: eq
        propertyOf: model
        variable: id
      - fieldId: urn:mdl:fld:*
        encoding: none
        location: body
        operation: set
        propertyOf: model
    responseFields:
      - fieldId: urn:mdl:fld:*
        propertyOf: model
    transport:
      method: put
      protocol: http
  update-many:
    <<: *view-update
    desc: "Update zero or more {{model}}s"
    function: update-many
    path: "/{{model}}/update/{ids}"
    requestFields:
      - fieldId: urn:mdl:fld:id
        encoding: comma-delimited
        location: path
        operation: eq
        propertyOf: model
        variable: ids
      - fieldId: urn:mdl:fld:*
        encoding: none
        location: body
        operation: set
        propertyOf: model

dataModelField:
  allowed: {}
  attributes: []
  name: untitled
  type: string

dataModelFieldReference:
  fieldId: urn:mdl:fld:*
  propertyOf: model

dataResultFieldReference:
  _gamuts:
    path: "model.fieldReferences"
    query: {id: "urn:mdl:fld:ref:count"}

dataModelFieldOperation:
  fieldId: urn:mdl:fld:id
  encoding: string
  location: path
  operation: eq
  propertyOf: model

dataQueryFieldOperation:
  _gamuts:
    path: "model.fieldReferences"
    query: {id: "urn:mdl:fld:op:limit"}


environment:
  debug: false
  log:
    level: error
    transports:
      - type: http
  attributes: []

environments:
  - id: urn:env:dev
    value: development
    name: development
    desc: development environment
    debug: true
    log:
      level: debug
      transports:
        - type: console
        - type: file
          filename: development.log
    attributes: []
  - id: urn:env:stg
    value: staging
    name: staging
    desc: staging environment
    debug: false
    log:
      level: info
      transports:
        - type: http
        - type: file
          filename: staging.log
    attributes: []
  - id: urn:env:pig
    value: pig
    name: pig
    desc: pig development environment
    debug: true
    log:
      level: debug
      transports:
        - type: console
        - type: file
          filename: pig.log
    attributes:
      - read-only
  - id: urn:env:prd
    value: production
    name: production
    desc: production environment
    debug: false
    log:
      level: info
      transports:
        - type: http
        - type: file
          filename: production.log
    attributes: []

info:
  legal:
    license:
      name: Apache 2.0
      url: http://www.apache.org/licenses/LICENSE-2.0.html
  name: "untitled"
  version: "0.0.1"

server:
  desc: Application server host and base URL
  url: "http://{{serverHost}}:{{serverPort}}/v1"

proxy:
  id: urn:svc:http
  desc: http service
  type: http
  details:
    method: get

# todo: this idea can be expanded upon. For example - configuration objects per language.
targets:
  - nodejs

user:
  id: urn:usr:dummy
  name: dummy

variable:
  name: ""
  type: string
  value: ""

variables:
  urn:var:project:name:
    default:
      format: variable    # "format" is a proposal for validation
      name: projectName
      type: string
      scope: local
      value: untitled
  urn:var:server:host:
    default:
      format: host
      name: serverHost
      type: string
      scope: local
      value: localhost
    urn:env:dev:
      value: dev.server.com
    urn:env:stg:
      value: stg.server.com
    urn:env:prd:
      value: prod.server.com
  urn:var:server:port:
    default:
      format: integer
      name: serverPort
      scope: local
      type: integer
      value: 8080
  urn:var:mongo:host:
    default:
      format: host
      name: mongoHost
      scope: local
      type: string
      value: localhost
  urn:var:mongo:port:
    default:
      format: integer
      name: mongoPort
      scope: local
      type: number
      value: 27017
  urn:var:pgsql:host:
    default:
      format: host
      name: pgHost
      scope: local
      type: string
      value: localhost
  urn:var:pgsql:port:
    default:
      format: integer
      name: pgPort
      scope: local
      type: number
      value: 5432
  urn:var:pgsql:password:
    default:
      format: password
      name: pgPwd
      scope: local
      type: string
      value: mysecretpassword
  urn:var:pgsql:user:
    default:
      format: variable
      name: pgUser
      scope: local
      type: string
      value: postgres
