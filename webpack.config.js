/**
 * User: curtis
 * Date: 3/7/18
 * Time: 3:23 AM
 * Copyright @2018 by Xraymen Inc.
 */

const path=require("path");
const webpack=require("webpack");
const CleanWebackPlugin=require("clean-webpack-plugin");
const HtmlWebackPlugin=require("html-webpack-plugin");
const packageJson=require("./package.json");

process.env.NODE_ENV=process.env.NODE_ENV||"development";
process.env.VERSION=packageJson.version;

// eslint-disable-next-line no-console
console.debug(`NODE_ENV=${process.env.NODE_ENV}, VERSION=${process.env.VERSION}`);

const paths={
	res: path.resolve(__dirname, "res"),
	src: path.resolve(__dirname, "src"),
	build: path.resolve(__dirname, path.join("build", process.env.NODE_ENV))
};

module.exports={
	context: paths.src,
	devServer: {
		hot: true,
		historyApiFallback: true,
		inline: true,
		port: 9010,
		stats: "normal",
		proxy: {
			"/api": {
				target: "http://localhost:9000"
			}
		}
	},
	devtool: (process.env.NODE_ENV==="development")
		? "inline-source-map"
		: false,
	entry: [
		path.join(paths.src, "app.js")
	],
	output: {
		path: paths.build,
		filename: "app.min.js"
	},
	module: {
		rules: [
			{
				test: /\.jsx?$/,
				include: paths.src,
				use: [{
					loader: "babel-loader",
					options: {
						presets: ["react", "es2015", "stage-2"]
					}
				}]
			},
			{
				test: /\.css$/,
				include: paths.res,
				use: [
					"style-loader",
					"css-loader"
				]
			},
			{
				test: /\.html$/,
				include: paths.res,
				use: ["html-loader"]
			},
			{
				test: /\.yaml$/,
				include: paths.res,
				loader: ["yaml-loader"]
			}
		]
	},
	plugins: [
		new CleanWebackPlugin([paths.build]),
		new webpack.DefinePlugin({
			"process.env.NODE_ENV": JSON.stringify(process.env.NODE_ENV),
			"process.env.VERSION": JSON.stringify(process.env.VERSION)
		}),
		new HtmlWebackPlugin({
			template: path.join(paths.res, "html", "index.html")
		})
	],
	resolve: {
		alias: {
			"fs-extra": path.join(paths.src, "nodejs/stubs")
		}
	}
};
