/**
 * User: curtis
 * Date: 6/1/18
 * Time: 10:04 PM
 * Copyright @2018 by Xraymen Inc.
 *
 * Simplifications of our model factories intended to make model object creation simpler
 */

const model_factory=require("../../src/model/factory");

Object.assign(module.exports, model_factory);

/**
 * Either settings or a user must be specified
 * @param {Object} settings
 * @param {User} user
 * @returns {Project}
 */
module.exports.createProject=({
	settings=undefined,
	user=model_factory.createUser()}={})=>model_factory.createProject({settings, user});
