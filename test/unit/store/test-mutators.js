/**
 * User: curtis
 * Date: 8/14/18
 * Time: 10:38 PM
 * Copyright @2018 by Xraymen Inc.
 */

const assert=require("assert");
const mutators=require("../../../src/store/mutators");
const proxy=require("pig-core").proxy;

describe("store.mutators", function() {
	beforeEach(()=>{
		proxy.log.stub();
	});

	afterEach(()=>{
		proxy.unstub();
		proxy.log.unstub();
	});

	describe("sort", function() {
		describe("createByProperty", function() {
			it("should sort at the top level if no property-path specified", function() {
				const mutator=mutators.sort.createByProperty();
				assert.deepEqual(mutator.apply([3, 2, 1]), [1, 2, 3]);
			});

			it("should sort at the top level if no property-path specified", function() {
				const mutator=mutators.sort.createByProperty(":reverse");
				assert.deepEqual(mutator.apply([1, 2, 3]), [3, 2, 1]);
			});

			it("should honor case by default", function() {
				const mutator=mutators.sort.createByProperty();
				assert.deepEqual(mutator.apply(["b", "A"]), ["A", "b"]);
			});

			it("should ignore case if told to do so", function() {
				const mutator=mutators.sort.createByProperty(":no-case");
				assert.deepEqual(mutator.apply(["b", "A"]), ["A", "b"]);
			});

			it("should use path if included", function() {
				const mutator=mutators.sort.createByProperty("a");
				assert.deepEqual(mutator.apply([
					{a: 2},
					{a: 1}
				]), [
					{a: 1},
					{a: 2}
				]);
			});

			it("should use path with no-case if specified", function() {
				const mutator=mutators.sort.createByProperty("a:no-case");
				assert.deepEqual(mutator.apply([
					{a: "B"},
					{a: "a"}
				]), [
					{a: "a"},
					{a: "B"}
				]);
			});
		});
	});
});
