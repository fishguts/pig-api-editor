/**
 * User: curtis
 * Date: 5/16/18
 * Time: 10:13 PM
 * Copyright @2018 by Xraymen Inc.
 */

const assert=require("assert");
const model_factory=require("../../../src/model/factory");
const {
	validateObjectName,
	validateObjectUniqueness,
	validateModelName,
	validateModelFieldName,
	validateModelViewName,
	validateRequestFieldVariableName
}=require("../../../src/model/validate");
const support_factory=require("../../support/factory");

describe("model.validate", function() {
	describe("validateObjectName", function() {
		it("should fail an empty field", function() {
			assert.ok(validateObjectName(null).message.startsWith("invalid object name: "));
			assert.ok(validateObjectName("").message.startsWith("invalid object name: "));
			assert.ok(validateObjectName(" ").message.startsWith("invalid object name: "));
		});

		it("should fail invalid names", function() {
			assert.ok(validateObjectName("1").message.startsWith("invalid object name: "));
			assert.ok(validateObjectName("$").message.startsWith("invalid object name: "));
			assert.ok(validateObjectName("a.").message.startsWith("invalid object name: "));
			assert.ok(validateObjectName("a.b").message.startsWith("invalid object name: "));
		});

		it("should pass legitimate names", function() {
			assert.strictEqual(validateObjectName("a"), null);
			assert.strictEqual(validateObjectName("a1"), null);
			assert.strictEqual(validateObjectName("aa"), null);
			assert.strictEqual(validateObjectName("aA"), null);
			assert.strictEqual(validateObjectName("Aa"), null);
		});
	});

	describe("validateObjectUniqueness", function() {
		it("should pass non-duplicates", function() {
			const container=[
					{id: "id", name: "name"}
				],
				object={id: "id", name: "name"};
			assert.strictEqual(validateObjectUniqueness(container, object, "name"), null);
			assert.strictEqual(validateObjectUniqueness(container, object, "other"), null);
		});

		it("should fail on a duplicate", function() {
			const container=[
					{id: "id1", name: "name1"},
					{id: "id2", name: "name2"}
				],
				object={id: "id1", name: "name"};
			assert.strictEqual(validateObjectUniqueness(container, object, "name1"), null);
			assert.strictEqual(validateObjectUniqueness(container, object, "name2").message, "duplicate identifier");
		});
	});

	describe("validateModelName", function() {
		it("should fail an empty field", function() {
			const project=support_factory.createProject(),
				model=model_factory.createDataModel();
			assert.ok(validateModelName(project.models, model, null).message.startsWith("invalid model name: "));
			assert.ok(validateModelName(project.models, model, "").message.startsWith("invalid model name: "));
			assert.ok(validateModelName(project.models, model, " ").message.startsWith("invalid model name: "));
		});

		it("should fail invalid names", function() {
			const project=support_factory.createProject(),
				model=model_factory.createDataModel();
			assert.ok(validateModelName(project.models, model, "1").message.startsWith("invalid model name: "));
			assert.ok(validateModelName(project.models, model, "$").message.startsWith("invalid model name: "));
			assert.ok(validateModelName(project.models, model, "a.").message.startsWith("invalid model name: "));
			assert.ok(validateModelName(project.models, model, "a.b").message.startsWith("invalid model name: "));
		});

		it("should pass legitimate names", function() {
			const project=support_factory.createProject(),
				model=model_factory.createDataModel();
			assert.strictEqual(validateModelName(project.models, model, "a"), null);
			assert.strictEqual(validateModelName(project.models, model, "a1"), null);
			assert.strictEqual(validateModelName(project.models, model, "aa"), null);
			assert.strictEqual(validateModelName(project.models, model, "aA"), null);
			assert.strictEqual(validateModelName(project.models, model, "Aa"), null);
		});

		it("should fail on a duplicate", function() {
			const project=support_factory.createProject(),
				model1=model_factory.createDataModel(),
				model2=model_factory.createDataModel();
			project.models.push(model1);
			assert.strictEqual(validateModelName(project.models, model2, model2.name).message, "duplicate identifier");
		});
	});

	describe("validateModelFieldName", function() {
		it("should fail an empty field", function() {
			const model=model_factory.createDataModel(),
				field=model_factory.createDataModelField();
			assert.ok(validateModelFieldName(model, field, null).message.startsWith("invalid field name: "));
			assert.ok(validateModelFieldName(model, field, "").message.startsWith("invalid field name: "));
			assert.ok(validateModelFieldName(model, field, " ").message.startsWith("invalid field name: "));
		});

		it("should fail invalid names", function() {
			const model=model_factory.createDataModel(),
				field=model_factory.createDataModelField();
			assert.ok(validateModelFieldName(model, field, "1").message.startsWith("invalid field name: "));
			assert.ok(validateModelFieldName(model, field, "$").message.startsWith("invalid field name: "));
			assert.ok(validateModelFieldName(model, field, "a.").message.startsWith("invalid field name: "));
			assert.ok(validateModelFieldName(model, field, "a..").message.startsWith("invalid field name: "));
			assert.ok(validateModelFieldName(model, field, "a..b").message.startsWith("invalid field name: "));
		});

		it("should pass legitimate names", function() {
			const model=model_factory.createDataModel(),
				field=model_factory.createDataModelField();
			assert.strictEqual(validateModelFieldName(model, field, "a"), null);
			assert.strictEqual(validateModelFieldName(model, field, "a1"), null);
			assert.strictEqual(validateModelFieldName(model, field, "aa"), null);
			assert.strictEqual(validateModelFieldName(model, field, "aA"), null);
			assert.strictEqual(validateModelFieldName(model, field, "Aa"), null);
			assert.strictEqual(validateModelFieldName(model, field, "aa.bb"), null);
			assert.strictEqual(validateModelFieldName(model, field, "a.1.2"), null);
		});

		it("should fail on a duplicate", function() {
			const model=model_factory.createDataModel(),
				field1=model_factory.createDataModelField(),
				field2=model_factory.createDataModelField();
			model.fields.push(field1);
			assert.strictEqual(validateModelFieldName(model, field2, field2.name).message, "duplicate identifier");
		});
	});

	describe("validateRequestFieldVariableName", function() {
		it("should allow an empty variable name", function() {
			assert.strictEqual(validateRequestFieldVariableName([], {fieldId: "id"}, ""), null);
		});

		it("should reject an invalid object name", function() {
			assert.ok(validateRequestFieldVariableName([], {fieldId: "id"}, "1").message.startsWith("invalid variable name: "));
			assert.ok(validateRequestFieldVariableName([], {fieldId: "id"}, "$").message.startsWith("invalid variable name: "));
			assert.strictEqual(validateRequestFieldVariableName([], {fieldId: "id"}, "a"), null);
		});

		it("should reject a duplicate with the same location", function() {
			const fields=[{id: "id1", variable: "name1", location: "query"}],
				field={id: "id2", variable: "name2", location: "query"};
			assert.strictEqual(validateRequestFieldVariableName(fields, field, "name1").message, "duplicate variable name");
		});

		it("should not reject a duplicate with different locations", function() {
			const fields=[{id: "id1", name: "name1", location: "body"}],
				field={id: "id2", name: "name2", location: "query"};
			assert.strictEqual(validateRequestFieldVariableName(fields, field, "name1"), null);
		});
	});
});
