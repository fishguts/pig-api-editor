/**
 * User: curtis
 * Date: 3/5/18
 * Time: 10:29 PM
 * Copyright @2018 by Xraymen Inc.
 */

const assert=require("assert");
const deepFreeze=require("deep-freeze");
const base=require("../../../src/model/_base");

class InnerModel extends base.Model {
	constructor(embedded={
		embeddedInner1: {
			inner1: "inner1"
		}
	}) {
		super(embedded);
	}
}

class OutterModel extends base.Model {
	constructor() {
		super({
			outterObject1: {
				outter1: "outter1"
			},
			outterObject2: {
				outter2: "outter2"
			}
		});
	}
	get inner() {return this._data.inner;}
	set inner(value) {this._data.inner=value;}
}

describe("model.base", function() {
	describe("clone", function() {
		it("should deeply clone an object and return the proper type", function() {
			const original=new OutterModel();
			deepFreeze(original);
			const clone=original.clone();
			assert.ok(clone instanceof OutterModel);
			assert.notStrictEqual(clone, original);
			assert.deepEqual(clone.raw, original.raw);
		});

		it("should clone object properties of a path that are necessary to keep the original model immutable", function() {
			const original=new OutterModel();
			deepFreeze(original);
			const clone=original.clone({path: "outterObject1"});
			assert.ok(clone instanceof OutterModel);
			assert.notStrictEqual(clone, original);
			assert.notStrictEqual(clone._data, original._data);
			assert.notStrictEqual(clone._data.outterObject1, original._data.outterObject1);
			assert.equal(clone.outterObject2, original.outterObject2);
			assert.deepEqual(clone.raw, original.raw);
		});

		it("should clone model properties of a path that are necessary to keep the original model immutable", function() {
			const original=new OutterModel();
			original.inner=new InnerModel();
			deepFreeze(original);
			const clone=original.clone({path: "inner.embeddedInner1"});
			assert.ok(clone instanceof OutterModel);
			assert.notStrictEqual(clone, original);
			assert.notStrictEqual(clone._data, original._data);
			assert.notStrictEqual(clone._data.inner, original._data.inner);
			assert.equal(clone.outterObject1, original.outterObject1);
			assert.equal(clone.outterObject2, original.outterObject2);
			assert.deepEqual(clone.raw, original.raw);
		});

		it("should properly descend into an array", function() {
			const original=new OutterModel();
			original.inner=new InnerModel({
				arrayOutter: [new InnerModel()]
			});
			deepFreeze(original);
			const clone=original.clone({path: "inner.arrayOutter.0.embeddedInner1"});
			assert.ok(clone instanceof OutterModel);
			assert.notStrictEqual(clone, original);
			assert.notStrictEqual(clone._data, original._data);
			assert.notStrictEqual(clone._data.inner, original._data.inner);
			assert.deepEqual(clone.raw, original.raw);
		});
	});
});
