/**
 * User: curtis
 * Date: 3/7/18
 * Time: 5:00 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const assert=require("assert");
const file=require("pig-core").file;
const proxy=require("pig-core").proxy;
const model=require("../../../../src/model/objects/index");
const model_factory=require("../../../../src/model/factory");

describe("model.project.factory", function() {
	const model_defaults=file.readToJSONSync("./res/configuration/model-defaults.yaml");

	beforeEach(function() {
		proxy.log.stub();
	});

	afterEach(function() {
		proxy.unstub();
		proxy.log.unstub();
	});

	it("all that may be constructed with defaults should do so", function() {
		[
			[model_factory.createDatabase, model.Database],
			[model_factory.createEnvironment, model.Environment],
			[model_factory.createEnvironments, Array],
			[model_factory.createDataModel, model.DataModel],
			[model_factory.createDataView, model.DataView],
			[model_factory.createDataModelField, model.DataField],
			[model_factory.createDataModelFieldOperation, model.DataFieldOperation],
			[model_factory.createDataQueryFieldOperation, model.DataFieldOperation],
			[model_factory.createDataResultFieldReference, model.DataFieldReference],
			[model_factory.createDataModelFieldReference, model.DataFieldReference],
			[model_factory.createInfo, model.Info],
			[model_factory.createServiceProxy, model.ServiceProxy],
			[model_factory.createServer, model.Server],
			[model_factory.createUser, model.User],
			[model_factory.createVariable, model.Variable],
			[model_factory.createVariables, model.Variables]
		].forEach(function(fdata) {
			const result=fdata[0]();
			assert.ok(result instanceof fdata[1], `${result.constructor.name} not instance of ${fdata[1].name}`);
		});
	});

	describe("createDataView", function() {
		it("should create each of our predefined views successfully", function() {
			Object.keys(model_defaults.dataModelView).forEach(key=>{
				const view=model_factory.createDataView({path: `dataModelView.${key}`});
				assert.ok(view instanceof model.DataView);
			});
		});
	});

	describe("createProject", function() {
		it("should create a default configuration properly", function() {
			const project=model_factory.createProject({user: model_factory.createUser()});
			assert.ok(project.id.startsWith("urn:prj"));
			// databases
			assert.equal(project.databases.length, 0);
			// environments
			assert.notEqual(project.envs.length, 0);
			project.envs.forEach(function(environment) {
				assert.ok(environment instanceof model.Environment);
				assert.ok(environment.id.startsWith("urn:env"));
			});
			// info
			assert.equal(project.info.name, "untitled");
			assert.ok(_.isEmpty(project.info.desc));
			assert.equal(project.info.version, "0.0.1");
			// proxies
			assert.equal(project.proxies.length, 0);
			// server
			assert.ok(project.server.url.startsWith("http"));
			// user
			assert.ok(project.owner.id.startsWith("urn:usr"));
			// variables
			assert.deepEqual(project.variables.get("urn:var:server:port"), {
				"env": null,
				"id": "urn:var:server:port",
				"name": "serverPort",
				"scope": "local",
				"type": "integer",
				"value": 8080
			});
			assert.notEqual(project.variables.get("urn:var:server:host", "urn:env:dev"), undefined);
			assert.equal(project.variables.get("urn:var:server:port", "urn:env:dev"), undefined);
		});
	});
});

