/**
 * User: curtis
 * Date: 5/24/18
 * Time: 11:00 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const assert=require("assert");
const {
	ProjectDependencyGraph
}=require("../../../../src/model/dependents/project");
const factory=require("../../../../src/model/factory");


describe("model.dependents.ProjectDependencyGraph", function() {
	/**
	 * @returns {{
	 *  graph: ProjectDependencyGraph,
	 *  field: DataField,
	 *  model: DataModel,
	 *  view: DataView,
	 *  responseField: DataFieldReference
	 * }}
	 * @private
	 */
	function _createInstance() {
		const project=factory.createProject({user: factory.createUser()}),
			model=factory.createDataModel(),
			view=factory.createDataView({model});
		// note: default view constructor adds a response field for the whole model
		model.views.push(view);
		project.models.push(model);
		return {
			graph: new ProjectDependencyGraph(project),
			field: model.fields[0],
			model,
			responseField: view.responseFields[0],
			view
		};
	}

	describe("fieldToFieldReferences", function() {
		it("should properly find request and response references", function() {
			const {field, graph, model, responseField}=_createInstance(),
				dependents=graph.fieldToFieldReferences({field, model});
			assert.deepEqual(_.map(dependents, "object"), [responseField]);
		});
	});
});
