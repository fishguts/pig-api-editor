/**
 * User: curtis
 * Date: 5/24/18
 * Time: 11:00 PM
 * Copyright @2018 by Xraymen Inc.
 */

const assert=require("assert");
const {
	getProjectDependencyGraph,
	ProjectDependencyGraph
}=require("../../../../src/model/dependents");
const support_factory=require("../../../support/factory");


describe("model.dependents", function() {
	describe("getProjectDependencyGraph", function() {
		it("should get proper graph instance", function() {
			const project=support_factory.createProject(),
				graph=getProjectDependencyGraph(project);
			assert.ok(graph instanceof ProjectDependencyGraph);
		});

		it("should get cached instance if one exists", function() {
			const project1=support_factory.createProject(),
				project2=support_factory.createProject();
			assert.strictEqual(getProjectDependencyGraph(project1), getProjectDependencyGraph(project1));
			assert.notStrictEqual(getProjectDependencyGraph(project1), getProjectDependencyGraph(project2));
		});
	});
});
