/**
 * User: curtis
 * Date: 8/3/18
 * Time: 8:25 PM
 * Copyright @2018 by Xraymen Inc.
 */

const assert=require("assert");
const proxy=require("pig-core").proxy;
const {ApplicationState}=require("../../../../src/model/state/application");

describe("model.state.factory", function() {
	beforeEach(function() {
		proxy.log.stub();
	});

	afterEach(function() {
		proxy.unstub();
		proxy.log.unstub();
	});

	describe("getSessionSetting/setSessionSetting", function() {
		it("should return default if not set", function() {
			const instance=new ApplicationState();
			assert.equal(instance.getSessionSetting({
				id: "id",
				dfault: "default"
			}), "default");
		});

		it("should set and return proper value for unique id", function() {
			const instance=new ApplicationState();
			instance.setSessionSetting({
				id: "id",
				value: "george"
			});
			assert.equal(instance.getSessionSetting({id: "id"}), "george");
		});

		it("should not mix data for same ids with different domains", function() {
			const instance=new ApplicationState();
			instance.setSessionSetting({
				id: "id",
				value: "id"
			});
			instance.setSessionSetting({
				id: "id",
				context: "context",
				value: "id+context"
			});
			assert.equal(instance.getSessionSetting({
				id: "id"
			}), "id");
			assert.equal(instance.getSessionSetting({
				id: "id",
				context: "context"
			}), "id+context");
		});
	});
});

