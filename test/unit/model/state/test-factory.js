/**
 * User: curtis
 * Date: 3/7/18
 * Time: 5:00 PM
 * Copyright @2018 by Xraymen Inc.
 */

const assert=require("assert");
const proxy=require("pig-core").proxy;
const model_factory=require("../../../../src/model/factory");

describe("model.state.factory", function() {
	beforeEach(function() {
		proxy.log.stub();
	});

	afterEach(function() {
		proxy.unstub();
		proxy.log.unstub();
	});

	it("should create a new instance of application if it is not already loaded", function() {
	});
});

