/**
 * User: curtis
 * Date: 5/16/18
 * Time: 10:13 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const assert=require("pig-core").assert;
const model_factory=require("../../../src/model/factory");
const {DataField}=require("../../../src/model/objects");
const model_util=require("../../../src/model/util");

describe("model.util", function() {
	describe("arrangeFields", function() {
		it("should arrange model fields properly", function() {
			const model=model_factory.createDataModel(),
				fields=model_util.arrangeFields({model});
			assert.ok(_.each(fields, field=>field instanceof DataField));
			assert.deepEqual(_.map(fields, "id"), [
				"urn:mdl:fld:*",
				"urn:mdl:fld:id"
			]);
		});

		it("should arrange gamut fields properly", function() {
			const gamuts=model_factory.createGamutsState(),
				fields=model_util.arrangeFields({gamuts});
			assert.ok(_.each(fields, field=>field instanceof DataField));
			assert.deepEqual(_.map(fields, "id"), [
				"urn:mdl:fld:count",
				"urn:mdl:fld:deleted",
				"urn:mdl:fld:fields",
				"urn:mdl:fld:limit",
				"urn:mdl:fld:skip",
				"urn:mdl:fld:sort"
			]);
		});

		it("should arrange combined fields properly", function() {
			const model=model_factory.createDataModel(),
				gamuts=model_factory.createGamutsState(),
				fields=model_util.arrangeFields({gamuts, model});
			assert.ok(_.each(fields, field=>field instanceof DataField));
			assert.deepEqual(_.map(fields, "id"), [
				"urn:mdl:fld:*",
				"urn:mdl:fld:id",
				"urn:mdl:fld:count",
				"urn:mdl:fld:deleted",
				"urn:mdl:fld:fields",
				"urn:mdl:fld:limit",
				"urn:mdl:fld:skip",
				"urn:mdl:fld:sort"
			]);
		});

		it("should properly exclude fields", function() {
			const model=model_factory.createDataModel(),
				fields=model_util.arrangeFields({
					excludeIds: ["urn:mdl:fld:*"],
					model
				});
			assert.ok(_.each(fields, field=>field instanceof DataField));
			assert.deepEqual(_.map(fields, "id"), ["urn:mdl:fld:id"]);
		});
	});

	describe("findField", function() {
		it("should find field by id properly", function() {
			const model=model_factory.createDataModel(),
				field=model_util.findField({
					fields: model.fields,
					id: "urn:mdl:fld:*"
				});
			assert.equal(field.id, "urn:mdl:fld:*");
		});

		it("should find field by name properly", function() {
			const model=model_factory.createDataModel(),
				field=model_util.findField({
					fields: model.fields,
					name: "*"
				});
			assert.equal(field.id, "urn:mdl:fld:*");
		});

		it("should find field in complex arrangement of sources", function() {
			const gamuts=model_factory.createGamutsState(),
				model=model_factory.createDataModel(),
				fieldModel=model_util.findField({
					gamuts, model,
					id: "urn:mdl:fld:*"
				}),
				fieldGamuts=model_util.findField({
					gamuts, model,
					id: "urn:mdl:fld:count"
				});
			assert.equal(fieldModel.id, "urn:mdl:fld:*");
			assert.equal(fieldGamuts.id, "urn:mdl:fld:count");
		});

		it("should dispatch on error", function() {
			const model=model_factory.createDataModel(),
				field=model_util.findField({
					fields: model.fields,
					name: "no-such-field",
					dispatch: (action)=>{
						// our add action returns a dispatcher
						assert.equal(typeof(action), "function");
					}
				});
		});
	});
});
