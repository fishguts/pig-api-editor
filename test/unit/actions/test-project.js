/**
 * User: curtis
 * Date: 6/17/18
 * Time: 1:30 AM
 * Copyright @2018 by Xraymen Inc.
 */

const proxy=require("pig-core").proxy;
const actions=require("../../../src/actions/project");
const store=require("../../../src/store/index");
const factory=require("../../support/factory");

describe("actions.project", function() {
	beforeEach(()=>{
		proxy.log.stub();
	});

	afterEach(()=>{
		proxy.unstub();
		proxy.log.unstub();
	});
});
