/**
 * User: curtis
 * Date: 8/23/18
 * Time: 9:28 PM
 * Copyright @2018 by Xraymen Inc.
 */

const proxy=require("pig-core").proxy;
const assert=require("../../../src/reducers/_assert");

describe("reducers._assert", function() {
	beforeEach(()=>{
		proxy.log.stub();
	});

	afterEach(()=>{
		proxy.unstub();
		proxy.log.unstub();
	});

	describe("activityImmutable", ()=> {
		it("should lookup the current debug state and perform add and check if in debug", ()=> {
			const object={},
				check=assert.actionImmutable({
					type: "type",
					object
				}, object);
			assert.equal(typeof(check), "function");
			// let's run it and make sure the ship is sound
			check();
		});
	});
});
