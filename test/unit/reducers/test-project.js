/**
 * User: curtis
 * Date: 3/9/18
 * Time: 11:58 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const assert=require("assert");
const deepFreeze=require("deep-freeze");
const project_actions=require("../../../src/actions/project");
const model_factory=require("../../../src/model/factory");
const model_state=require("../../../src/model/state");
const model_objects=require("../../../src/model/objects");
const project_reducers=require("../../../src/reducers/project");
const support_factory=require("../../support/factory");


describe("reducers.project", function() {
	/**
	 * sets up a <code>projects</code> state with a default project in it and selects the project.
	 * @returns {{project: Project, state: ProjectsState}}
	 * @private
	 */
	function _createProject() {
		const project=support_factory.createProject(),
			state=project_reducers.projects(
				model_factory.createProjectsState({projects: [project]}),
				project_actions.select(project)
			);
		return {
			project,
			state
		};
	}

	describe("manage", function() {
		it("should properly handle action.load", function() {

		});

		it("should properly handle action.create", function(done) {
			const project=support_factory.createProject(),
				handler=project_actions.create(project, {
					edit: false,
					save: false
				});
			handler(_.partial(project_reducers.projects, undefined))
				.then(()=> {
					done();
				});
		});

		it("should properly handle action.delete", function() {
		});

		it("should properly handle actions.save", function() {

		});

		it("default should return new state argument if undefined", function() {
			assert.ok(project_reducers.projects(undefined, {}) instanceof model_state.ProjectsState);
		});

		it("default should return unmolested state argument if unknown action", function() {
			const state=model_factory.createActivityState();
			deepFreeze(state);
			assert.strictEqual(project_reducers.projects(state, {}), state);
		});
	});

	describe("execute", function() {
		it("should properly handle actions.build", function() {

		});

		it("should properly handle actions.run", function() {

		});

		it("should properly handle actions.stop", function() {

		});

		it("should properly handle actions.validate", function() {

		});
	});

	describe("edit", function() {
		it("should properly handle actions.addDatabase", function() {
			const {project, state}=_createProject(),
				database=model_factory.createDataModel(),
				action=project_actions.addDatabase(database);
			deepFreeze(project);
			const newState=project_reducers.projects(state, action);
			assert.deepEqual(newState.selected.databases, [database]);
		});

		it("should properly handle actions.deleteDatabase", function() {
			const {project, state}=_createProject(),
				database=model_factory.createDataModel(),
				action=project_actions.deleteDatabase(database);
			project.databases.push(database);
			deepFreeze(project);
			const newState=project_reducers.projects(state, action);
			assert.deepEqual(newState.selected.databases, []);
		});

		it("should properly handle actions.updateDatabase", function() {
			const {project, state}=_createProject(),
				database1=model_factory.createDataModel(),
				database2=database1.clone(),
				action=project_actions.updateDatabase(database2);
			project.databases.push(database1);
			deepFreeze(project);
			const newState=project_reducers.projects(state, action);
			assert.deepEqual(newState.selected.databases, [database2]);
		});

		it("should properly handle actions.addEnvironment", function() {
			const {project, state}=_createProject(),
				env=model_factory.createEnvironment(),
				action=project_actions.addEnvironment(env);
			deepFreeze(project);
			const newState=project_reducers.projects(state, action);
			assert.deepEqual(newState.selected.envs, project.envs.concat(env));
		});

		it("should properly handle actions.deleteEnvironment", function() {
			const {project, state}=_createProject(),
				env=model_factory.createEnvironment(),
				action=project_actions.deleteEnvironment(env),
				original=project.envs.slice();
			project.envs.push(env);
			deepFreeze(project);
			const newState=project_reducers.projects(state, action);
			assert.deepEqual(newState.selected.envs, original);
		});

		it("should properly handle actions.updateEnvironment", function() {
			const {project, state}=_createProject(),
				env1=project.envs[0],
				env2=env1.clone(),
				action=project_actions.updateEnvironment(env2);
			env2.name="test";
			deepFreeze(project);
			const newState=project_reducers.projects(state, action);
			assert.deepEqual(newState.selected.envs, [env2].concat(project.envs.slice(1)));
		});

		it("should properly handle actions.updateInfo", function() {
			const {project, state}=_createProject(),
				info=project.info.clone(),
				action=project_actions.updateInfo(info);
			info.name="test";
			deepFreeze(project);
			const newState=project_reducers.projects(state, action);
			assert.equal(newState.selected.info, info);
		});

		it("should properly handle actions.addDataModel", function() {
			const {project, state}=_createProject(),
				action=project_actions.addDataModel();
			deepFreeze(project);
			const newState=project_reducers.projects(state, action);
			assert.deepEqual(newState.selected.models.map((object)=>object.constructor), [model_objects.DataModel]);
		});

		it("should properly handle actions.deleteDataModel", function() {
			const {project, state}=_createProject(),
				model=model_factory.createDataModel(),
				action=project_actions.deleteDataModel(model);
			project.models.push(model);
			deepFreeze(project);
			const newState=project_reducers.projects(state, action);
			assert.deepEqual(newState.selected.models, []);
		});

		it("should properly handle actions.updateDataModel", function() {
			const {project, state}=_createProject(),
				model1=model_factory.createDataModel(),
				model2=model1.clone(),
				action=project_actions.updateDataModel(model2);
			project.models.push(model1);
			deepFreeze(project);
			const newState=project_reducers.projects(state, action);
			assert.deepEqual(newState.selected.models, [model2]);
		});

		it("should properly handle actions.addDataField", function() {
			const {project, state}=_createProject(),
				model=model_factory.createDataModel(),
				action=project_actions.addDataField(model);
			project.models.push(model);
			deepFreeze(project);
			const newState=project_reducers.projects(state, action);
			assert.deepEqual(_.last(newState.selected.models[0].fields).constructor, model_objects.DataField);
		});

		it("should properly handle actions.deleteDataField", function() {
			const {project, state}=_createProject(),
				model=model_factory.createDataModel(),
				field=model_factory.createDataModelField(),
				fields=model.fields.slice(),
				action=project_actions.deleteDataField(model, field);
			model.fields.push(field);
			project.models.push(model);
			deepFreeze(project);
			const newState=project_reducers.projects(state, action);
			assert.deepEqual(newState.selected.models[0].fields, fields);
		});

		it("should properly handle actions.updateDataField", function() {
			const {project, state}=_createProject(),
				model=model_factory.createDataModel(),
				field1=model_factory.createDataModelField(),
				field2=field1.clone(),
				action=project_actions.updateDataField(model, field1, field2);
			model.fields.push(field1);
			project.models.push(model);
			deepFreeze(project);
			const newState=project_reducers.projects(state, action);
			assert.deepEqual(_.last(newState.selected.models[0].fields), field2);
		});


		it("should properly handle actions.addDataView", function() {
			const {project, state}=_createProject(),
				model=model_factory.createDataModel(),
				view=model_factory.createDataView(),
				action=project_actions.addDataView(model, view);
			project.models.push(model);
			deepFreeze(project);
			const newState=project_reducers.projects(state, action);
			assert.deepEqual(newState.selected.models[0].views, [view]);
		});

		it("should properly handle actions.deleteDataView", function() {
			const {project, state}=_createProject(),
				model=model_factory.createDataModel(),
				view=model_factory.createDataView(),
				action=project_actions.deleteDataView(model, view);
			project.models.push(model);
			model.views.push(view);
			deepFreeze(project);
			const newState=project_reducers.projects(state, action);
			assert.deepEqual(newState.selected.models[0].views, []);
		});

		it("should properly handle actions.updatedDataView", function() {
			const {project, state}=_createProject(),
				model=model_factory.createDataModel(),
				view1=model_factory.createDataView(),
				view2=view1.clone(),
				action=project_actions.updateDataView(model, view2);
			view2.name="test";
			model.views.push(view1);
			project.models.push(model);
			deepFreeze(project);
			const newState=project_reducers.projects(state, action);
			assert.equal(newState.selected.models[0].views[0], view2);
		});

		it("should properly handle actions.addDataViewRequestField", function() {
			const {project, state}=_createProject(),
				model=model_factory.createDataModel(),
				view=model_factory.createDataView(),
				action=project_actions.addDataViewRequestField(model, view);
			project.models.push(model);
			model.views.push(view);
			deepFreeze(project);
			// todo: action is now a function. I am not sure how to get the updated state when doing the dispatch on our own
			// const newState=project_reducers.projects(state, action);
			// assert.deepEqual(newState.selected.models[0].views[0].requestFields, [action.field]);
		});

		it("should properly handle actions.deleteDataViewRequestField", function() {
			const {project, state}=_createProject(),
				model=model_factory.createDataModel(),
				viewU=model_factory.createDataView(),
				viewO=viewU.clone({deep: true}),
				field=model_factory.createDataModelFieldOperation(),
				action=project_actions.deleteDataViewRequestField(model, viewU, field);
			project.models.push(model);
			model.views.push(viewU);
			viewU.requestFields.push(field);
			deepFreeze(project);
			// todo: action is now a function. I am not sure how to get the updated state when doing the dispatch on our own
			// const newState=project_reducers.projects(state, action);
			// assert.deepEqual(newState.selected.models[0].views[0], viewO);
		});

		it("should properly handle actions.updateDataViewRequestField", function() {
			const {project, state}=_createProject(),
				model=model_factory.createDataModel(),
				view=model_factory.createDataView(),
				field1=model_factory.createDataModelFieldOperation(),
				field2=field1.clone(),
				action=project_actions.updateDataViewRequestField(model, view, field2);
			field1.operation="eq";
			field2.operation="ne";
			project.models.push(model);
			model.views.push(view);
			view.requestFields.push(field1);
			deepFreeze(project);
			// todo: action is now a function. I am not sure how to get the updated state when doing the dispatch on our own
			// const newState=project_reducers.projects(state, action);
			// assert.deepEqual(newState.selected.models[0].views[0].requestFields, [field2]);
		});


		it("should properly handle actions.addDataViewResponseField", function() {
			const {project, state}=_createProject(),
				model=model_factory.createDataModel(),
				view=model_factory.createDataView(),
				action=project_actions.addDataViewResponseField(model, view);
			project.models.push(model);
			model.views.push(view);
			deepFreeze(project);
			const newState=project_reducers.projects(state, action);
			assert.deepEqual(_.last(newState.selected.models[0].views[0].responseFields), action.field);
		});

		it("should properly handle actions.deleteDataViewResponseField", function() {
			const {project, state}=_createProject(),
				model=model_factory.createDataModel(),
				viewU=model_factory.createDataView(),
				viewO=viewU.clone({deep: true}),
				field=model_factory.createDataModelFieldReference(),
				action=project_actions.deleteDataViewResponseField(model, viewU, field);
			project.models.push(model);
			model.views.push(viewU);
			viewU.responseFields.push(field);
			deepFreeze(project);
			const newState=project_reducers.projects(state, action);
			assert.deepEqual(newState.selected.models[0].views[0], viewO);
		});

		it("should properly handle actions.updateDataViewResponseField", function() {
			const {project, state}=_createProject(),
				model=model_factory.createDataModel(),
				view=model_factory.createDataView(),
				field1=model_factory.createDataModelFieldReference({fieldId: "id"}),
				field2=field1.clone(),
				action=project_actions.updateDataViewResponseField(model, view, field2);
			project.models.push(model);
			model.views.push(view);
			view.responseFields.push(field1);
			deepFreeze(project);
			const newState=project_reducers.projects(state, action);
			assert.deepEqual(_.last(newState.selected.models[0].views[0].responseFields), field2);
		});

		it("should properly handle actions.addProxy", function() {
			const {project, state}=_createProject(),
				proxy=model_factory.createServiceProxy(),
				action=project_actions.addProxy(proxy);
			deepFreeze(project);
			const newState=project_reducers.projects(state, action);
			assert.deepEqual(newState.selected.proxies, [proxy]);
		});

		it("should properly handle actions.deleteProxy", function() {
			const {project, state}=_createProject(),
				proxy=model_factory.createServiceProxy(),
				action=project_actions.deleteProxy(proxy);
			project.proxies.push(proxy);
			deepFreeze(project);
			const newState=project_reducers.projects(state, action);
			assert.deepEqual(newState.selected.proxies, []);
		});

		it("should properly handle actions.updateProxy", function() {
			const {project, state}=_createProject(),
				proxy1=model_factory.createServiceProxy(),
				proxy2=proxy1.clone(),
				action=project_actions.updateProxy(proxy2);
			project.proxies.push(proxy1);
			deepFreeze(project);
			const newState=project_reducers.projects(state, action);
			assert.deepEqual(newState.selected.proxies, [proxy2].concat(project.proxies.slice(1)));
		});

		it("should properly handle actions.setVariable", function() {
			const {project, state}=_createProject(),
				variable=model_factory.createVariable(),
				action=project_actions.setVariable(variable);
			deepFreeze(project);
			const newState=project_reducers.projects(state, action);
			assert.deepEqual(newState.selected.variables.get(variable.id), variable);
		});

		it("should properly handle actions.deleteVariable", function() {
			const {project, state}=_createProject(),
				variable=project.variables.get("urn:var:project:name"),
				action=project_actions.deleteVariable(variable);
			assert.notEqual(variable, undefined);
			deepFreeze(project);
			const newState=project_reducers.projects(state, action);
			assert.equal(newState.selected.variables.get("urn:var:project"), undefined);
		});
	});
});
